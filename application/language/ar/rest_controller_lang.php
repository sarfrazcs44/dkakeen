<?php

/*
 * English language
 */

$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';


$lang['nav_whoweare'] = 'من نحن';
$lang['nav_ourbrands'] = 'علامتنا التجارية';
$lang['nav_forcustomers'] = 'للعملاء';
$lang['nav_language'] = 'تسجيل الدخول';
$lang['home_top_section_heading'] = 'مسج أنظمة الأمن';
$lang['home_top_section_paragraph'] = 'لوريم إيبسوم دولور سيت أميت، كونسكتيتور أديبيسشينغ إليت. إينيان كومودو ليغولا إغيت دولور. إينان ماسا. كوم سوسييس ناتوك بيناتيبوس إت ماغنيس ديس بارتورينت مونتيس، ناسيتور ريديكولوس موس. دونيك كوام فيليس، ولتريسيز نيك، بيلنتيسكو يو، بريتيوم كويس، سيم. نولا ريسكاتات ماسا كويس إنيم.';
$lang['home_top_section_button'] = 'أكمل القراءة';
$lang['home_filter_heading'] = 'حدود المنتج';
$lang['home_filter_category'] = 'الاقسام';
$lang['home_filter_brand'] = 'علامة تجارية';
$lang['home_filter_subvategory'] = 'الفئات الفرعية';
$lang['home_filter_button'] = 'منقي';
$lang['home_load_more'] = 'تحميل المزيد';
$lang['home_ourclients'] = 'عملائنا';
$lang['home_testimonialname'] = 'جميل المنيا';
$lang['home_testimonialdesignation'] = 'الرئيس التنفيذي';
$lang['home_testimonialcontent'] = 'لوريم إيبسوم دولور سيت أميت، كونسكتيتور أديبيسشينغ إليت.';
$lang['home_newsletter'] = 'إشترك في نشرتنا الإخبارية';
$lang['home_newsletterplaceholder'] = 'أدخل بريدك الإلكتروني';
$lang['home_newsletterbutton'] = 'الاشتراك';
$lang['home_footercopyright'] = 'حقوق النشر | مسج سيكوريتي سيستمز';
$lang['home_faq'] = 'أسئلة وأجوبة';
$lang['home_becomeareseller'] = 'تصبح موزع';
$lang['home_downloadcenter'] = 'مركز التنزيل';
$lang['home_contactus'] = 'اتصل بنا';
$lang['home_careers'] = 'وظائف';
$lang['terms_and_conditions'] = 'الشروط والأحكام';

/* All products */

$lang['allproducts_heading'] = 'جميع المنتجات';
$lang['allproducts_button'] = 'العثور على المنتج';


/* Categories */

$lang['categories_upper_heading'] = 'منتجات';
$lang['categories_content'] = '<b>MSJ</b> لديها ميزة من المنتجات الرائدة في صناعة عالية الجودة ، والتي تلبي جميع احتياجات عملائنا. نحن نقدم جميع أنواع أنظمة الأمن والحلول مثل';
$lang['categories_lower_heading'] = 'الاقسام';
$lang['categories_button'] = 'العثور على المنتج';


/* Contact */


$lang['contact_heading'] = 'اتصل بنا';
$lang['contact_content'] = 'لوريم إيبسوم دولور سيت أميت، كونسكتيتور أديبيسشينغ إليت. إينيان كومودو ليغولا إغيت دولور. إينان ماسا. كوم سوسييس ناتوك بيناتيبوس إت ماغنيس ديس بارتورينت مونتيس، ناسيتور ريديكولوس موس. دونيك كوام فيليس، ولتريسيز نيك، بيلنتيسكو يو، بريتيوم كويس، سيم. نولا ريسكاتات ماسا كويس إنيم.';
$lang['contact_mainbranch'] = 'المكتب الرئيسي';
$lang['contact_branch1'] = 'فرع فرع الرياض';
$lang['contact_branch2'] = 'فرع فرع جدة';
$lang['contact_branch3'] = 'فرع فرع جدة';
$lang['contact_address'] = 'برج مسج العليا، الرياض';
$lang['contact_pobox'] = 'ص.ب 12321، الرياض 14121،
المملكة العربية السعودية';


/* Customer Service */

$lang['customer_heading'] = 'خدمة الزبائن';
$lang['customer_subheading'] = 'الاتصال لدينا الرقم المجاني';
$lang['customer_content'] = 'لوريم إيبسوم دولور سيت أميت، كونسكتيتور أديبيسشينغ إليت. إينيان كومودو ليغولا إغيت دولور. إينان ماسا. كوم سوسييس ناتوك بيناتيبوس إت ماغنيس ديس بارتورينت مونتيس، ناسيتور ريديكولوس موس. دونيك كوام فيليس، ولتريسيز نيك، بيلنتيسكو يو، بريتيوم كويس، سيم. نولا ريسكاتات ماسا كويس إنيم.';
$lang['customer_sendafeedback'] = 'إرسال تعليقات';
$lang['customer_submitbutton'] = 'خضع';
$lang['customer_fullname'] = 'الاسم الكام';
$lang['customer_email'] = 'البريد الإلكتروني';
$lang['customer_city'] = 'مدينة';
$lang['customer_mobile'] = 'التليفون المحمول';
$lang['customer_message'] = 'رسالة';
$lang['complain'] = 'تذمر';
$lang['visit'] = 'يزور';
$lang['reg_com'] = 'تسجيل شكوى';
$lang['confirm'] = 'تؤكد';
$lang['visit_date'] = 'حدد وقت تاريخ الزيارة';

/* Downloads*/


$lang['download_companyprofileheading'] = 'ملف الشركة';
$lang['download_companyprofilesubheading1'] = 'شركة مسج';
$lang['download_companyprofilesubheading2'] = 'بروفيل 2017';
$lang['download_brandcatalogue'] = 'كتالوج العلامة التجارية';
$lang['download_samsungCCTV'] = 'سامسونج كتف';
$lang['download_catalogue'] = 'فهرس';

/* Login */


$lang['Login_heading'] = 'دخول';
$lang['Login_subheading'] = 'للوصول إلى المعلومات الخاصة بك';
$lang['Login_button'] = 'دخول';
$lang['Login_username'] = 'اسم المستخدم';
$lang['Login_password'] = 'كلمة المرور';
$lang['Login_forgot'] = 'نسيت كلمة المرور';
$lang['Login_noaccount'] = 'لم يكن لديك حساب';
$lang['Login_register'] = 'سجل الآن';
$lang['register_heading'] = 'تسجيل';
$lang['register_fullname'] = 'الاسم الكامل';
$lang['register_username'] = 'اسم المستخدم';
$lang['register_email'] = 'البريد الإلكتروني';
$lang['register_phone'] = 'الهاتف';
$lang['register_city'] = 'مدينة';
$lang['register_country'] = 'بلد';
$lang['register_password'] = 'كلمة المرور';
$lang['register_confirmpassword'] = 'تأكيد كلمة المرور';
$lang['register_male'] = 'ذكر';
$lang['register_female'] = 'أنثى';
$lang['register_submit'] = 'عرض';




/* Product Detail */

$lang['productdetail_button'] = 'العثور على المنتج';
$lang['productdetail_description'] = 'وصف المنتج';
$lang['productdetail_usage'] = 'استعمال';
$lang['productdetail_related'] = 'منتجات ذات صله';


/* Product */

$lang['product_button'] = 'العثور على المنتج';


/* Subcategories */

$lang['subcategories_button'] = 'العثور على المنتج';


/* Who we are */

$lang['whoweare_heading'] = 'من نحن';



/* Questions */

$lang['question_heading'] = 'أسئلة متكررة';
$lang['question_field'] = 'أسئلة البحث';




/* Ticketing */

$lang['ticketing_new'] = 'ملء تذكرة شكوى جديدة';
$lang['ticketing_completed'] = 'تذاكر مكتملة';
$lang['ticketing_current'] = 'تذاكر الحالية';
$lang['ticketing_chat'] = 'دردشة';


/* Live Chat */

$lang['chat_livechat'] = 'الدردشة المباشرة';
$lang['chat_subject'] = 'موضوع';







/* Become a Reseller */

$lang['reseller_heading'] = 'تصبح البائع';

$lang['reseller_form_companyname'] = 'اسم الشركة';
$lang['reseller_form_directorname'] = 'اسم المدير';
$lang['reseller_form_mobile'] = 'التليفون المحمول';
$lang['reseller_form_email'] = 'البريد الإلكتروني';
$lang['reseller_form_tel'] = 'الهاتف';
$lang['reseller_form_fax'] = 'الفاكس';
$lang['reseller_form_file_heading'] = 'من أجل استكمال الطلب، يرجى تحميل ملف بدف يحتوي على: كر، طلب التعامل، ملف الشركة';
$lang['reseller_form_sendbutton'] = 'إرسال';

$lang['reseller_heading_resellerprogram'] = 'برنامج المورد';
$lang['reseller_content_resellerprogram'] = 'يمكن لمتخصصي النظم المؤهلين والموردين التقدم بطلب للحصول على عضوية برنامج شريكنا المخصص مع التفاصيل التالية:';

$lang['reseller_heading_keybenefits'] = 'الفوائد الرئيسية';
$lang['reseller_li1_keybenefits'] = 'الدعم في تصميم النظام وتحديد الحلول المناسبة';
$lang['reseller_li2_keybenefits'] = 'دعم أسعار حصري لمشاريع واسعة النطاق';
$lang['reseller_li3_keybenefits'] = 'دعم فني مخصص والمساعدة عن بعد عبر الإنترنت';
$lang['reseller_li4_keybenefits'] = 'برامج تدريب دورية مجانية على أحدث المنتجات';
$lang['reseller_li5_keybenefits'] = 'برنامج تدريبي حسب الطلب للحلول القائمة على المشاريع';
$lang['reseller_li6_keybenefits'] = 'مخزون كبير للتسليم الفوري على النظام';
$lang['reseller_li7_keybenefits'] = 'قطع الغيار الأسهم لدعم سريع بعد البيع';


$lang['reseller_heading_whocanapply'] = 'من يمكنه التقدم بطلب؟';
$lang['reseller_li1_whocanapply'] = 'تكامل النظم الأمنية والبائعين';
$lang['reseller_li2_whocanapply'] = 'أنظمة منخفضة الحالية';
$lang['reseller_li3_whocanapply'] = 'شركات الكمبيوتر والشبكات';
$lang['reseller_li4_whocanapply'] = 'المقاولين الميكانيكية والكهربائية والمقاولين الرئيسيين في المشاريع';

$lang['reseller_heading_requirement'] = 'متطلبات التسجيل:';
$lang['reseller_li1_requirement'] = 'تحديث الشركة حسب الأصول';
$lang['reseller_li2_requirement'] = 'معلومات الاتصال الكاملة مع العنوان البريدي';
$lang['reseller_li3_requirement'] = 'نسخة من السجل التجاري الساري';




$lang['account_not_verified'] = 'لم يتم التحقق من حسابك';
$lang['verify_account'] = 'تحقق من الحساب';
$lang['incorrect_code'] = 'رمز غير صحيح';

$lang['account_verify'] = 'تم التحقق من حسابك الآن';
$lang['verification_code'] = 'رمز التحقق';


$lang['company_name'] = 'الشركة';
$lang['directory_name'] = 'اسم المخرج';
$lang['tel'] = 'الهاتف';
$lang['fax'] = 'فاكس';
$lang['mobile'] = 'التليفون المحمول';
$lang['send'] =   'إرسال';
$lang['pdf_upload_bocome'] = 'لاستكمال الطلب ، يرجى تحميل ملف PDF يحتوي على: CR ، طلب تعامل ، ملف تعريف الشركة';
$lang['admin_close_ticket_request'] =   'المشرف إغلاق هذه التذكرة فتحه؟';
$lang['add_to_cart'] =   'أضف إلى السلة';
$lang['login_first'] =   'الرجاء تسجيل الدخول أولا';

$lang['add_successfully'] = 'اضيف بنجاح';
$lang['deleted_successfully'] = 'احذف بنجاح';

$lang['updated_successfully'] = 'تم التحديث بنجاح';
$lang['something_went_wrong'] = 'هناك خطأ ما';

$lang['po_box'] = 'P.O Box';
$lang['street'] = 'شارع';
$lang['block']  = 'بلوك';
$lang['city']   = 'مدينة';
$lang['district'] = 'منطقة';
$lang['zip']    = 'الرمز البريدي';
$lang['flat'] = 'شقة لا';
$lang['country'] = 'بلد';
$lang['welcome'] = 'أهلا بك';
$lang['add_address'] = 'اضف عنوان';
$lang['submit'] = 'خضع';
$lang['phone'] = 'هاتف';
$lang['address'] = 'عنوان';
$lang['use_this_address'] = 'استخدم هذا العنوان';
$lang['set_successfully'] = 'تعيين الافتراضي بنجاح';
$lang['logout'] = 'الخروج';
$lang['account'] = 'الحساب';
$lang['my_account'] = 'حسابي';
$lang['proceed_to_checkout'] = 'باشرالخروج من الفندق';
$lang['total_cost'] = 'التكلفة الإجمالية';
$lang['delete_message'] = 'هل أنت متأكد أنك تريد حذف؟';
$lang['proceed'] = 'تقدم';
$lang['set_default_address'] = 'يرجى تحديد العنوان الافتراضي';
$lang['no_product_in_cart'] = 'لا يوجد منتج في عربة';
$lang['thank_you'] = 'شكرا لوضع النظام';
$lang['will_back'] = 'سوف نعود اليكم قدر الإمكان';
$lang['track_id'] = 'رقم الحافلة';

$lang['pin_your_location'] = 'قم بتثبيت موقعك';
$lang['orders'] = 'أوامر';

$lang['account_not_approved'] = 'Your account is not approved';
$lang['mobile_no_verified'] = 'Your mobile no is verified successfully.';
$lang['mobile_no_failed_to_verified'] = 'Your mobile no verification has failed.';
$lang['account_not_verified1'] = 'Your account is not verified.';


