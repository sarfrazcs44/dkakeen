<?php
class LanguageLoader
{
	function initialize() {
		$ci =& get_instance();
		$ci->load->helper('language');
		
		if($ci->session->userdata('lang'))
		{
			$language = $ci->session->userdata('lang');
		} else
		{
			
	     	$language = 'en';
		}
		$ci->lang->load('rest_controller', $language);
	}
}