<?php
Class Model_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("users");
		
	}
    
    
    
   /* public function getUser($data){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$data['email']);
        $this->db->where('password',$data['password']);
        $this->db->where('role_id !=',3);
        $result = $this->db->get();
        if($result->num_rows > 0){
            return $result->result();
        }else
        {
            return false;
        }
    }*/

   public function checkIfUsernameAvailable($username)
   {
       $this->db->select('*');
       $this->db->from('users');
       $this->db->where('username',$username);
       $this->db->where('role_id',3);
       $result = $this->db->get();
       return $result->num_rows();
   }

    public function checkIfEmailAvailable($email)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email',$email);
        $this->db->where('role_id',3);
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function getUsers($where)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function getDriverUser($where)
    {
        $this->db->select('assigned_orders_for_delivery.user_id as return_assign_to,users.*');
        $this->db->from('assigned_orders_for_delivery');
        $this->db->join('users','users.user_id = assigned_orders_for_delivery.user_id','left');
        $this->db->where($where);
        $result = $this->db->get();
        return $result->row_array();
    }

   // checkIfEmailAvailable
	
		
}