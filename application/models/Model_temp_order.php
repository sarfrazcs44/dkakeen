<?php
Class Model_temp_order extends Base_Model
{
	public function __construct()
	{
		parent::__construct("temp_orders");
		
	}
    
    
    
    
    public function getTotalProduct($user_id){
        $this->db->select('COUNT(product_id) as products_count, SUM(product_quantity) as total');
        $this->db->from('temp_orders');
        $this->db->where('user_id',$user_id);
        return $this->db->get()->result();
    }
    
    
    public function getCartData($user_id){
        
        $this->db->select('temp_orders.temp_order_id,temp_orders.product_quantity,products.*,brands.title_en as brand_title_en,brands.title_ar as brand_title_ar,c.title_en as category_title_en,c.title_ar as category_title_ar,sc.title_en as sub_category_title_en,sc.title_ar as sub_category_title_ar');
        $this->db->from('temp_orders');
        $this->db->join('products','temp_orders.product_id = products.product_id','left');
        $this->db->join('brands','products.brand_id = brands.brand_id','left');
        $this->db->join('categories c','c.category_id = products.category_id','left');
        $this->db->join('categories sc','sc.category_id = products.sub_category_id','left');
        $this->db->where('temp_orders.user_id',$user_id);
        return $this->db->get()->result_array();
        
        
    }
    
    
	
		
}