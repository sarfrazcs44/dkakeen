<?php
Class Model_support_user extends Base_Model
{
	public function __construct()
	{
		parent::__construct("support_users");
		
	}

	public function getAllSupportUsers()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','2');
        return $this->db->get()->result();
    }
}