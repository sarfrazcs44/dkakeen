<?php
Class Model_chat_request extends Base_Model
{
	public function __construct()
	{
		parent::__construct("chat_request");
		
	}
    
    
    public function get_users_data($user_id)
	{
		$this->db->where_in('id',$user_id);
		$query = $this->db->get('chat_request');
		return $query->result_array();
	}




	public function getCount($insert_id){

		$this->db->select('*');
		$this->db->from('chat_request');
		$where = "id != $insert_id AND is_admin = 0 AND is_closed = 0";
		$this->db->where($where);
		return $this->db->get()->num_rows();
	}

	public function getLatestChat($chat_id){

		$this->db->select('*');
		$this->db->from('chat_request');
		$where = "id > $chat_id AND is_admin = 0 AND is_closed = 0";
		$this->db->where($where);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
		
}