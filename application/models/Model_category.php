<?php
Class Model_category extends Base_Model
{
	public function __construct()
	{
		parent::__construct("categories");
		
	}
    
    
    public function getData($category_id)
	{
		
		$this->db->select('category.*, categoryT.*,sys_lng.language_title');
		$this->db->from('categories category');
		$this->db->join('categories_text categoryT','category.category_id = categoryT.category_id');
		$this->db->join('system_languages sys_lng','categoryT.language_id = sys_lng.system_language_id');
		
		$this->db->where('category.category_id',$category_id);
		
		$query = $this->db->get();
		
	
	
	if($query->num_rows() > 0)
		{
			return $query->result();
		}else
		{
			return false;
		}
	
	
	}
	
		
}