<link href="<?php echo base_url('assets/front/');?>css/whoweare.css" rel="stylesheet" />
<style>

@media (min-height:992px)  and (max-height: 1199px) {
	
	.min-height{
	
	min-height:73.5%;
		
		}


}



@media (min-height:1200px)  and (max-height: 1600px) {
	
	.min-height{
	
	min-height:76%;
		
		}


}


</style>



<script></script>


<div class="pt90 pb90 min-height">
	<div class="container">
		<div class="row pt60">
			<div class="col-lg-12">

				<h3 class="text-red">
					<?php echo lang('whoweare_heading');?>

				</h3>
			</div>

			<div class="col-lg-12 pt30 pb30">
				<div class="row">



					<div class="w3-sidebar w3-bar-block w3-light-grey w3-card col-lg-3 col-sm-12 col-xs-12">
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/who_we_are');"><b><?php $profile = getPageData(1); echo ($this->session->lang == 'en' ? $profile->page_title_en : $profile->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/commitment');"><b><?php $commitment = getPageData(2); echo ($this->session->lang == 'en' ? $commitment->page_title_en : $commitment->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink  text-light-gray" onclick="redirect('page/mission');"><b><?php $mission = getPageData(3); echo ($this->session->lang == 'en' ? $mission->page_title_en : $mission->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray" onclick="redirect('page/partner');"><b><?php $partner = getPageData(4);echo ($this->session->lang == 'en' ? $mission->page_title_en : $mission->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-blue" onclick="redirect('page/awards');"><b><?php $awards = getPageData(5); echo ($this->session->lang == 'en' ? $awards->page_title_en : $awards->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink  text-light-gray " onclick="redirect('page/team');"><b><?php $teams = getPageData(6); echo ($this->session->lang == 'en' ? $teams->page_title_en : $teams->page_title_ar ); ?></b></button>
						<button class="w3-bar-item w3-button tablink text-light-gray " onclick="redirect('page/career');"><b><?php $careers = getPageData(7); echo ($this->session->lang == 'en' ? $careers->page_title_en : $careers->page_title_ar ); ?></b></button>
					</div>

					<div class="col-lg-9 col-sm-12 col-xs-12 tabs-content">

				        <div id="item5" class="w3-container content">

							<div>
								<div class="container">
                                    <?php 
                                    $awards = getPageData(5);
                                    $images   = getPageImages(5);
                                    
                                    ?>
									<div class="col-lg-12 col-sm-12 col-xs-12">

										<h3 class="text-blue">
											<?php echo ($this->session->lang == 'en' ? $awards->page_title_en : $awards->page_title_ar ); ?>

										</h3>

										<?php echo ($this->session->lang == 'en' ? $awards->page_description_en : $awards->page_description_ar ); ?>

									</div>

									<div class="row">

                                                                             <?php 
                                                                                if($images){ ?>
                                                                                    
										<div class="col-lg-6 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
											<div class="entry-card">
												<a data-fancybox="gallery" href="<?php echo base_url().$images[0]->image;?>" class="entry-thumb">
													<img src="<?php echo base_url().$images[0]->image;?>" alt="" class="img-fluid img-full">
												</a><!--/entry thumb-->

												<div class="entry-content">
													<div class="entry-meta mb5" align="center">
                                                                <span>
                                                                    <!--CCTV systems-->
                                                                </span>
													</div>


												</div><!--/entry content-->

											</div>
										</div><!--/.col-->
                                                                                <?php } ?>
                                                                                <?php 
                                                                                if($images){
                                                                                    $count = 1;
                                                                                 foreach($images as $key => $image){ 
                                                                                     if($key != 0 && count($images) > $count){
                                                                                     ?>
										<div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
											
                                                                                    
                                                                                    <?php if(isset($images[$key])){?>
                                                                                     <div class="entry-card mb10">
												<a data-fancybox="gallery" href="<?php echo base_url().$images[$key]->image;?>" class="entry-thumb">
													<img src="<?php echo base_url().$images[$key]->image;?>" alt="" class="img-fluid img-full">
												</a><!--/entry thumb-->
												<div class="entry-content" style="padding:1px !important;">
													<div class="entry-meta mb5" align="center">
                                                                <span>
                                                                   
                                                                </span>
													</div>


												</div><!--/entry content-->

											</div>
                                                                                    <?php } ?>
                                                                                    <?php if(isset($images[$key+1])){?>
                                                                                    <div class="entry-card mb10">
												<a data-fancybox="gallery" href="<?php echo base_url().$images[$key+1]->image;?>" class="entry-thumb">
													<img src="<?php echo base_url().$images[$key+1]->image;?>" alt="" class="img-fluid img-full">
												</a><!--/entry thumb-->
												<div class="entry-content" style="padding:1px !important;">
													<div class="entry-meta mb5" align="center">
                                                                <span>
                                                                   
                                                                </span>
													</div>


												</div><!--/entry content-->

											</div>
                                                                                    
                                                                                    <?php } ?>

											

										</div><!--/.col-->

                                                                                <?php $count = $count+2; } } } ?>
										<!--<div class="col-lg-12 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" align="center">
											<a href="#" class="btn btn-sm btn-red-outline">
												Load More
											</a>
										</div>-->




										<!--/.col-->



									</div>
								</div>
							</div>

						</div>

					</div>



				</div>

			</div>

		</div>


	</div>
</div>

<script>
    function openWhoweare(evt, Name) {
        var i, x, tablinks;
        x = document.getElementsByClassName("content");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" text-blue", " text-light-gray");
        }


        document.getElementById(Name).style.display = "block";

        // $(".tab").addClass("active"); // instead of this do the below
        evt.currentTarget.className += " text-blue";
    }
</script>