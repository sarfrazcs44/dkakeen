<style>

    @media (min-height:992px)  and (max-height: 1199px) {

        .min-height{

            min-height:66%;

        }


    }


    @media (min-height:1200px)  and (max-height: 1600px) {

        .min-height{

            min-height:69.5%;

        }


    }

</style>


<div class="bg-parallax parallax-overlay accounts-page min-height" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg6.jpg")'>
    <div class="container">
        <div class="row pb30">
            <div class="col-lg-6 col-md-6 mr-auto ml-auto col-sm-8">


                <br />
                <form role="form" method="post" action="<?php echo base_url();?>forgotpassword/save" class="forgotPassword" onsubmit="return false;">
                    <div class="alert  validatio-msg" style="display: none;">
                    </div>
                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control text-field-border" id="password" name="password" placeholder="Enter" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="password" class="form-control text-field-border" id="confirm_password" name="confirm_password" placeholder="Enter" autocomplete="off">
                    </div>
                    <div class="form-group" align="center">
                        <button type="submit" class="btn btn-success btn-md">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>