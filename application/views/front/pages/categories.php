<div class=" pt90 pb90 ">
	<div class="container">
		<div class="row pt90 pb90">
			<div class="col-lg-12">

				<h3 class="text-red">
					<?php echo lang('categories_upper_heading');?>

				</h3>
				<p><?php echo lang('categories_content');?></p>
			</div>

		</div>

		<div class="row">
			<div class="col-lg-10">

				<h3 class="text-blue">
					<?php echo lang('categories_lower_heading');?>

				</h3>
			</div>

			<div class="col-lg-2">

				<h3 class="text-blue">
                        <span class="input-group-btn" style="float:right;">
                            <a href="<?php echo base_url()?>page/products"><button  class="btn btn-product btn-primary" type="button" ><?php echo lang('allproducts_button');?></button></a>
                        </span>

				</h3>
			</div>




			<?php if($categories) {  ?>
			<?php $i = 0; foreach ($categories as $category) { ?>
                <?php $col_class = 'col-lg-3'; if($i == 0 || $i == 1) { $col_class = 'col-lg-6'; } ?>
                <div class="<?php echo $col_class ?> mb30 mt30 wow fadeInUp" data-wow-delay=".2s">
                    <div class="item">
                        <a href="<?php echo base_url()?>page/sub_categories?category=<?php echo $category['category_id']?>" class="project-overlay">
                            <img alt="image" class="img-fluid border-round" src="<?php echo base_url() . $category['image'] ?>">
                            <div class="project-overlay-caption">
                                <h4><?php echo ($this->session->lang == 'en') ? $category['title_en'] : $category['title_ar'] ?></h4>
                            </div>
                        </a><!--project overlay-->
                    </div>

                </div><!--/.col-->
                <?php $i++;  } ?>
            <?php } ?>
		</div>
	</div>
</div>