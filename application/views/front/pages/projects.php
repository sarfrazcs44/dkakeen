<?php 
$page_data = getPageData(14);
$projects = getChildPages(14);
?>
<div class=" pt90 pb90 grey">
	<div class="container">
        
		<div class="row pt90">
            
            <div class="col-lg-12">

				<h3 class="text-blue"><?php echo $page_data->page_title_en;?></h3>
            

			<?php if($projects) { ?>
            <?php foreach($projects as $project) {
                $project_images = getPageImages($project->page_id);
                ?>
			<div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" style="display: inline-block;">
				<div class="entry-card">
				
                        <?php if(isset($project_images[0]->image)){
                                    $image = base_url() . $project_images[0]->image;
                        }else{
                                $image = '';
} ?>
						<img src="<?php echo $image; ?>" alt="" class="img-fluid">
					
					<div class="entry-content">
						<h4 class="entry-title text-capitalize" align="center">
							<a href="#">
                                <?php echo ($this->session->lang == 'en') ? $project->page_title_en : $project->page_title_ar; ?>
							</a>
						</h4>
						<p align="center">
							<a href="<?php echo base_url('page/project_detail').'/'.$project->page_id; ?>" class="btn font-button btn-md btn-rounded btn-success">Detail</a>
						</p>
                        <div class="row msg alert" style="display:none;">
                            <div class="col-lg-12 msg_text" style="text-align: center;">
                                added to cart
                            </div>
                        </div>

					</div><!--/entry content-->
				</div>
			</div><!--/.col-->
            <?php } ?>
            <?php }else { ?>
                <div class="col-lg-6 col-lg-offset-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" style="text-align: center; margin: 0 auto"><div class="alert alert-info">No projects found!</div></div>
            <?php } ?>
		</div>
	</div>
</div>