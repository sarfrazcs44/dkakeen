<div class="edMainImgWrapper">
    <div class="container">
        <div class="imgBoxHere">
            <div class="row">
                <div class="col-md-6 text-right mb-4">
                    <img src="<?php echo base_url('assets/front')?>/images/mainImage.png" height="" width="" alt="">
                </div>
                <div class="col-md-6">
                    <p class="arb mb-0">مؤسسة ابتسام عبدالله باوزير للتجارة</p>
                    <p class="mb-0"><strong>Commercial registration</strong>: 1010891979</p>
                    <p><strong>P.O.Box</strong> 34162, <strong>Zip</strong> 11468</p>
                    <p>Dkakeen is an online grocery shopping application which is the fastest and convenient way to purchase your daily life groceries and receiving them at your door with just a simple Checkout.</p>
                    <p>Customers can register with Dkakeen by verifying their mobile number. Product lists are maintained and updated very well on weekly basis, efficiently by our team of experts to provide the best customer experience. We also maintain the exciting new offers and discounts for our best customers</p>
                    <p>Customers can purchase any product with easy payment system online. We as a service provider ensure the safety and order delivery quality on the specified times with our expertise</p>
                    <p>Get your app and Register Now! <a href="javascript:void(0);" data-toggle="modal" data-target="#termAndConditions"><strong>Terms & Conditions</strong></a></p>
                    <ul class="pl-0 m-0 mb-3">
                        <li class="media">
                            <a href="#"> <img class="d-flex ml-0 img-fluid" width="" src="<?php echo base_url('assets/front')?>/images/apple.png" alt="Generic placeholder image"></a>
                            <a href="#"> <img class="d-flex ml-3 img-fluid" width="" src="<?php echo base_url('assets/front')?>/images/gplay.png" alt="Generic placeholder image"></a>
                        </li>
                    </ul>
                    <p>Safe Payments via</p>
                    <ul class="list-unstyled latest-news">
                        <li class="media">
                            <a href="#"> <img class="d-flex ml-0 img-fluid" width="" src="<?php echo base_url('assets/front')?>/images/pgt_1.png" alt="Generic placeholder image"></a>
                            <a href="#"> <img class="d-flex ml-3 img-fluid" width="" src="<?php echo base_url('assets/front')?>/images/pgt_2.png" alt="Generic placeholder image"></a>
                            <a href="#"> <img class="d-flex ml-3 img-fluid" width="" src="<?php echo base_url('assets/front')?>/images/madaImg.png" alt="Generic placeholder image"></a>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal" id="termAndConditions" tabindex="-1" role="dialog" aria-labelledby="termAndConditionsTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="termAndConditionsTitle">Terms & Conditions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo $result['page_description_en']; ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>