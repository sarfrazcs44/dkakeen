<?php 
$page_data = getPageData(15);
$events = getChildPages(15);
?>
<div class=" pt90 pb90 grey">
	<div class="container">
        
		<div class="row pt90">
            
            <div class="col-lg-12">

				<h3 class="text-blue"><?php echo $page_data->page_title_en;?></h3>
			</div>
            

			<?php if($events) { ?>
            <?php foreach($events as $event) {
                $event_images = getPageImages($event->page_id);
                ?>
			<div class="col-lg-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" style="display: inline-block;">
				<div class="entry-card">
				
                        <?php if(isset($event_images[0]->image)){
                                    $image = base_url() . $event_images[0]->image;
                        }else{
                                $image = '';
} ?>
						<img src="<?php echo $image; ?>" alt="" class="img-fluid">
					
					<div class="entry-content">
						<h4 class="entry-title text-capitalize" align="center">
							<a href="#">
                                <?php echo ($this->session->lang == 'en') ? $event->page_title_en : $event->page_title_ar; ?>
							</a>
						</h4>
						<p align="center">
							<a href="<?php echo base_url('page/event_detail').'/'.$event->page_id; ?>" class="btn font-button btn-md btn-rounded btn-success">Detail</a>
						</p>
                        <div class="row msg alert" style="display:none;">
                            <div class="col-lg-12 msg_text" style="text-align: center;">
                                added to cart
                            </div>
                        </div>

					</div><!--/entry content-->
				</div>
			</div><!--/.col-->
            <?php } ?>
            <?php }else { ?>
                <div class="col-lg-6 col-lg-offset-3 mb30 mt30 wow fadeInUp" data-wow-delay=".2s" style="text-align: center; margin: 0 auto"><div class="alert alert-info">No events found!</div></div>
            <?php } ?>
		</div>
	</div>
</div>