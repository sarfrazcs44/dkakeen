<div class=" pt90 pb20 grey">
    <div class="container">
        <div class="row pt90">
            <div class="col-lg-12 mb30">
                <h3 class="text-red">
                    <?php echo ($this->session->lang == 'en' ? 'Orders Listing' : 'قائمة الطلبات'); ?>
                </h3>
            </div>
        </div>
    </div>
</div>
<div class="container pb60 pt60">
    <div class="row pt20">
        <?php $this->load->view('front/layouts/usertabs'); ?>
    </div>
    <div class="row pt60 pb40">
        <div class="col-lg-12" align="left">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs tabs-default justify-content-center mb30" role="tablist">
                <li class="nav-item" role="presentation"><a class="active nav-link" href="#t3" aria-controls="home" role="tab" data-toggle="tab"><?php echo ($this->session->lang == 'en' ? 'Current Orders' : 'الطلبات الحالية'); ?></a></li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="#t4" aria-controls="profile"
                                                            role="tab" data-toggle="tab"><?php echo ($this->session->lang == 'en' ? 'Orders History' : 'تاريخ الطلبات'); ?></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Current orders tab start -->
                <div role="tabpanel" class="tab-pane active show fade" id="t3">
                    <div id="accordion" class="mb70" role="tablist" aria-multiselectable="true">
                        <?php
                        if (count($current_orders) > 0)
                        {
                            $i = 1;
                            foreach ($current_orders as $current_order)
                            { ?>
                                <div class="item card mb10">
                                    <div class="card-header accordion-header contains_basic_details" role="tab" id="heading-current-order-<?php echo $i; ?>">
                                        <h5 class="mb-0">
                                            <div class="col-md-12">
                                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 right-inline top-percent-4"
                                                     align="right">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-current-order-<?php echo $i; ?>"
                                                       aria-expanded="true" aria-controls="collapse-current-order-<?php echo $i; ?>">
                                                        <img src="<?php echo base_url() ?>assets/front/images/more.png">
                                                    </a>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 left-inline">
                                                    <span style="font-size:14px !important;"><?php echo $current_order['full_name']; ?></span><br/>
                                                    <span style="font-size:12px !important;"><?php echo ($this->session->lang == 'en' ? 'City:' : 'مدينة:'); ?> <?php echo $current_order['city']; ?></span>
                                                    <div class="progress">

                                                        <?php if ($current_order['status'] == 'Dispatched')
                                                        { ?>
                                                            <div class="progress dispatched" ></div>
                                                        <?php } ?>
                                                        <?php if ($current_order['status'] == 'Delivered')
                                                        { ?>
                                                            <div class="progress delivered" ></div>
                                                        <?php } ?>

                                                    
                                                        <div class="one primary-color"></div>
                                                        <div class="two primary-color"></div>
                                                        <div class="three primary-color"></div>
                                                        <div class="progress-bar"></div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-5 col-md-12 col-md-12 col-sm-12 col-xs-12 left-inline top-percent-4"
                                                     align="right">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                        	<span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Order No' : 'رقم الطلب'); ?></span>
                                                            <span class="span-detail-text"><?php echo $current_order['order_track_id']; ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                            <span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Order Status' : 'حالة الطلب'); ?></span>
															<span class="span-detail-text"><?php echo $current_order['status']; ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                            <span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Total amount' : 'المبلغ الإجمالي'); ?></span>
                                                            <span class="span-detail-text"><?php echo orderTotalSumPaid($current_order['order_id']); ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                </div>
                                            </div>
                                        </h5>
                                    </div>

                                    <div id="collapse-current-order-<?php echo $i; ?>" class="collapse contains_items_detail" role="tabpanel" aria-labelledby="heading-current-order-<?php echo $i; ?>">
                                        <div class="card-body">
                                            <div class="container">
                                            
                                            
                                            
                                            <div class="col-lg-12">
                                            <div class="row">
                                            
                                            <div class="col-lg-8 left-inline" style="padding-left:0px;">
                                                <table class="table table-condensed table-responsive cart-table mb40 table-border">
                                                    <thead>
                                                     <tr class="div-color-blue">
                                                            <td class="product-image">
                                                            </td>
                                                            <td class="product-name"><?php echo ($this->session->lang == 'en' ? 'Product Name' : 'اسم المنتج'); ?></td>
                                                            <td class="product-price"><?php echo ($this->session->lang == 'en' ? 'Unit Price' : 'سعر الوحده'); ?></td>
                                                            <td class="product-quantity">
                                                             <?php echo ($this->session->lang == 'en' ? 'Quantity' : 'كمية'); ?>
                                                            </td>
                                                            <td class="product-total">
                                                             <?php echo ($this->session->lang == 'en' ? 'Total' : 'مجموع'); ?>
                                                            </td>
                                                            
                                                        </tr>
                                                    
                                                    </thead>
                                                    
                                                    <tbody>
                                                    
                                                    <?php
                                                    $total_cost = 0;
                                                    $order_items = getOrderItems($current_order['order_id']);
                                                    $order_details = getDetail($current_order['order_id']);
                                                    foreach ($order_items as $item)
                                                    {
                                                        $total_cost = $total_cost + ($item['palce_order_price']  * $item['quantity']);
                                                        $product_images = getProductImages($item['product_id']);
                                                        ?>
                                                        <tr>
                                                            <td class="product-image">
                                                                <img src="<?php echo base_url().$product_images[0]->product_image; ?>" alt="" width="50">
                                                            </td>
                                                            <td class="product-name"><a href="<?php echo base_url().'page/product_details?id='.$item['product_id']; ?>" target="_blank"><?php echo $item['product_title_en']; ?>, <?php echo $item['model_en']; ?></a><br><?php echo $item['product_description_en']; ?></td>
                                                            <td class="product-price"><?php echo $item['palce_order_price']; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></td>
                                                            <td class="product-quantity">
                                                                <?php echo $item['quantity']; ?>
                                                            </td>
                                                            <td class="product-total">
                                                                <?php echo $item['palce_order_price']  * $item['quantity']; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?>
                                                            </td>
                                                            
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    </tbody>
                                                </table>  
                                            </div>
                                            
                                             <div class="col-lg-4 left-inline" style="padding-right:0px;">
                                            
                                            <div class="col-lg-12 left-inline" style="padding-right:0px;">
                                                <h5 class="div-color-blue" style="padding:10px;margin:0px;"><?php echo ($this->session->lang == 'en' ? 'Delivery Address' : 'عنوان التسليم'); ?></h5>
                                                <p class="box-showdow-padding">
                                                    <?php
                                                    $full_address = '';
                                                    if ($order_details[0]['flat_no'] != '')
                                                    {
                                                        $full_address .= 'Flat No. '.$order_details[0]['flat_no'].', ';
                                                    }
                                                    if ($order_details[0]['block_no'] != '')
                                                    {
                                                        $full_address .= 'Block No. '.$order_details[0]['block_no'].', ';
                                                    }
                                                    if ($order_details[0]['street'] != '')
                                                    {
                                                        $full_address .= 'Street '.$order_details[0]['street'].', ';
                                                    }
                                                    if ($order_details[0]['location'] != '')
                                                    {
                                                        $full_address .= $order_details[0]['location'].', ';
                                                    }
                                                    if ($order_details[0]['district'] != '')
                                                    {
                                                        $full_address .= 'District.'.$order_details[0]['district'].', ';
                                                    }
                                                    if ($order_details[0]['city'] != '')
                                                    {
                                                        $full_address .= $order_details[0]['city'].', ';
                                                    }
                                                    if ($order_details[0]['country'] != '')
                                                    {
                                                        $full_address .= $order_details[0]['country'].', ';
                                                    }
                                                    $full_address = rtrim($full_address, ', ');
                                                    echo $full_address.'.';
                                                    ?>
                                                </p>
                                            </div>
                                            
                                            </div>
                                            
                                            </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                            <div class="row">
                                            
                                            <div class="col-lg-8 table-border pb20 pt20 div-color-blue left-inline margin-top-neg-40">
                                                    <div class="row">
                                                        <div class="col-lg-9 col-md-6 col-sm-6"><?php echo ($this->session->lang == 'en' ? 'Order No.' : 'رقم الطلب'); ?> <?php echo $order_details[0]['order_track_id']; ?></div>
                                                        <div class="col-lg-3 col-md-6 col-sm-6" align="left">
                                                            <span><?php echo ($this->session->lang == 'en' ? 'Total Cost' : 'التكلفة الإجمالية'); ?></span><span style="margin-left:20px !important;"><?php echo $total_cost; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            </div> 
                                            
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; }
                        }else{ ?>
                            <div style="text-align: center;color: red;"><?php echo ($this->session->lang == 'en' ? 'No records found.' : 'لا توجد سجلات'); ?></div>
                        <?php } ?>
                    </div>
                </div>
                <!-- Current orders tab end -->

                <!-- History orders tab start -->
                <div role="tabpanel" class="tab-pane fade" id="t4">
                    <div id="accordion" class="mb70" role="tablist" aria-multiselectable="true">
                        <?php
                        if (count($history_orders) > 0)
                        {
                            $j = 1;
                            foreach ($history_orders as $history_order)
                            { ?>
                                <div class="item card mb10">
                                    <div class="card-header accordion-header contains_basic_details" role="tab" id="heading-history-order-<?php echo $j; ?>">
                                        <h5 class="mb-0">
                                            <div class="col-md-12">
                                                <div class="col-lg-1 col-md-12 col-sm-12 col-xs-12 right-inline top-percent-4"
                                                     align="right">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-history-order-<?php echo $j; ?>"
                                                       aria-expanded="true" aria-controls="collapse-history-order-<?php echo $j; ?>">
                                                        <img src="<?php echo base_url() ?>assets/front/images/more.png">
                                                    </a>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 left-inline">
                                                    <span style="font-size:14px !important;"><?php echo $history_order['full_name']; ?></span><br/>
                                                    <span style="font-size:12px !important;"><?php echo ($this->session->lang == 'en' ? 'City:' : 'مدينة:'); ?> <?php echo $history_order['city']; ?></span>
                                                    <div class="progress">

                                                        <?php if ($history_order['status'] == 'Dispatched')
                                                        { ?>
                                                            <div class="progress dispatched" ></div>
                                                        <?php } ?>
                                                        <?php if ($history_order['status'] == 'Delivered')
                                                        { ?>
                                                            <div class="progress delivered" ></div>
                                                        <?php } ?>


                                                        <div class="one primary-color"></div>
                                                        <div class="two primary-color"></div>
                                                        <div class="three primary-color"></div>
                                                        <div class="progress-bar"></div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-5 col-md-12 col-md-12 col-sm-12 col-xs-12 left-inline top-percent-4"
                                                     align="right">
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                            <span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Order No' : 'رقم الطلب'); ?></span>
                                                            <span class="span-detail-text"><?php echo $history_order['order_track_id']; ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                            <span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Order Status' : 'حالة الطلب'); ?></span>
                                                            <span class="span-detail-text"><?php echo $history_order['status']; ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 wow fadeInUp left-inline"
                                                         data-wow-delay=".2s">
                                                        <div class="item">
                                                            <span class="span-detail-text" style="font-size:10px;"><?php echo ($this->session->lang == 'en' ? 'Total amount' : 'المبلغ الإجمالي'); ?></span>
                                                            <span class="span-detail-text"><?php echo orderTotalSumPaid($history_order['order_id']); ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></span>
                                                        </div>
                                                    </div><!--/.col-->
                                                </div>
                                            </div>
                                        </h5>
                                    </div>

                                    <div id="collapse-history-order-<?php echo $j; ?>" class="collapse contains_items_detail" role="tabpanel" aria-labelledby="heading-history-order-<?php echo $j; ?>">
                                        <div class="card-body">
                                            <div class="container">



                                                <div class="col-lg-12">
                                                    <div class="row">

                                                        <div class="col-lg-8 left-inline">
                                                            <table class="table table-condensed table-responsive cart-table mb40 table-border">
                                                                <thead>
                                                                <tr class="div-color-blue">
                                                                    <td class="product-image">
                                                                    </td>
                                                                    <td class="product-name"><?php echo ($this->session->lang == 'en' ? 'Product Name' : 'اسم المنتج'); ?></td>
                                                                    <td class="product-price"><?php echo ($this->session->lang == 'en' ? 'Unit Price' : 'سعر الوحده'); ?></td>
                                                                    <td class="product-quantity">
                                                                        <?php echo ($this->session->lang == 'en' ? 'Quantity' : 'كمية'); ?>
                                                                    </td>
                                                                    <td class="product-total">
                                                                        <?php echo ($this->session->lang == 'en' ? 'Total' : 'مجموع'); ?>
                                                                    </td>

                                                                </tr>

                                                                </thead>

                                                                <tbody>

                                                                <?php
                                                                $total_cost = 0;
                                                                $order_items = getOrderDetail($history_order['order_id']);
                                                                foreach ($order_items as $item)
                                                                {
                                                                    $total_cost = $total_cost + ($item['palce_order_price']  * $item['quantity']);
                                                                    $product_images = getProductImages($item['product_id']);
                                                                    ?>
                                                                    <tr>
                                                                        <td class="product-image">
                                                                            <img src="<?php echo base_url().$product_images[0]->product_image; ?>" alt="" width="50">
                                                                        </td>
                                                                        <td class="product-name"><a href="<?php echo base_url().'page/product_details?id='.$item['product_id']; ?>" target="_blank"><?php echo $item['product_title_en']; ?>, <?php echo $item['model_en']; ?></a><br><?php echo $item['product_description_en']; ?></td>
                                                                        <td class="product-price"><?php echo $item['palce_order_price']; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></td>
                                                                        <td class="product-quantity">
                                                                            <?php echo $item['quantity']; ?>
                                                                        </td>
                                                                        <td class="product-total">
                                                                            <?php echo $item['palce_order_price']  * $item['quantity']; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?>
                                                                        </td>

                                                                    </tr>
                                                                <?php }
                                                                ?>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                        <div class="col-lg-4 left-inline">
                                                            <h5 class="div-color-blue" style="padding:10px;margin:0px;"><?php echo ($this->session->lang == 'en' ? 'Delivery Address' : 'عنوان التسليم'); ?></h5>
                                                            <p class="box-showdow-padding">
                                                                <?php
                                                                $full_address = '';
                                                                if ($order_items[0]['flat_no'] != '')
                                                                {
                                                                    $full_address .= 'Flat No. '.$order_items[0]['flat_no'].', ';
                                                                }
                                                                if ($order_items[0]['block_no'] != '')
                                                                {
                                                                    $full_address .= 'Block No. '.$order_items[0]['block_no'].', ';
                                                                }
                                                                if ($order_items[0]['street'] != '')
                                                                {
                                                                    $full_address .= 'Street '.$order_items[0]['street'].', ';
                                                                }
                                                                if ($order_items[0]['location'] != '')
                                                                {
                                                                    $full_address .= $order_items[0]['location'].', ';
                                                                }
                                                                if ($order_items[0]['district'] != '')
                                                                {
                                                                    $full_address .= 'District.'.$order_items[0]['district'].', ';
                                                                }
                                                                if ($order_items[0]['city'] != '')
                                                                {
                                                                    $full_address .= $order_items[0]['city'].', ';
                                                                }
                                                                if ($order_items[0]['country'] != '')
                                                                {
                                                                    $full_address .= $order_items[0]['country'].', ';
                                                                }
                                                                $full_address = rtrim($full_address, ', ');
                                                                echo $full_address.'.';
                                                                ?>
                                                            </p>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-lg-12 table-border pb20 pt20 div-color-blue">
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-5 col-sm-5"><?php echo ($this->session->lang == 'en' ? 'Order No.' : 'رقم الطلب'); ?> <?php echo $order_items[0]['order_track_id']; ?></div>
                                                        <div class="col-lg-7 col-md-7 col-sm-7" align="left">
                                                            <span><?php echo ($this->session->lang == 'en' ? 'Total Cost' : 'التكلفة الإجمالية'); ?></span><span style="margin-left:10px !important;"><?php echo $total_cost; ?> <?php echo ($this->session->lang == 'en' ? 'SAR' : 'الريال السعودي'); ?></span>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $j++; }
                        }else{ ?>
                            <div style="text-align: center;color: red;"><?php echo ($this->session->lang == 'en' ? 'No records found.' : 'لا توجد سجلات'); ?></div>
                        <?php } ?>
                    </div>
                </div>
                <!-- History orders tab end -->
            </div>
        </div>
    </div>
</div>