<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Msj</title>
    <style type="text/css">

        p{
            margin-bottom:5px !important;
            margin-top:5px !important;
        }

        h4{

            margin-bottom:20px;


        }


        #wrap {
            float: left;
            position: relative;
            left: 50%;
        }

        #content {
            float: left;
            position: relative;
            left: -50%;
        }

        table {
            border-collapse: collapse;
            font-family:sans-serif;
        }

    </style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                        <tr><td><h4>Hi <?php echo ucfirst($order_details[0]['full_name']); ?>,</h4>
                                                <p>Thank you for Purchasing from MSJ Security Systems.</p>
                                                <p> Your order # <?php echo $order_details[0]['order_track_id']; ?> has been placed successfully.</p>
                                            </td></tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="1" style="text-align: center;">
                                                    <thead>
                                                    <tr style="border: #0b2e13 solid 1px;">
                                                        <th colspan="2" class="th_heading" style="color: #015685 !important;">Order Details</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td style="width: 140px;">Order Tracking #</td>
                                                        <td><strong><?php echo $order_details[0]['order_track_id']; ?></strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Recipient</td>
                                                        <td><?php echo ucfirst($order_details[0]['full_name']); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 120px;">Email</td>
                                                        <td><?php echo ucfirst($order_details[0]['email']); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Delivery Address</td>
                                                        <td><?php
                                                            $full_address = '';
                                                            if ($order_details[0]['flat_no'] != '')
                                                            {
                                                                $full_address .= 'Flat No. '.$order_details[0]['flat_no'].', ';
                                                            }
                                                            if ($order_details[0]['block_no'] != '')
                                                            {
                                                                $full_address .= 'Block No. '.$order_details[0]['block_no'].', ';
                                                            }
                                                            if ($order_details[0]['street'] != '')
                                                            {
                                                                $full_address .= 'Street '.$order_details[0]['street'].', ';
                                                            }
                                                            if ($order_details[0]['location'] != '')
                                                            {
                                                                $full_address .= $order_details[0]['location'].', ';
                                                            }
                                                            if ($order_details[0]['district'] != '')
                                                            {
                                                                $full_address .= 'District.'.$order_details[0]['district'].', ';
                                                            }
                                                            if ($order_details[0]['city'] != '')
                                                            {
                                                                $full_address .= $order_details[0]['city'].', ';
                                                            }
                                                            if ($order_details[0]['country'] != '')
                                                            {
                                                                $full_address .= $order_details[0]['country'].', ';
                                                            }
                                                            $full_address = rtrim($full_address, ', ');
                                                            echo $full_address.'.';
                                                            ?>
                                                            </p></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="1" style="text-align: center;">
                                                    <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th style="color: #015685 !important;">Product Name</th>
                                                        <th style="color: #015685 !important;">Unit Price</th>
                                                        <th style="color: #015685 !important;">Quantity</th>
                                                        <th style="color: #015685 !important;">Total</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $total_cost = 0;
                                                    $order_items = getOrderItems($order_details[0]['order_id']);
                                                    foreach ($order_items as $item)
                                                    {
                                                        $total_cost = $total_cost + ($item['palce_order_price']  * $item['quantity']);
                                                        $product_images = getProductImages($item['product_id']);
                                                        ?>
                                                        <tr>
                                                            <td class="product-image">
                                                                <img src="<?php echo base_url().$product_images[0]->product_image; ?>" alt="" width="50">
                                                            </td>
                                                            <td class="product-name"><a href="<?php echo base_url().'page/product_details?id='.$item['product_id']; ?>" target="_blank"><?php echo $item['product_title_en']; ?>, <?php echo $item['model_en']; ?></a><br><?php echo $item['product_description_en']; ?></td>
                                                            <td class="product-price"><?php echo $item['palce_order_price']; ?> SAR</td>
                                                            <td class="product-quantity">
                                                                <?php echo $item['quantity']; ?>
                                                            </td>
                                                            <td class="product-total">
                                                                <?php echo $item['palce_order_price']  * $item['quantity']; ?> SAR
                                                            </td>

                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    <tr style="font-weight: bold;"><td></td><td></td><td colspan="3" style="color: #015685 !important;">Total Paid: <?php echo $total_cost; ?> SAR</td></tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p style="font-family:sans-serif;font-size:14px;">Need Help? <a href="<?php echo base_url(); ?>" style="color:#015685;text-decoration:none;">Click here</a></p>
            <br />
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="<?php echo base_url(); ?>assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Dkakeen</h4>
                        <span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
                        
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>

</body>
</html>