<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dkakeen</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets')?>/images/favicon.ico?V=1.1">
    <!-- Plugins CSS -->
    <?php if($this->session->lang == 'en'){?>
    <link href="<?php echo base_url('assets/front')?>/css/plugins/plugins.css" rel="stylesheet">
    <?php } ?>
    <link href="<?php echo base_url('assets/front')?>/css/font-awesome.css" rel="stylesheet" />
    <?php if($this->session->lang == 'en'){?>
    <link href="<?php echo base_url('assets/front')?>/css/style.css?v=<?php echo(rand()) ?>" rel="stylesheet">
    <?php } ?>
    <script src="<?php echo base_url('assets/front')?>/js/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

</head>
<body class="content-<?php echo $this->session->lang; ?>">
<nav class="navbar navbar-expand-lg navbar-light navbar-transparent bg-faded nav-sticky">
    <div class="search-inline">
        <form>
            <input type="text" class="form-control" placeholder="Type and hit enter...">
            <button type="submit"><i class="ti-search"></i></button>
            <a href="javascript:void(0)" class="search-close"><i class="ti-close"></i></a>
        </form>
    </div><!--/search form-->
    <div class="container">
        <a class="navbar-brand" href="<?php echo base_url()?>page/">
            <img class='logo logo-light' src="<?php echo base_url('assets/front')?>/images/logo.png?v=1.1" width="120" height="50" alt="">
        </a>
        <P class="m-0"><strong>Contact Us:</strong><br>dhakeen@schopfen.com</P>
        
    </div>
</nav>
<!-- nav end -->
<!--Common Layout End -->
<script src="<?php echo base_url('assets/front')?>/js/jquery-3.3.1.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>