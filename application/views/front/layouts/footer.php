<div class="col-lg-12 sticky-footer">
<div class="footer-bottomAlt">
    <div class="container">
        <div class="row">
            <div class="col">
                <span class="d-inline-block text-white">&copy; Copyrights Dhakeen <?php // echo lang('home_footercopyright');?> | <?php echo date('Y'); ?></span>
            </div>
            <div class="col">
                <div class="clearfix float-right" style="display:inline;">
                    <a href="http://schopfen.com/" target="_blank" class="social-icon-sm si-dark ">
                        <span class="d-inline-block">Powered by</span><img src="<?php echo base_url('assets/front')?>/images//logo_white.svg" alt="" height="24" class="ml-1 d-inline-block">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div><!--/footer bottom-->
</div>
<!--back to top-->

</body>
</html>
