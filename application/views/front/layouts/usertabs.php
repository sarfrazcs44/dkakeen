<div class="col-lg-12" style="display: block;">
    <div class="col-lg-3 left-inline">

        <a href="<?php echo base_url(); ?>orders"
           class="btn inactive-login-tabs nav-link tab-font <?= ($active == 'orders') ? 'active-login-tabs' : ''; ?>">
            <?php echo($this->session->lang == 'en' ? 'Orders' : 'أوامر'); ?>
        </a>

    </div>

    <div class="col-lg-3 left-inline">

        <a href="<?php echo base_url('address'); ?>"
           class="btn inactive-login-tabs nav-link tab-font <?= ($active == 'address') ? 'active-login-tabs' : ''; ?>">
            <?php echo($this->session->lang == 'en' ? 'Addresses' : 'عناوين'); ?>
        </a>

    </div>


    <div class="col-lg-3 left-inline">

        <a href="<?php echo base_url('support'); ?>"
           class="btn inactive-login-tabs  nav-link tab-font <?= ($active == 'support') ? 'active-login-tabs' : ''; ?>">
            <?php echo($this->session->lang == 'en' ? 'Tickets' : 'تذاكر'); ?>
        </a>

    </div>

    <div class="col-lg-3 left-inline">

        <a href="<?php echo base_url() ?>page/profile_edit"
           class="btn inactive-login-tabs nav-link tab-font <?= ($active == 'profile_edit') ? 'active-login-tabs' : ''; ?>">
            <?php echo($this->session->lang == 'en' ? 'Settings' : 'الإعدادات'); ?>
        </a>

    </div>

</div>