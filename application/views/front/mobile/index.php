<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Al-Faisaliah</title>


    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/hamburgers.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/slick.css" type="text/css">
    <link rel="stylesheet" href="assets/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">


    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/script.js"></script>
</head>
<body>
	<div class="wrapper">
		<nav class="navbar navbar-fixed-top">
		  <div class="container-fluid no-padding">
			<div class="row no-gutters">
				<div class="col-md-10 col-sm-7 col-xs-7">
					<div class="menu-area">
						<button style="" class="hamburger hamburger--slider" type="button">
						  <span class="hamburger-box">
							<span class="hamburger-inner"></span>
						  </span>
						</button>
					</div>
				</div>
				<div class="col-md-2 col-sm-5 col-xs-5">
					<div class="logo-area">
						<img src="assets/img/logo.png" alt="Logo" />
					</div>
				</div>
			</div>
		  </div>
		</nav>
		<div id="section1" class="section">
			<div class="home-slider">
				<div class="image" style="background: url(assets/img/1.png);">
					<div class="details">
						<h3><b>Developing a </b>Smart city</h3>
						<p>Supporting Makkah & Jeddah..</p>
					</div>
				</div>
				<div class="image" style="background: url(assets/img/2.png);">
					<div class="details">
						<h3><b>Developing a </b>Smart city</h3>
						<p>Supporting Makkah & Jeddah..</p>
					</div>
				</div>
			</div>
			<div class="slider-controls">
				<img width="50%" class="left-arrow" src="assets/img/left-arrow.png" />
				<img width="50%" class="right-arrow" src="assets/img/right-arrow.png" />
			</div>
		</div>
		<div id="section2" class="section">
			<div class="custom-container">
				<div class="display-table title clearfix">
					<h2 class="middle no-margin">Latest Updates</h2>
					<div class="middle news-controls inline-block right">
						<img width="50%" class="left-arrow" src="assets/img/left-arrow.png" />
						<img width="50%" class="right-arrow" src="assets/img/right-arrow.png" />
					</div>
				</div>
				<div class="row section-row">
					<div class="col col-md-12 col-sm-12">
						<div class="news-card">
							<div class="image-mask">
								<img src="assets/img/king.png"/>
							</div>
							<div class="desc">
								<p class="bold date">22.12.2018</p>
								<p>His Highness, King Salman Bin Abdulaziz Al Saud</p>
							</div>
						</div>
					</div>
					<div class="col col-md-6 col-sm-6">
						<div class="news-card">
							<div class="image-mask">
								<img src="assets/img/king.png"/>
							</div>
							<div class="desc">
								<p class="bold date">22.12.2018</p>
								<p>His Highness, King Salman Bin Abdulaziz Al Saud</p>
							</div>
						</div>
					</div>
					<div class="col col-md-6 col-sm-6">
						<div class="news-card">
							<div class="image-mask">
								<img src="assets/img/king.png"/>
							</div>
							<div class="desc">
								<p class="bold date">22.12.2018</p>
								<p>His Highness, King Salman Bin Abdulaziz Al Saud</p>
							</div>
						</div>
					</div>
					<div class="col col-md-12 col-sm-12">
						<div class="news-card">
							<div class="image-mask">
								<img src="assets/img/king.png"/>
							</div>
							<div class="desc">
								<p class="bold date">22.12.2018</p>
								<p>His Highness, King Salman Bin Abdulaziz Al Saud</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="section3" class="section">
			<div class="video-section display-table" style="background: url(assets/img/video-section.png)">
				<div class="middle">
					<div class="container">
						<div class="row">
							<div class="col-md-3 col-sm-2">
								<a href="#"><img width="100%" src="assets/img/play-btn.png"/></a>
							</div>
							<div class="col-md-9 col-sm-10">
								<p>This is some description</p>
								<div>
									<img width="20%" src="assets/img/mini-logo.png">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="section4" class="section">
			<div class="principals-section custom-container">
				<div class="title">
					<h2 class="no-margin">Principals of Al-Faisaliah</h2>
				</div>
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-12 col-sm-12 content">
						<h3>Basic Framework</h3>
						<ul>
							<li>-The main direction point is Mecca</li>
							<li>-The hills and valleys of the site from a landscape</li>
							<li>&nbsp;</li>
							<li>Create contact points and pivot points</li>
							<li>-Key centers and focal points for motivational activities</li>
							<li>-Initiation of development from the main center and focal points of catalytic activities</li>
							<li>&nbsp;</li>
							<li>Take advantage of the site's infrastructure</li>
							<li>-List methods for power generation, and list plants for desalination</li>
						<ul>
					</div>
					<div class="col-md-12 col-sm-12 content">
						<h3>Specified Districts</h3>
						<ul>
							<li><b>Eastern Zone:</b> Agricultural Production and Distribution Center</li>
							<li>&nbsp;</li>
							<li><b>Central Area:</b> Center of Management, Commerce and Arts Culture</li>
							<li>&nbsp;</li>
							<li><b>Oasis:</b> Take advanture of the beautiful natural allegiance it offers</li>
							<li>&nbsp;</li>
							<li><b>Northern Hill:</b> Modesty is the unique natural pledge of allegiance</li>
							<li>&nbsp;</li>
							<li><b>The mountains:</b> Located at the foot of Mount Sitta, which is a natural successor</li>
							<li>&nbsp;</li>
							<li><b>South Zone:</b> regional center for logistics and commodities, and product distribution</li>
							<li>&nbsp;</li>
							<li><b>Youth and Enterpreneurship:</b> A vibrant area, creative and lively activities</li>
							<li>&nbsp;</li>
							<li><b>Sea Shore:</b> Quite resort and recreation area overlooking the Red Sea</li>
						<ul>
					</div>
				</div>
			</div>
		</div>
		<div id="section5" class="section">
			<div class="gap"></div>
			<div class="bg-section">
				<div class="card" style="background: url(assets/img/ph1.png);">
					<div class="custom-container">
						<div class="desc">
							<h3 class="title">Islamic identity</h3>
							<p>Architecture and design Salami Culture and heritage preservation unique location</p>
						</div>
					</div>
				</div>
				<div class="card" style="background: url(assets/img/ph2.png);">
					<div class="custom-container">
						<div class="desc">
							<h3 class="title">Intelligent and interconnected</h3>
							<p>Integrated communictions infrastructure and new technology connectivity</p>
						</div>
					</div>
				</div>
				<div class="card" style="background: url(assets/img/ph3.png);">
					<div class="custom-container">
						<div class="desc">
							<h3 class="title">Integrated and suitable for living</h3>
							<p>Equity and diversity of housing costs Amenities and public facilities Jobs and a good life</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="section6" class="section">
			<div class="stratergy-section text-center custom-container">
				<div class="title">
					<h3>Area Development Stratergy</h3>
				</div>
				<h5>Transform vision into stratergies for Area development and design principles</h5>
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="image-mask">
							<img src="assets/img/g1.png">
						</div>
						<p>Creating</p>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="image-mask">
							<img src="assets/img/g2.png">
						</div>
						<p>Respect</p>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="image-mask">
							<img src="assets/img/g3.png">
						</div>
						<p>Support</p>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="image-mask">
							<img src="assets/img/g4.png">
						</div>
						<p>Transport</p>
					</div>
				</div>
			</div>
		</div>
		<div id="section7" class=" section">
			<div class="suburbs-section custom-container">
				<div class="title">
					<h3 class="no-margin">The suburbs</h3>
					<p class="no-margin">Specified the properties of the area</p>
				</div>
				<div class="gap"></div>
				<div class="suburbs-slick row">
					<div class="card col-md-4 col-sm-4">
						<div class="image-mask">
							<img src="assets/img/slick-1.png" />
						</div>
						<div class="desc">
							<p>The Oasis</p>
						</div>
					</div>
					<div class="card col-md-4 col-sm-4">
						<div class="image-mask">
							<img src="assets/img/slick-2.png" />
						</div>
						<div class="desc">
							<p>Central City Area</p>
						</div>
					</div>
					<div class="card col-md-4 col-sm-4">
						<div class="image-mask">
							<img src="assets/img/slick-3.png" />
						</div>
						<div class="desc">
							<p>Eastern Zone</p>
						</div>
					</div>
					<div class="card col-md-4 col-sm-4">
						<div class="image-mask">
							<img src="assets/img/slick-4.png" />
						</div>
						<div class="desc">
							<p>The Mountains</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="section8" class="section">
			<div class="facilities-section custom-container">
				<div class="row">
					<div class="col-md-12 col-sm-12 content">
						<div class="title">
							<h3>District facilities</h3>
						</div>
						<p>The function of neighborhood centers is to facilitate social and economic development of society:</p>
						<div class="main-content-wrapper">
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i1.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Mosques</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i2.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Open spaces</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i3.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Educational facilities</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i4.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Health care facilities</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 content">
						<div class="title">
							<h3>Public transport system</h3>
						</div>
						<p>The multimodal transport systen provides effective regional and local connectivity</p>
						<div class="main-content-wrapper">
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i5.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Air</p>
									<div>
										<ul>
											<li>-Regional Airport</li>
											<li>-Airport Cargo with Le Joste facilities</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i6.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Maritime</p>
									<div>
										<ul>
											<li>-A Port equipped with supportive equipment for nearby ports</li>
											<li>-Cruise lines, yachts, yachts and small boats</li>
											<li>-Water taxi</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-1 col-sm-1">
									<img width="100%" src="assets/img/i7.png" />
								</div>
								<div class="col-md-11 col-sm-11">
									<p>Ground</p>
									<div>
										<ul>
											<li>-Passenger & Freight train</li>
											<li>Subway</li>
											<li>-Tram / Bus Fast</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="section9" class="section">
			<div class="aspects-section custom-container">
				<div class="text-center">
					<h2 class="lighter">Key Aspects for Investment</h2>
				</div>
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-6 col-sm-6 text-center">
						<div class="image-mask">
							<img src="assets/img/asp1.png" />
						</div>
						<p class="no-margin">Smart Connectivity</p>
					</div>
					<div class="col-md-6 col-sm-6 text-center">
						<div class="image-mask">
							<img src="assets/img/asp2.png" />
						</div>
						<p class="no-margin">Smart Community</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('.home-slider').slick({
				infinite: false,
				prevArrow: $('#section1 .left-arrow'),
				nextArrow: $('#section1 .right-arrow')
			});
			$('.suburbs-slick').slick({
				infinite: false,
				arrows: false,
				slidesToShow: 3,
				slidesToScroll: 3
			});
		});
	</script>
</body>
</html>