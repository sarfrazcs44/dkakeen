<!--Common Layout End -->

<div class=" pt90 pb20 grey">
    <div class="container">
        <div class="row pt90">
            <div class="col-lg-12 mb30">

                <h3 class="text-red">

                    Shopping Cart

                </h3>
            </div>



        </div>
    </div>

</div>

<div class="container pb60">
    <div class="row msg alert" style="display:none;">
        <div class="col-lg-12 msg_text">

        </div>
    </div>
    <table class="table table-condensed table-responsive cart-table mb40 table-border table-prop">

        <tbody>
        <?php
        $total = 0;
        if($cart_products){
            foreach($cart_products as $value){
                $product_image = getProductImages($value->product_id);
                if($product_image){
                    $image = base_url().$product_image[0]->product_image;
                }else{
                    $image = '';
                }
                ?>
                <tr id="<?php echo $value->temp_order_id;?>">
                    <td class="product-image">
                        <img src="<?php echo $image;?>" alt="" width="50" height="50">
                    </td>
                    <td class="product-name"><a href="<?php echo base_url() ?>page/product_details?id=<?php echo $value->product_id; ?>"><?php echo ($this->session->lang == 'en' ? $value->product_title_en.' / '.$value->product_description_en : $value->product_title_ar.' / '.$value->product_description_ar);?></a></td>
                    <td class="product-price"><?php echo $value->price.' SAR';?></td>
                    <td class="product-quantity">
                        <input type="number" value="<?php echo $value->product_quantity;?>" min="1" class="fl qty-text quantity" name="quantity"  data-id = "<?php echo $value->temp_order_id;?>" >
                    </td>
                    <td class="product-total">
                        <?php
                        $price = $value->price * $value->product_quantity;
                        $total = $total + $price;
                        echo $price.' SAR';
                        ?>
                    </td>
                    <td class="product-delete"><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" class="quantity_delete" data-id = "<?php echo $value->temp_order_id;?>" title="" data-original-title="Remove this item"><i class="fa fa-times text-grey"></i></a></td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>

    <a href="<?php echo base_url('checkout/proceed?checkout');?>" >
    <div class="col-lg-12 table-border pb20 pt20 div-color">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4"><?php echo lang('proceed_to_checkout');?></div>
            <div class="col-lg-7 col-md-6 col-sm-6" align="right"><span><?php echo lang('total_cost');?></span><span style="margin-left:10px !important;"><?php echo $total; ?>  SAR</span></div>
            <div class="col-lg-1 col-md-2 col-sm-2 div-color"><i class="fa fa-forward text-white"></i></div>
        </div>
    </div>
</a>

</div>




<!-- Common Layout Start -->