<!-- CSS start -->


<style>


    .rightside-left-chat,.rightside-right-chat{
        float:left;
        width:80%;
        position: relative;
    }
    .rightside-right-chat{
        float:right;
        margin-bottom:20px;

    }

    .rightside-left-chat{
        float:left;
        margin-bottom:20px;

    }

    .right-header-contentChat{

        background-color: #FFFFFF;
        position: relative;
    }

    .right-header-contentChat .rightside-left-chat p,.right-header-contentChat .rightside-right-chat p{
        background-color: #FFFFFF;
        padding:15px;
        border-radius:8px;
        color:#000;
    }

    .padding-chat{

        padding-top:9px !important;
        padding-bottom:9px !important;

    }


    .background-text-blue {
        background-color: #10396F !important;
    }


	 .no-padding-right {
        padding-right: 0px !important;
    }
	
	.no-padding-left {
        padding-left: 0px !important;
    }
	


    @media only screen and (max-width:320px){
        .main-section{
            display: none;
        }
    }



@media (min-height:992px)  and (max-height: 1199px) {
	
	.min-height{
	
	min-height:73.5%;
		
		}


}


@media (min-height:1200px)  and (max-height: 1600px) {
	
	.min-height{
	
	min-height:76%;
		
		}


}





</style>



<!-- CSS END -->




<!-- COntent Start -->

<div class="pt90 pb90 min-height">
    <div class="container">
        <div class="row pt60">
        



 			<?php $this->load->view('front/layouts/usertabs');?>



        
            <div class="col-lg-12 pt20" align="right">

                <h6 class="text-blue">
                    <?php echo($this->session->lang == 'en' ? 'Welcome' : 'أهلا بك'); ?>
                    <br />
                    <?php echo $user['full_name'] ?>

                </h6>


            </div>

            <div class="col-lg-12" align="right">

                <span class="input-group-btn" style="float:right;">
                    <a href="<?php echo base_url() ?>support/create"><button class="btn btn-md btn-danger">
                            <?php echo($this->session->lang == 'en' ? '+ Open a new ticket' : '+ افتح تذكرة جديدة'); ?>
                        </button></a>
                </span>

            </div>


            <div class="col-lg-12" align="left">


                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-default mb30" role="tablist">
                    <li class="nav-item" role="presentation"><a class="active nav-link" href="#open_tickets" aria-controls="home" role="tab" data-toggle="tab">
                            <?php echo($this->session->lang == 'en' ? 'Open Tickets' : 'افتح التذاكر'); ?>
                        </a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="#closed_tickets" aria-controls="profile" role="tab" data-toggle="tab">
                            <?php echo($this->session->lang == 'en' ? 'Closed Tickets' : 'تذاكر مغلقة'); ?>
                        </a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                
                

                   

                    <div role="tabpanel" class="tab-pane active show fade" id="open_tickets">
                         <?php if($tickets_open){ ?>
                        <div class="row text-field-border">
                            <div class="w3-sidebar w3-bar-block w3-light-grey w3-card col-lg-4 col-sm-12 col-xs-12 no-padding custom-height" >
                                <div class="tablink" >
                                    <?php foreach($tickets_open as $ticket) { ?>
                                        <button data-ticket_id="<?php echo $ticket['ticket_id'] ?>" class="ticket-open ticket-opener w3-bar-item-ticketing w3-button tablink" >
                                            <h6 class="text-blue"><?php echo($this->session->lang == 'en' ? 'Request No.' : 'رقم الطلب'); ?> <?php echo $ticket['ticket_id'] ?><span class="text-red"> <?php echo ucfirst($ticket['status'])?> </span></h6>
                                            <p><?php echo date('d, M Y', strtotime($ticket['created_at'])); ?></p>
                                            <p><?php echo substr($ticket['message'], 0, 100); ?></p>
                                            <?php if($ticket['unread'] > 0) { ?>
                                                <span class="no_unread"><?php echo $ticket['unread'] ?></span>
                                            <?php } ?>
                                        </button>
                                    
                                    <?php } ?>
                                </div>


                            </div>

                            <div class="col-lg-8 col-sm-12 col-xs-12 tabs-content-ticketing no-padding" style="height:500px; overflow-y: scroll;" >



                                <div id="item2" class="w3-container content" style="display:block">
                                    <div>
                                        <div class="container-fluid no-padding">




                                            <div class="col-lg-12">
                                                <div class="row">

                                                   

                                                    </div> 


                                                        <div class="row">
                                                            <div class="col-md-12 right-header-contentChat chatCommentsOpen">

                                                            </div>

                                                            <div class="col-lg-12" >
                                                                <div class="row no-padding">

                                                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-8 background-text pt30 pb30 " >
                                                                        <div class="w3-bar-item-ticketing w3-button tablink">
                                                                            <input type="text" name="name" id="chatMsgBox" class="form-control required text-field-border" placeholder="<?php echo($this->session->lang == 'en' ? 'Write your message...' : 'اكتب رسالتك...'); ?>" autocomplete="off">
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 background-text pt30 pb30" >
                                                                        <div class="w3-bar-item-ticketing w3-button tablink">

                                                                            <button class="btn btn-md btn-success padding-chat sendChat"><?php echo($this->session->lang == 'en' ? 'Send' : 'إرسال'); ?></button>

                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

<?php }else { ?>


                        <div role="tabpanel" class="tab-pane active show fade" id="open_tickets">

                            <div class="row text-field-border bottom-sticky">
                                <div class="alert-alert-info"><?php echo($this->session->lang == 'en' ? 'No tickets found' : 'لم يتم العثور على تذاكر'); ?></div>
                            </div>

                        </div>
                    <?php } ?>

                            </div>

                    
                    
                    
                    <div role="tabpanel" class="tab-pane fade" id="closed_tickets">
	                    <?php 
                        
                        $tickets_closed = fetchTickets($user_id,'closed');
                        if($tickets_closed){ ?>
                        <div class="row text-field-border">



                            <div class="w3-sidebar w3-bar-block w3-light-grey w3-card col-lg-4 col-sm-12 col-xs-12 no-padding custom-height">
                                <div class="tablink" >
				                    <?php foreach($tickets_closed as $ticket_c) { ?>
                                        <button data-ticket_id="<?php echo $ticket_c['ticket_id'] ?>" class="ticket-closed ticket-opener w3-bar-item-ticketing w3-button tablink">
                                            <h6 class="text-blue"><?php echo($this->session->lang == 'en' ? 'Request No.' : 'رقم الطلب'); ?> <?php echo $ticket_c['ticket_id'] ?><span class="text-red"> <?php echo ucfirst($ticket_c['status'])?> </span></h6>
                                            <p><?php echo date('d, M Y', strtotime($ticket_c['created_at'])); ?></p>
                                            <p><?php echo substr($ticket_c['message'], 0, 100); ?></p>
                                             <?php if($ticket_c['closed_request'] == 1){?>
                                        <p><?php echo lang('admin_close_ticket_request'); ?></p>
                                                            <p><a href="#" data-ticket_id="<?php echo $ticket_c['ticket_id'] ?>" class="btn btn-icon waves-effect waves-light btn-success btn-sm closed_tic yes">Y</a>
                                                            <a href="#" data-ticket_id="<?php echo $ticket_c['ticket_id'] ?>" class="btn btn-icon waves-effect waves-light btn-danger btn-sm closed_tic no">N</a></p>
                                    <?php } ?>
				                    <?php } ?>
                                        </button>
                                   
                                </div>


                            </div>

                            <div class="col-lg-8 col-sm-12 col-xs-12 tabs-content-ticketing no-padding" style="height:500px; overflow-y: scroll;" >



                                <div id="item2" class="w3-container content" style="display:block">
                                    <div>
                                        <div class="container-fluid no-padding">




                                            <div class="col-lg-12 mt30">
                                                <div class="row">

                                                    <!--                                                    <div class="col-lg-6" style="float:left;">-->
                                                    <!--                                                        <h5 class="text-blue">Request No. 1852210</h5>-->
                                                    <!--                                                        <p>22, Jun 2017</p>-->
                                                    <!--                                                    </div>-->

                                                    <div class="col-lg-12">

                                                        <!--                                                        <p>-->
                                                        <!--                                                            London is the capital city of England.-->
                                                        <!--                                                            It is the most populous city in the United Kingdom, with a metropolitan area of over 13 million inhabitants.-->
                                                        <!--                                                        </p>-->

                                                        <!--                                                        <h6>Replies</h6>-->


                                                        <div class="row">
                                                            <div class="col-md-12 right-header-contentChat chatCommentsClosed">

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>



                        </div>
	                    <?php }else { ?>

                           <div class="row text-field-border bottom-sticky">
                                <div class="alert-alert-info"><?php echo($this->session->lang == 'en' ? 'No tickets found' : 'لم يتم العثور على تذاكر'); ?></div>
                            </div>
	                    <?php } ?>
                    </div>


                        </div>

                    </div>



                    



                </div>



            </div>


        </div>




<!-- Content End -->


<!-- script start -->

<!--<script>-->
<!---->
<!---->
<!--    function openTicketing(evt, Name) {-->
<!--        var i, x, tablinks;-->
<!---->
<!---->
<!--        x = document.getElementsByClassName("content");-->
<!--        for (i = 0; i < x.length; i++) {-->
<!--            x[i].style.display = "none";-->
<!--        }-->
<!---->
<!---->
<!--        tablinks = document.getElementsByClassName("tablink");-->
<!--        for (i = 0; i <= x.length; i++) {-->
<!---->
<!---->
<!--            tablinks[i].className = tablinks[i].className.replace(" background-blue", "");-->
<!--        }-->
<!---->
<!---->
<!--        document.getElementById(Name).style.display = "block";-->
<!---->
<!--        // $(".tab").addClass("active"); // instead of this do the below-->
<!--        evt.currentTarget.className += " background-blue";-->
<!--    }-->
<!---->
<!--</script>-->

<!-- script end -->


