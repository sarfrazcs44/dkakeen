<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dkakeen</title>
    <style type="text/css">

        p {
            margin-bottom: 5px !important;
            margin-top: 5px !important;
        }

        h4 {

            margin-bottom: 20px;


        }


        #wrap {
            float: left;
            position: relative;
            left: 50%;
        }

        #content {
            float: left;
            position: relative;
            left: -50%;
        }

        table {
            border-collapse: collapse;
            font-family: sans-serif;
        }

    </style>

</head>


<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                        <tr>
                                            <td>
                                                <?php echo $content; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
                 <p style="font-family:sans-serif;font-size:14px;margin-left: 43px;">Need Help? Email us at <a href="mailto:info@dkakeen.com">info@dkakeen.com</a></p>
                
               
            
                <p style="font-family:sans-serif;font-size:14px;margin-left: 43px;">Whatsapp: <a href="tel:9661234567"
                                                                                style="color:#015685;text-decoration:none;">9661234567</a></p>
            
                <p style="font-family:sans-serif;font-size:14px;margin-left: 43px;">Fax: <a href="tel:9661234567"
                                                                                style="color:#015685;text-decoration:none;">9661234567</a></p>
            
            <br/>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="<?php echo base_url();?>assets/front/images/logo.png" width="60" height="60"
                             alt="Site Logo">
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">Dkakeen</h4>
                        <span style="font-family:sans-serif;color:grey;font-size:12px;">&nbsp;</span><br>
                        <span style="font-family:sans-serif;">
                           
                        </span>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>

</body>
</html>