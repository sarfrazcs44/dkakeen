<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
             
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Home Images</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       <!-- <li>
                                        <a href="<?php echo base_url('cms/homeimage/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Home Images</button>
                                        </a>
                                        </li>-->
                                       
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title Eng</th>
                                            <th>Title Arb</th>
                                            <th>Is Active</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($homeimages){
                                                foreach($homeimages as $homeimage){ ?>
                                                    <tr id="<?php echo $homeimage->homeimage_id;?>">
                                                    <th><img src="<?php echo base_url($homeimage->image);?>" alt="image"
                                                         class="img-responsive thumb-sm"/>
                                                    </th>
                                                    <th><?php echo $homeimage->title_en; ?></th>
                                                    <th><?php echo $homeimage->title_ar; ?></th>
                                                    <th><?php echo ($homeimage->is_active == 1 ? 'Yes' : 'No'); ?></th> 
                                                    <th><?php echo $homeimage->created_at; ?></th>
                                                    <th><?php echo $homeimage->updated_at; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/homeimage/edit/'.$homeimage->homeimage_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                       <!-- <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $homeimage->homeimage_id;?>','cms/homeimage/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>-->
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
@import url("https://fonts.googleapis.com/css?family=Cairo");                
                    table.table-bordered.dataTable tbody tr th:nth-child(3) {
                         font-family: "Cairo";
}
                </style>