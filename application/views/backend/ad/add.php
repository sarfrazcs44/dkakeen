<?php
$option = '<option value="">Choose Product</option>';
  if($products){
      
      foreach($products as $product){
          $option .= '<option value="'.$product['product_id'].'">'.$product['product_title_en'].'</option>';
      }
  }
    
?>






<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Ad</h4>
                               
                                <form action="<?php echo base_url();?>cms/ad/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="save">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div>
                                    <div class="form-group">
                                        <label for="heard">Choose Product :</label>
                                        <select id="heard" name="product_id" class="form-control" required="">
                                            <?php echo $option; ?>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                     <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="ad_title_en" id="title-eng" required value="">
                                     </div>
                                    
                                    <div class="form-group">
	                                   <label>Description Eng :</label>
                                        <textarea class="form-control" rows="5" name="ad_description_en"></textarea>
	                                               
	                               </div>
                                     
                                    <div class="form-group">
                                    <label for="image">Please Choose Image :</label>
                                    <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="ad_title_ar" id="title-arb" required="" value="">
                                     </div>
                                    <div class="form-group">
	                                   <label>Description Arb :</label>
                                        <textarea class="form-control arabic-cms-fields" rows="5" name="ad_description_ar"></textarea>
	                                               
	                               </div>
                                    
                                    <div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" checked="" name="is_active">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
