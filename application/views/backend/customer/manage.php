<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Gender</th>
                                <th>City</th>
                                <th>Country</th>
                                <th>Status</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($customers) {
                                foreach ($customers as $customer) { ?>
                                    <tr id="<?php echo $customer->user_id; ?>">
                                        <th><?php echo $customer->full_name; ?></th>
                                        <th><?php echo $customer->username; ?></th>
                                        <th><?php echo $customer->email; ?></th>
                                        <th><?php echo $customer->phone; ?></th>
                                        <th><?php echo $customer->gender; ?></th>
                                        <th><?php echo $customer->city; ?></th>
                                        <th><?php echo $customer->country; ?></th>
                                        <th><?php echo ucfirst($customer->status); ?></th>
                                        <th>
                                            <?php
                                            if ($customer->status == 'active') { ?>
                                                <a href="javascript:void(0);"
                                                   onclick="suspendUser('<?php echo $customer->user_id; ?>','cms/customer/action','cms/customer')">
                                                    <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                                        Suspend</button>
                                                </a>
                                            <?php } elseif ($customer->status == 'inactive') { ?>
                                                <a href="javascript:void(0);"
                                                   onclick="activateUser('<?php echo $customer->user_id; ?>','cms/customer/action','cms/customer')">
                                                    <button class="btn btn-icon waves-effect waves-light btn-primary m-b-5">
                                                        Activate</button>
                                                </a>
                                            <?php }
                                            ?>

                                            <?php if($customer->is_verified == 0){ ?>

                                                <a href="javascript:void(0);"
                                                   onclick="verifyUser('<?php echo $customer->user_id; ?>','cms/customer/action','cms/customer')">
                                                    <button class="btn btn-icon waves-effect waves-light btn-primary m-b-5">
                                                        Click To Verify</button>
                                                </a>

                                            <?php } ?>
                                            <a href="javascript:void(0);"
                                               onclick="deleteRecord('<?php echo $customer->user_id; ?>','cms/customer/action','')">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                                    <i class="fa fa-remove"></i></button>
                                            </a>
                                        </th>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->