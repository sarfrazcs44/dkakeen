<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Site Settings</h4>
                               
                                <form action="<?php echo base_url();?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="id" value="<?php echo $result->id;?>">
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    

                                      <div class="form-group">
                                       <label for="vat">Vat % * :</label>
                                       <input type="text" class="form-control" name="vat" id="vat" required value="<?php echo $result->vat; ?>">
                                     </div>
                                    
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
