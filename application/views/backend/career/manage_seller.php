<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">


			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title">Become A Seller Applications</h4>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->


			<div class="row">
				<div class="col-sm-12">
					<div class="card-box table-responsive">

						<h4 class="m-t-0 header-title">&nbsp;</h4>

						<div class="alert" id="validatio-msg" style="display: none;"></div>
						<table id="datatable-responsive"
						       class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
						       width="100%">
							<thead>
							<tr>
								<th>Company Name</th>
								<th>Director Name</th>
                                                                <th>Mobile</th>
                                                                <th>Email</th>
								<th>Tel</th>
								<th>Fax</th>
								<th>Actions</th>
							</tr>
							</thead>
							<tbody>
							<?php if($careers){
								foreach($careers as $career){ ?>
									<tr id="<?php echo $career->id;?>">
										<th><?php echo $career->company_name;?></th>
										<th><?php echo $career->director_name; ?></th>
										<th><?php echo $career->mobile ?></th>
										<th><?php echo $career->email ?></th>
                                                                                <th><?php echo $career->phone ?></th>
                                                                                <th><?php echo $career->fax ?></th>
										<th>
                                            <?php if($career->file != '' && file_exists($career->file)){?>
                                            <a href="<?php echo base_url() . $career->file ?>" download>Download</a> |
                                            <?php } ?>
                                         <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $career->id;?>','cms/career/delete','')"> 
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                        </th>
									</tr>
									<?php
								}

							}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>



		</div> <!-- container -->

	</div> <!-- content -->