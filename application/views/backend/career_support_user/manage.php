<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Career Support Users</h4>
                        <ol class="breadcrumb p-0 m-0">

                            <li>
                                <a href="<?php echo base_url('cms/career_support_user/add');?>">
                                    <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Career Support User</button>
                                </a>
                            </li>

                            <!-- <li>
                                 <a href="#">Tables </a>
                             </li>
                             <li class="active">
                                 Responsive Table
                             </li>-->
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>

                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact No.</th>
                                <th>Gender</th>
                                <th>Receive Email At</th>
                                <th>Created At</th>

                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if($career_support_user){
                                foreach($career_support_user as $user){ ?>
                                    <tr id="<?php echo $user->id;?>">

                                        <th><?php echo $user->full_name; ?></th>
                                        <th><?php echo $user->email; ?></th>
                                        <th><?php echo $user->phone; ?></th>
                                        <th><?php echo ucfirst($user->gender); ?></th>
                                        <th><?php echo $user->receive_career_email_at; ?></th>
                                        <th><?php echo $user->created_at; ?></th>

                                        <th>

                                            <a href="<?php echo base_url('cms/career_support_user/edit/'.$user->user_id);?>">
                                                <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                            <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $user->user_id;?>','cms/career_support_user/delete','')">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                        </th>
                                    </tr>
                                    <?php
                                }

                            }
                            ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div> <!-- container -->

    </div> <!-- content -->