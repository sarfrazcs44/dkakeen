<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Support User</h4>

                                <form action="<?php echo base_url();?>cms/support_user/save" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="full_name">Full Name</label>
                                       <input type="text" class="form-control" name="full_name" id="full_name" required value="">
                                     </div>

                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input type="text" class="form-control" name="username" id="username" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone</label>
                                        <input type="tel" class="form-control" name="phone" id="phone" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="complaint_type_id">Support Types</label>
                                        <select class="multiple_dropdown" id="complaint_type_id" name="complaint_type_id[]" multiple="multiple">
                                            <?php foreach ($complain_types_new as $type)
                                            { ?>
                                                <option value="<?php echo $type['complaint_type_id']; ?>"><?php echo $type['complaint_title_en']; ?></option>
                                            <?php }?>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>
                                    <!--<span id="append_here"></span>-->
                                    <!--<div class="form-group" style="overflow:hidden">
                                        <span style="float: left;margin-right: 15px;">Is Active ? &nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input type="checkbox" id="switch7" switch="primary" checked="" name="is_verified">
                                        <input type="checkbox" id="switch7" switch="primary" checked="" name="is_verified">
                                        <label style="display: inline-block;float: left;" for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                       
                                    </div>-->
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <a href="<?php echo base_url('cms/support_user'); ?>"><button type="button" class="btn btn-danger waves-effect waves-light">
                                                Cancel
                                            </button></a>
                                        <!--<button id="add_more" type="button">Add More</button>-->
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

    <script>
            var html = "<select>" +
                <option></option>
                "</select>";
            /*$("#add_more").click(function(){
                alert('here');
                $("#append_here").after("<i>After</i>");
            });*/

            $(document).on('click', '#add_more', function () {
                alert('here');
            });
    </script>
    
