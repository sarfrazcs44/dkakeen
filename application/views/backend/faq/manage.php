<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Faqs</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                        <li>
                                        <a href="<?php echo base_url('cms/faq/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add faq</button>
                                        </a>
                                        </li>
                                       
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>Question Eng</th>
                                            <th>Question Arb</th>
                                            
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($faqs){
                                                foreach($faqs as $faq){ ?>
                                                    <tr id="<?php echo $faq->faq_id;?>">
                                                    
                                                    <th><?php echo $faq->question_en; ?></th>
                                                    <th><?php echo $faq->question_ar; ?></th>
                                                     
                                                    <th><?php echo $faq->created_at; ?></th>
                                                    <th><?php echo $faq->updated_at; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/faq/edit/'.$faq->faq_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $faq->faq_id;?>','cms/faq/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->