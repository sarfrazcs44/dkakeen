<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Edit faq</h4>
                               
                                <form action="<?php echo base_url();?>cms/faq/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="faq_id" value="<?php echo $faq_id;?>">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                   
                                    <div class="form-group">
                                       <label for="title-eng">Question Eng * :</label>
                                       <input type="text" class="form-control" name="question_en" id="title-eng" required value="<?php echo $result->question_en; ?>">
                                     </div>
                                     
                                    <div class="form-group">
	                                   <label>Answer Eng : </label>
                                        <textarea class="form-control" rows="5" name="answer_en"><?php echo $result->answer_en; ?></textarea>
	                                               
	                               </div>
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Question Arb * :</label>
                                       <input type="text" class="form-control arabic-cms-fields" name="question_ar" id="title-arb" required="" value="<?php echo $result->question_ar; ?>">
                                     </div>
                                    
                                    <div class="form-group">
	                                   <label>Answer Arb : </label>
                                        <textarea class="form-control arabic-cms-fields" rows="5" name="answer_ar"><?php echo $result->answer_ar; ?></textarea>
	                                               
	                               </div>
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
