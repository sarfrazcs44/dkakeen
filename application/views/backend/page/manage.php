<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
                            <?php
                            if ($parent_id == 9)
                            {
                                $page_title = 'Catelogs';
                            }elseif ($parent_id == 14)
                            {
                                $page_title = 'Projects';
                            }elseif ($parent_id == 15)
                            {
                                $page_title = 'Events';
                            }else{
                                $page_title = 'Pages';
                            }
                            ?>
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title"><?php echo $page_title;?></h4>
                                    <ol class="breadcrumb p-0 m-0">
                                       <?php if($parent_id == 9){?>
                                        <li>
                                        <a href="<?php echo base_url('cms/pages/add/9');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Download Center Pdf</button>
                                        </a>
                                        </li> 
                                        <?php }elseif ($parent_id == 14) { ?>
                                        <a href="<?php echo base_url('cms/pages/add/14');?>">
                                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Project</button>
                                        </a>
                                         <?php }elseif ($parent_id == 15){ ?>
                                            <a href="<?php echo base_url('cms/pages/add/15');?>">
                                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Event</button>
                                        </a>
                                         <?php } ?>
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            
                                            <th>Title Eng</th>
                                            <th>Title Arb</th>
                                            
                                            
                                            <th>Updated At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($pages){
                                                foreach($pages as $page){ 
                                                    if($page->page_id != 10){
                                            ?>
                                                    <tr id="<?php echo $page->page_id;?>">
                                                    
                                                    <th><?php echo $page->page_title_en; ?></th>
                                                    <th><?php echo $page->page_title_ar; ?></th>
                                                    
                                                    
                                                    <th><?php echo $page->updated_at; ?></th>
                                                        
                                                    <th>
                                                       <?php if($page->parent_id == 9){ ?>
                                                        <a href="<?php echo base_url('cms/pages/editCatelog/'.$page->page_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $page->page_id;?>','cms/pages/action','')">
                                                            <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i></button></a>
                                                    
                                                    <?php
                                                }elseif ($page->parent_id == 14){ ?>
                                                           <a href="<?php echo base_url('cms/pages/editProject/'.$page->page_id);?>">
                                                               <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>

                                                           <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $page->page_id;?>','cms/pages/action','')">
                                                               <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i></button></a>
                                                       <?php }elseif ($page->parent_id == 15){ ?>
                                                        <a href="<?php echo base_url('cms/pages/editEvent/'.$page->page_id);?>">
                                                               <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>

                                                           <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $page->page_id;?>','cms/pages/action','')">
                                                               <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i></button></a>
                                                       <?php }else{ ?>
                                                  <a href="<?php echo base_url('cms/pages/edit/'.$page->page_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>  
                                             <?php   }
                                                        ?>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
                                                }
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
@import url("https://fonts.googleapis.com/css?family=Cairo");                
                    table.table-bordered.dataTable tbody tr th:nth-child(2) {
                         font-family: "Cairo";
}
                </style>