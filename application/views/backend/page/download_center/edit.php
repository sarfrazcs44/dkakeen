 <link href="<?php echo base_url();?>assets/plugins/summernote/summernote.css" rel="stylesheet" />
<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="header-title m-t-0 m-b-30">Edit Catelog</h4>
                                        </div>
                                        
                                    </div>
                                <form action="<?php echo base_url();?>cms/pages/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
                                    <input type="hidden" name="form_type" value="update">
                                    <input type="hidden" name="page_id" value="<?php echo $result->page_id;?>">
                                    <input type="hidden" name="parent_id" value="<?php echo $result->parent_id;?>">
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                
                                    <div class="form-group">
                                       <label for="title-eng">Title Eng * :</label>
                                       <input type="text" class="form-control" name="page_title_en" id="title-eng" required value="<?php echo $result->page_title_en;?>">
                                     </div>
                                    
                                    <?php if($result->image != ''){ ?>
                                    <a href="<?php echo base_url($result->image);?>" target="_blank"><img src="<?php echo base_url('assets/images/pdf.jpg');?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/></a>
                                    <?php } ?>
                                    <div class="form-group">
                                            <label for="image">Please Choose File :</label>
                                            <input type="file" class="filestyle" id="image" name="image[]" data-placeholder="No Image">
                                    </div>
                                          
                                    
                                    
                                    
                                    
                                    <div class="form-group">
                                       <label for="title-arb">Title Arb * :</label>
                                       <input type="text" class="form-control  arabic-cms-fields" name="page_title_ar" id="title-arb" required="" value="<?php echo $result->page_title_ar;?>">
                                     </div>
                                    
                                    
                                    
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        
                                        
                                        
                                    </div>
                                     </div>
                                    </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
    
