<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Products</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                        <li>
                                        <a href="<?php echo base_url('cms/product/add');?>">
                                           <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5">Add Product</button>
                                        </a>
                                        </li>
                                       
                                       <!-- <li>
                                            <a href="#">Tables </a>
                                        </li>
                                        <li class="active">
                                            Responsive Table
                                        </li>-->
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Title Eng</th>
                                            
                                            <th>Model Eng</th>
                                            
                                            <th>Price</th>
                                            <th>Is Active</th>
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($products){
                                                foreach($products as $product){ 
                                                 $images = getProductImages($product->product_id);
                                                 $image = '';    
                                                 if($images)
                                                 {
                                                     $image = $images[0]->product_image;
                                                 }
                                            ?>
                                                    <tr id="<?php echo $product->product_id;?>">
                                                    <th><img src="<?php echo base_url().$image;?>" alt="image"
                                                         class="img-responsive thumb-sm"/>
                                                    </th>
                                                    <th><?php echo $product->product_title_en; ?></th>
                                                   
                                                    
                                                    <th><?php echo $product->model_en; ?></th> 
                                                    <th><?php echo $product->price; ?></th> 
                                                    <th><?php echo ($product->is_active == 1 ? 'Yes' : 'No'); ?></th> 
                                                    <th><?php echo $product->created_at; ?></th>
                                                    <th><?php echo $product->updated_at; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/product/edit/'.$product->product_id);?>">
                                                            <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-pencil"></i></button></a>
                                                        <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $product->product_id;?>','cms/product/action','')">
                                                        <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5"> <i class="fa fa-remove"></i> </button></a>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->
                <style>
                    .thumb-sm {
                        height: 60px;
                        width: 60px;
                    }
                </style>