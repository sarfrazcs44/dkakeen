<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Chats</h4>
                        <ol class="breadcrumb p-0 m-0">

                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">

                        <h4 class="m-t-0 header-title">&nbsp;</h4>

                        <div class="alert" id="validatio-msg" style="display: none;"></div>
                        <table id="datatable-responsive-nosort"
                               class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <!--<th>Message</th>-->
                                <th>Status</th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody id="newChatTbody">
                            <?php
                            $LastChatId = 0;
                            if ($chat_requests) {
                                foreach ($chat_requests as $request) {
                                    ?>
                                    <tr class="chat_row" id="<?php echo $request['id']; ?>">
                                        <td class="user"><?php echo $request['username']; ?></td>
                                        <td><?php echo $request['email']; ?></td>
                                        <td class="subject"><?php echo $request['subject']; ?></td>
                                        <!--<td class="chat_mess"><?php // echo $request['message'];
                                        ?></td>-->
                                        <td>
                                            <button type="button"
                                                    class="btn btn-default btn-rounded w-md waves-effect m-b-5 QueuedMsg<?php echo $request['id']; ?> <?php echo($request['is_closed'] == 0 ? 'NotClosed' : ''); ?>"
                                                    disabled="disabled"><?php echo($request['is_closed'] == 1 ? 'Closed' : '0 Queued Message'); ?></button>
                                        </td>
                                        <td>
                                            <button data-chat_request_id="<?php echo $request['id']; ?>"
                                                    data-customer_full_name="<?php echo $request['username']; ?>"
                                                    data-customer_email="<?php echo $request['email']; ?>"
                                                    type="button"
                                                    class="btn btn-primary waves-effect w-md waves-light m-b-5 showChatBTN show_friend_messages chat_button startchat<?php echo $request['id']; ?>"
                                                    data-attr="<?php echo $request['id']; ?>" <?php echo($request['is_closed'] == 1 ? 'disabled="disabled"' : ''); ?>>
                                                Start Chat
                                            </button>
                                            | <a href="javascript:void(0);"
                                                 onclick="deleteRecord('<?php echo $request['id']; ?>','cms/myChat/delete','')">
                                                <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5">
                                                    <i class="fa fa-remove"></i></button>
                                            </a></td>
                                    </tr>
                                    <?php
                                    $LastChatId = ($LastChatId < $request['id'] ? $request['id'] : $LastChatId);
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <input type="hidden" id="getChatId" value="<?php echo $LastChatId; ?>">


        </div> <!-- container -->

    </div> <!-- content -->

    <!-- sample modal content -->
    <div id="chatDiv" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content card-box">
                <div class="modal-header">
                                                    <span class="cus-span">
                                                        <span class="un-span un-span-1"><i class="fa fa-user user-icon"
                                                                                           aria-hidden="true"></i></span>
                                                        <span class="show_username"></span>
                                                        <span><button type="button"
                                                                      class="btn btn-default btn-rounded w-md waves-effect m-b-5 closeChat"
                                                                      style="float: right;">End Chat</button></span>
                                                    </span>
                    <span class="show_subject" style="display:inherit;"></span>
                </div>
                <div class="modal-body">
                    <div class="chat-conversation">
                        <ul class="show_messages conversation-list slimscroll-alt"
                            style="height: 380px; min-height: 332px;">
                        </ul>
                        <div class="row">
                            <div class="col-sm-9 chat-inputbar">
                                <textarea placeholder="Type Your Message..." name="message" class="message"
                                          style="width: 100%; height: 40px;"></textarea>
                            </div>
                            <div class="col-sm-3 chat-send">
                                <button type="submit"
                                        class="btn btn-md btn-info btn-block waves-effect waves-light SendMessage">Send
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <script>
        var clickable_id = 0;
        <?php if (isset($_REQUEST['r']) && $_REQUEST['r'] > 0)
        { ?>
        clickable_id = <?php echo $_REQUEST['r']; ?>;
        <?php } ?>
    </script>
