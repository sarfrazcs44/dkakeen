<div class="content-page" style="margin-top:10px;">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30">Add Delivery User</h4>

                                <form action="<?php echo base_url();?>cms/delivery_user/save" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                                    <div class="alert" id="validatio-msg" style="display: none;">
                                    </div> 
                                     <div class="form-group">
                                       <label for="full_name">Full Name *</label>
                                       <input type="text" class="form-control" name="full_name" id="full_name" required value="">
                                     </div>

                                    <div class="form-group">
                                        <label for="username">Username *</label>
                                        <input type="text" class="form-control" name="username" id="username" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">Phone *</label>
                                        <input type="tel" class="form-control" name="phone" id="phone" required value="">
                                    </div>

                                     <div class="form-group">
                                        <label for="dob">Date Of Birth *</label>
                                        <input type="date" class="form-control" name="dob" id="dob" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="gender">Gender *</label>
                                        <select class="form-control" id="gender" name="gender">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                        <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password *</label>
                                        <input type="password" class="form-control" name="password" id="password" required value="">
                                    </div>


                                    <div class="form-group">
                                        <label for="address">address *</label>
                                        <textarea  class="form-control" name="address" id="address" required></textarea> 
                                    </div>

                                    <div class="form-group">
                                        <label for="iqama_number">Iqama Number *</label>
                                        <input type="text" class="form-control" name="iqama_number" id="iqama_number" required value="">
                                    </div>


                                    <div class="form-group">
                                        <label for="iqama_expiry">Iqama Expiry *</label>
                                        <input type="date" class="form-control" name="iqama_expiry" id="iqama_expiry" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="driving_license_no">Driving License No *</label>
                                        <input type="text" class="form-control" name="driving_license_no" id="driving_license_no" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="driving_license_expiry">Driving License Expiry *</label>
                                        <input type="date" class="form-control" name="driving_license_expiry" id="driving_license_expiry" required value="">
                                    </div>

                                    <div class="form-group">
                                    <label for="driving_license_img">Driving License Image :</label>
                                    <input type="file" class="filestyle" id="driving_license_img" name="driving_license_img[]" data-placeholder="No Image" required="">
                                    </div>


                                     <div class="form-group">
                                        <label for="vehicle_make">Vehicle Make *</label>
                                        <input type="text" class="form-control" name="vehicle_make" id="vehicle_make" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="vehicle_model">Vehicle Model *</label>
                                        <input type="text" class="form-control" name="vehicle_model" id="vehicle_model" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="registration_no">Registration No *</label>
                                        <input type="text" class="form-control" name="registration_no" id="registration_no" required value="">
                                    </div>

                                    <div class="form-group">
                                    <label for="registration_license_img">Registration License Image :</label>
                                    <input type="file" class="filestyle" id="registration_license_img" name="registration_license_img[]" data-placeholder="No Image" required="">
                                    </div>

                                    <div class="form-group">
                                        <label for="emergency_contact">Emergency Contact *</label>
                                        <input type="text" class="form-control" name="emergency_contact" id="emergency_contact" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="iban">IBAN *</label>
                                        <input type="text" class="form-control" name="iban" id="iban" required value="">
                                    </div>

                                    <div class="form-group">
                                        <label for="bank_name">Bank Name *</label>
                                        <input type="text" class="form-control" name="bank_name" id="bank_name" required value="">
                                    </div>

                                    <div class="form-group">
                                    <label for="profile_pic">Profile Pic :</label>
                                    <input type="file" class="filestyle" id="profile_pic" name="profile_pic[]" data-placeholder="No Image" required="">
                                    </div>

                                    <div class="form-group">
                                        <label for="city">City</label>
                                    <select class="multiple_dropdown" id="city" name="city_id" required>
                                        <option value="">Select City</option>
                                        <?php foreach ($ksa_cities as $ksa_city)
                                        { ?>
                                            <option value="<?php echo $ksa_city->id; ?>"><?php echo $ksa_city->eng_name; ?></option>
                                        <?php }?>
                                    </select>
                                    <i class="fa fa-chevron-down"></i>
                                    </div>
                                    <div class="form-group">
                                        <label for="branch_id">Branch</label>
                                    <select class="multiple_dropdown" id="branch_id" name="branch_id" required>
                                        <option value="">Select Branch</option>

                                        <?php if($branches){
                                            foreach ($branches as $value)
                                        { ?>
                                            <option value="<?php echo $value['user_id']; ?>" ><?php echo $value['full_name']; ?></option>
                                        <?php } } ?>
                                    </select>
                                    <i class="fa fa-chevron-down"></i>
                                    </div>

                                    <!--<div class="form-group">
                                        <label for="cities">Districts</label>
                                    <select class="multiple_dropdown" id="districts" name="districts[]" multiple="multiple" required>
                                        
                                    </select>
                                    <i class="fa fa-chevron-down"></i>
                                    </div>-->
                                    <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            Submit
                                        </button>
                                        <a href="<?php echo base_url('cms/support_user'); ?>"><button type="button" class="btn btn-danger waves-effect waves-light">
                                                Cancel
                                            </button></a>
                                        <!--<button id="add_more" type="button">Add More</button>-->
                                        
                                        
                                    </div>
                                     </div>
                                 </form>
                                </div>
                            </div> <!-- end col -->

                            
                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->

    <script>
            var html = "<select><option></option></select>";
            /*$("#add_more").click(function(){
                alert('here');
                $("#append_here").after("<i>After</i>");
            });*/

            $(document).on('click', '#add_more', function () {
                alert('here');
            });
    </script>
    
