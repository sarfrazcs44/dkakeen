<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">


			<div class="row">
				<div class="col-xs-12">
					<div class="page-title-box">
						<h4 class="page-title" style="margin-right: 20px; ">Ticket #<?php echo $ticket_id ?></h4>
                        <?php if($ticket->status == 'open') { ?>
                        <a href="#">
                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 updateStatus" data-ticketid="<?php echo $ticket_id ?>"> Close Ticket</button>
                        </a>
                        <?php } ?>
                        <a href="<?php echo base_url().'cms/support/share_chat/'.$ticket_id; ?>" target="_blank"><button type="button" class="btn btn-success waves-effect w-md waves-light m-b-5"> Share Chat</button></a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<!-- end row -->


			<div class="row">
				<div class="col-sm-12">

						<div class="col-lg-6">
                            <div class="card-box">
							<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0">
								<thead>
                                    <tr>
                                        <th colspan="2" class="th_heading">Ticket Details</th>
                                    </tr>
								</thead>
								<tbody>
								<?php if($ticket) { ?>
									<tr>
										<td style="width: 120px;">Ticket Type</td>
										<td><?php $ticket_type_parent = ticket_type_detail($ticket->ticket_type); echo $ticket_type_parent->complaint_title_en; ?></td>
									</tr>
									<tr>
										<td>Status</td>
										<td>
											<?php $status_class = ($ticket->status == 'open') ? "ticket_status_open" : "ticket_status_closed" ?>
											<span class="<?php echo $status_class ?>"><?php echo ucfirst($ticket->status) ?></span>
										</td>
									</tr>
									<!--<tr>
										<td>Product</td>
										<td><?php /*echo ucfirst($ticket->product); */?></td>
									</tr>-->

                                    <tr>
                                        <td>Complain</td>
                                        <td><?php $ticket_type_child = ticket_type_detail($ticket->type_id); echo $ticket_type_child->complaint_title_en; ?></td>
                                    </tr>

									<?php if($ticket->ticket_type == 'visit' && $ticket->visit_time) { ?>
                                        <tr>
                                            <td>Visit Time</td>
                                            <td><?php echo date('d F, Y G:i', $ticket->visit_time); ?></td>
                                        </tr>
									<?php }?>

                                    <tr>
                                        <td>Location</td>
                                        <td><a href="#" class="map_modal" data-lat="<?php echo $ticket->lat ?>" data-lng="<?php echo $ticket->lng ?>" data-toggle="modal" data-target="#locationModal"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $ticket->location; ?></a></td>
                                    </tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
                    </div>


                        <div class="col-lg-6">
                            <div class="card-box">
							<table class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0">
								<thead>
								<tr>
									<th colspan="2" class="th_heading">Client Details</th>
								</tr>
								</thead>
								<tbody>
								<?php if($ticket) { ?>
									<tr>
										<td>Client Name</td>
										<td><?php echo ucfirst($ticket->full_name); ?></td>
									</tr>
									<tr>
										<td>Email</td>
										<td><?php echo ucfirst($ticket->email); ?></td>
									</tr>
									<tr>
										<td>City</td>
										<td><?php echo ucfirst($ticket->city); ?></td>
									</tr>
									<tr>
										<td>Phone</td>
										<td><?php echo ucfirst($ticket->phone); ?></td>
									</tr>

								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <div class="card-box">
                        <h4 class="m-t-0 m-b-20 header-title"><b>Support Chat</b></h4>

                        <div class="chat-conversation">
                            <ul class="conversation-list slimscroll-alt" style="height: 380px; min-height: 332px;">
                                <?php if($chat) { ?>
                                <?php foreach ($chat as $msg ) { ?>

                                <?php $even_odd = ($msg->user_id == $admin['user_id']) ? "even" : "odd" ?>
                                    <li class="item_message clearfix <?php echo $even_odd; ?>" data-ticket_id="<?php echo $ticket->ticket_id?>" data-comment_id="<?php echo $msg->comment_id ?>">
                                        <div class="chat-avatar">
                                            <span><strong><?php echo ($even_odd == "even") ? "Support: " : "Client: " ?></strong></span>
                                            <i><?php echo date("h:i a", strtotime($msg->created_at));?></i>
                                        </div>
                                        <div class="conversation-text">
                                            <div class="ctext-wrap">
    <!--                                            <i>John Deo</i>-->
                                                <p><?php echo $msg->message ?></p>
                                            </div>
                                        </div>
                                    </li>

                                  <?php } ?>
                                <?php } ?>

                            </ul>

                        <?php if($ticket->status == "open"){ ?>
                            <div class="row">
                                <div class="col-sm-9 chat-inputbar">
                                    <input type="text" id="chatMsgBox" class="form-control chat-input" placeholder="Enter your message">
                                </div>
                                <div class="col-sm-3 chat-send">
                                    <button type="submit"
                                            class="btn btn-md btn-info btn-block waves-effect waves-light sendChat"
                                            data-ticketId="<?php echo $ticket->ticket_id?>">Send
                                    </button>
                                </div>
                            </div>
                            <div class="loader"></div>
                        </div>
                        <?php } ?>


                    </div>
                </div>
            </div>



		</div> <!-- container -->

	</div> <!-- content -->

	<div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModal" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                
				<div class="modal-header">
                    <a  target="_blank" href="https://www.google.com/maps?q=<?php echo $ticket->lat ?>,<?php echo $ticket->lng ?>">View map</a>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="map_with_pin"></div>
				</div>
			</div>
		</div>
	</div><!--/modal-->


