<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Order Detail </h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <!-- <div class="panel-heading">
                                        <h4>Invoice</h4>
                                    </div> -->
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h3> <?php echo $order_items[0]['full_name'];?></h3>
                                            </div>
                                            <div class="pull-right">
                                                <h4>Track ID # <br>
                                                    <strong><?php echo $order_items[0]['order_track_id'];?></strong>
                                                </h4>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <div class="pull-left m-t-30">
                                                    <address>
                                                      <strong><?php echo $order_items[0]['city'];?>, <?php echo $order_items[0]['country'];?></strong><br>
                                                      <b>Building No</b> : <?php echo $order_items[0]['building_no'];?><br>
                                                      <b>Floor no:</b> <?php echo $order_items[0]['floor_no'];?>, <b>Appartment no:</b><?php echo $order_items[0]['apartment_no'];?><br>
                                                     <b>District:</b> <?php echo $order_items[0]['district'];?><br>
                                                     <b>Address:</b><?php echo $order_items[0]['location'];?><br>
                                                      <abbr title="Phone">P:</abbr> <?php echo $order_items[0]['phone'];?>
                                                      </address>
                                                </div>
                                                <div class="pull-right m-t-30">
                                                    <p><strong>Transaction ID: </strong> <?php echo $order_items[0]['transaction_id'];?></p>
                                                    <p><strong>Order Date: </strong> <?php echo date('d-m-Y',strtotime($order_items[0]['order_date']));?></p>
                                                    <p><strong>Order Status: </strong> <span class="label label-danger order_status"><?php echo $order_items[0]['status'];?></span></p>
                                                     <?php if($order_items[0]['is_order_mark_as_return'] == 1){ ?>
                                                        <p> <span class="label label-danger ">Order Return</span></p>
                                                     <?php } ?>
                                                    <p><strong>Order ID: </strong> #<?php echo $order_items[0]['order_id'];?></p>
                                                </div>
                                            </div><!-- end col -->
                                        </div>
                                        <!-- end row -->

                                        <div class="m-h-50"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table m-t-30">
                                                        <thead>
                                                            <tr><th>#</th>
                                                            <th>Item</th>
                                                            
                                                            <th>Quantity</th>
                                                            <th>Unit Cost</th>
                                                            <th>Total</th>

                                                            <?php if($order_items[0]['is_order_mark_as_return'] == 1){ ?>
                                                                <th>Return Product</th>
                                                                <th>Return Quantity</th>
                                                                <th>Return Total</th>

                                                            <?php } ?>

                                                        </tr></thead>
                                                        <tbody>
                                            <?php 
                                                            $total = 0;
                                                            $ordered_itmes = getOrderItems($order_items[0]['order_id']);
                                                            foreach($ordered_itmes as $key => $value){
                                                                $total = $total + ($value['palce_order_price']  * $value['quantity']);?>
                                                            <tr>
                                                                <td><?php echo $key +1 ; ?></td>
                                                                <td><a href="<?php echo base_url('cms/product/edit/'.$value['product_id']);?>" target="_blank"><?php echo $value['product_title_en'];?></a></td>
                                                                
                                                                <td><?php echo $value['quantity']; ?></td>
                                                                <td>SAR <?php echo $value['palce_order_price'];?></td>
                                                                <td>SAR <?php echo ($value['palce_order_price']  * $value['quantity']);?></td>

                                                                <?php if($order_items[0]['is_order_mark_as_return'] == 1){ ?>
                                                                <td><?php echo ($value['is_return'] == 1 ? 'Yes' : 'No');?></td>
                                                                <td><?php echo $value['return_quantity']; ?></td>
                                                                <td>SAR <?php echo ($value['return_quantity'] * $value['palce_order_price']); ?></td>

                                                            <?php } ?>
                                                            </tr>
                                            <?php } ?>                    
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="clearfix m-t-40">
                                                    &nbsp
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                                <p class="text-right">Vat Amount: <?php echo ($order_items[0]['vat_total'] == '' ? 'N/A' : $order_items[0]['vat_total'].' SAR'); ?> </p>
                                                <p class="text-right">Delivery Charges: <?php echo ($order_items[0]['delivery_charges'] == '' ? 'N/A' : $order_items[0]['delivery_charges'].' SAR'); ?> </p>
                                                <p class="text-right"><b>Sub Total:</b> <?php echo $order_items[0]['total_amount']; ?> SAR</p>

                                                <?php if($order_items[0]['is_order_mark_as_return'] == 1){ ?>

                                                <p class="text-right"><b>Total Return:</b> <?php echo $order_items[0]['total_return_amount']; ?> SAR</p>
                                            <?php } ?>
                                                <!--<p class="text-right">Discout: 12.9%</p>
                                                <p class="text-right">VAT: 12.9%</p>
                                                <hr>
                                                <h3 class="text-right">USD 2930.00</h3> -->
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="hidden-print">
                                            <div class="pull-right">
                                                <?php
                                                if ($this->session->userdata['admin']['role_id'] != 2 && $order_items[0]['status'] == 'Dispatched') { ?>
                                                    <button type="button" class="btn btn-success waves-effect"  data-toggle="modal" data-target="#exampleModal">Order delivered to customer</button>
                                                <?php } ?>
                                                <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                                <!--<a href="#" class="btn btn-primary waves-effect waves-light">Submit</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- end row -->



                    </div> <!-- container -->

                </div> <!-- content -->
<!--onclick="markAsDelivered(<?php echo $order_items[0]['order_id'];?>);"-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Order Delivery OTP form</h5>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="<?php echo base_url();?>cms/order/mark_as_delivered" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate=""> 
        
      <div class="modal-body">
       <div class="alert" id="validatio-msg" style="display: none;"></div>
            <input type="hidden" name="order_id" value="<?php echo $order_items[0]['order_id']; ?>">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Enter OTP:</label>
            <input type="text" class="form-control" id="otp" required name="otp" value="">
          </div>
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
  </form>
    </div>
  </div>
</div>              