<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Orders</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Status</th>
                                            
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                             
                                            <th>Actions</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <th><?php echo $order->order_track_id; ?></th>
                                                    <th id="status_<?php echo $order->order_id;?>"><?php echo $order->status; ?></th>
                                                    
                                                    <th><?php echo $order->created_at; ?></th>
                                                    <th><?php echo $order->updated_at; ?></th>
                                                        
                                                    <th>
                                                       
                                                        <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        <select onchange="changeStatus(this.value,'<?php echo $order->order_id;?>')">
                                                        <option disabled="disabled">Change Stauts</option>
                                                        <option value="Received">Received</option>
                                                        <option value="Dispatched">Dispatched</option>
                                                        <option value="Delivered">Delivered</option>
                                                        </select>
                                                        </th> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->