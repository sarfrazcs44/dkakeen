<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Orders</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Status</th>
                                            <th>Transaction ID</th>
                                            <th>Amount</th>
                                            
                                            <th>Created At</th>
                                            <th>Updated At</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <th><?php echo $order->order_track_id; ?></th>
                                                    <th id="status_<?php echo $order->order_id;?>"><?php echo $order->status; ?></th>
                                                     <td><?php echo $order->transaction_id; ?></td>
                                                    <td><?php echo $order->total_amount; ?></td>
                                                    <th><?php echo $order->created_at; ?></th>
                                                    <th><?php echo $order->updated_at; ?></th>
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->