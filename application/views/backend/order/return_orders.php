<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">


                        <div class="row">
							<div class="col-xs-12">
								<div class="page-title-box">
                                    <h4 class="page-title">Orders</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        
                                       
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
							</div>
						</div>
                        <!-- end row -->


                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title">&nbsp;</h4>
                                    
                                    <div class="alert" id="validatio-msg" style="display: none;"></div>
                                    <table id="datatable-responsive"
                                           class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>Order Track ID</th>
                                            <th>Status</th>
                                            <th>Assign To</th>
                                            <th>Transaction ID</th>
                                            <th>Amount</th>
                                            <th>Return Amount</th>

                                            <?php
                                            if ($this->session->userdata['admin']['role_id'] == 1 || $this->session->userdata['admin']['role_id'] == 4) { ?>
                                                <th>Actions</th>
                                            <?php }
                                            ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                    <?php if($orders){
                                                foreach($orders as $order){ ?>
                                                    <tr id="<?php echo $order->order_id;?>">
                                                    
                                                    <td><?php echo $order->order_track_id; ?></td>
                                                    <td><?php echo $order->status; ?></td>
                                                     <?php
                                                            $is_assigned = checkIfOrderAssinged($order->order_id,'return');
                                                            if (!$is_assigned)
                                                            { 

                                                                $is_assigned = 'N/A';
                                                            } 

                                                             ?>
                                                    
                                                    <td id="status_<?php echo $order->order_id;?>"><?php echo $is_assigned;?></td>
                                                    <td><?php echo $order->transaction_id; ?></td>
                                                    <td><?php echo $order->total_amount; ?></td>
                                                    <td><?php echo $order->total_return_amount; ?></td>
                                                    
                                                   
                                                        
                                                    <td>
                                                       
                                                        <a href="<?php echo base_url('cms/order/view/'.$order->order_id);?>">
                                                        <button class="btn btn-icon waves-effect waves-light btn-success m-b-5"> <i class="fa fa-eye"></i></button></a>
                                                        <?php
                                                        $delivery_users = getDeliveryUsersForOrder($order->order_id);
                                                                if (count($delivery_users) > 0)
                                                                { ?>
                                                                    <select  id="assign_delivery" onchange="assignForDelivery(this.value,'<?php echo $order->order_id;?>','return_order')">
                                                                        <option disabled="disabled" selected>Choose user for delivery...</option>
                                                                        <?php
                                                                        foreach ($delivery_users as $delivery_user)
                                                                        {
                                                                            $exploded_data = explode('|', $delivery_user);
                                                                            $user_id = $exploded_data[0];
                                                                            $user_name = $exploded_data[1];
                                                                            ?>
                                                                            <option value="<?php echo $user_id; ?>"><?php echo $user_name; ?></option>
                                                                        <?php }
                                                                        ?>
                                                                    </select>
                                                                <?php }else{ ?>
                                                                    <span>No Delivery Men Assgined To This Order's District</span>
                                                                <?php }
                                                        ?>
                                                        
                                                        </td> 
                                                </tr>
                                            <?php 
                                                }
    
                                            }
                                            ?>
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>



                    </div> <!-- container -->

                </div> <!-- content -->