<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function currentDate()
{
    return date('Y-m-d H:i:s');
}


function checkAdminSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('admin')) {
        return true;

    } else {
        redirect($CI->config->item('base_url') . 'cms/account/login');
    }
}

function checkUserSession()
{
    $CI = &get_Instance();
    if ($CI->session->userdata('user')) {
        return true;

    } else {
        redirect($CI->config->item('base_url') . 'page/login');
    }
}


function getProductImages($product_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_product_image');
    $fetch_by = array();
    $fetch_by['product_id'] = $product_id;
    $result = $CI->Model_product_image->getMultipleRows($fetch_by);
    return $result;
}


function getTotalProduct($user_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_temp_order');
    $result = $CI->Model_temp_order->getTotalProduct($user_id);

    return $result[0]->total;
    //return $result[0]->products_count;
}

function fetchTickets($user_id, $status)
{
    $CI = &get_Instance();
    $CI->load->model('Model_support_tickets');
    $fetch_by = array();
    $fetch_by['user_id'] = $user_id;
    $fetch_by['status'] = $status;
    $result = $CI->Model_support_tickets->getMultipleRows($fetch_by, true);
    return $result;
}

function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
          $data = array();
         $extension=array("jpeg","jpg","png","gif");
         foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
                        $data['ImagePath'] = $path.$file_name; 
                        $this->Model_destination_image->save($data);*/
                    
                }
               
            }
            return true;


    }
    

function getPageImages($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_page_image');
    $fetch_by = array();
    $fetch_by['page_id'] = $page_id;
    $result = $CI->Model_page_image->getMultipleRows($fetch_by);
    return $result;
}

function getPageData($page_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_page');

    $result = $CI->Model_page->get($page_id, false, 'page_id');
    return $result;
}

function getChildPages($parent_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_page');
    $fetch_by = array();
    $fetch_by['parent_id'] = $parent_id;
    $result = $CI->Model_page->getMultipleRows($fetch_by);
    return $result;
}

function dump($data)
{
    echo '<pre>';
    print_r($data);
    exit();
}

function assigned_complaint_types($user_id)
{
    $support_type_text = array();
    $CI = &get_Instance();
    $CI->load->model('Model_support_user');
    $CI->load->model('Model_complaint_type');
    $assigned_support_types = $CI->Model_support_user->getMultipleRows(array('user_id' => $user_id));
    if($assigned_support_types){
        foreach ($assigned_support_types as $support_type) {
                $type_detail = $CI->Model_complaint_type->get($support_type->complaint_type_id, false, 'complaint_type_id');
             $support_type_text[] = $type_detail->complaint_title_en;
        }
    }
    

    if (count($support_type_text) > 0) {
        return implode(', ', $support_type_text);
    } else {
        return 'None';
    }

}

function assigned_complaint_types_arr($user_id)
{
    $support_type_text = array();
    $CI = &get_Instance();
    $CI->load->model('Model_support_user');
    $assigned_support_types = $CI->Model_support_user->getMultipleRows(array('user_id' => $user_id));
    foreach ($assigned_support_types as $support_type) {
        $support_type_text[] = $support_type->complaint_type_id;
    }

    return $support_type_text;
}

function assigned_cities($user_id)
{
    return '';//we have changed this table name
    $user_cities_arr = array();
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $user_cities = $CI->Model_general->getMultipleRows('user_cities', array('user_id' => $user_id));
    foreach ($user_cities as $city) {
        $city_detail = $CI->Model_general->getSingleRow('city', array('id' => $city->city_id));
        $user_cities_arr[] = $city_detail->eng_name;
    }
    return implode(', ', $user_cities_arr);
}

function assigned_districts($user_id)
{
    $user_district_arr = array();
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $user_districts = $CI->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id),false,'district_id');

    if($user_districts){
        foreach ($user_districts as $value) {
        $district_detail = $CI->Model_general->getSingleRow('districts', array('district_id' => $value->district_id));
            if($district_detail){
                $user_district_arr[] = $district_detail->eng_name;
            }
        
        }

        if(!empty($user_district_arr)){
             return implode(', ', $user_district_arr);
        }
       
    }else{
        return '';
    }
    
}

function emailTemplate($data)
{
    return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Msj</title>
    <style type="text/css">

        p{
            margin-bottom:5px !important;
            margin-top:5px !important;
        }

        h4{

            margin-bottom:20px;


        }


        #wrap {
            float: left;
            position: relative;
            left: 50%;
        }

        #content {
            float: left;
            position: relative;
            left: -50%;
        }

        table {
            border-collapse: collapse;
            font-family:sans-serif;
        }

    </style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                        <tr>
                                            <td>
                                                ' . $data . '
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p style="font-family:sans-serif;font-size:14px;">Need Help? <a href="' . base_url() . '" style="color:#015685;text-decoration:none;">Click here</a></p>
            <br />
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">MSJ Security Systems</h4>
                        <span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
                        <span style="font-family:sans-serif;">
                            <a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">www.msj.com.sa</a>
                        </span>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>

</body>
</html>';
}


function emailTemplateBK($data)
{


    return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>MSJ</title>
	
<style>
/* ------------------------------------- 
		GLOBAL 
------------------------------------- */
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
}


/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #ebebeb;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}


/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 10px; 
	font-weight: normal; 
	font-size:14px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* ------------------------------------- 
		SIDEBAR 
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}





/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}


.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}
	
	table.social div[class="column"] {
		width:auto!important;
	}

}
</style>

</head>
 
<body bgcolor="#FFFFFF">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#216f7d">
	<tr>
		<td></td>
		<td class="header container">
			
				<div class="content">
					<table bgcolor="#216f7d">
					<tr>
						<td><img src="' . base_url() . 'assets/front/images/logo.png"/></td>
						<td align="right"><h6 class="collapse" style="color:#fff;"></h6></td>
					</tr>
				</table>
				</div>
				
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<div class="content">
			<table>
				<tr>
					<td>
						
						' . $data . '
												
						<br/>
						<br/>							
												
						<!-- social & contact -->
						<table class="social" width="100%">
							<tr>
								<td>
									
									<!--- column 1 -->
									<table align="left" class="column">
										<tr>
											<td>				
												
												<h5 class="">Connect with Us:</h5>
												<p class=""><a href="#" class="soc-btn fb">Facebook</a> 
												<a href="#" class="soc-btn tw">Twitter</a> 
												<a href="#" class="soc-btn gp">Google+</a></p>
						
												
											</td>
										</tr>
									</table><!-- /column 1 -->	
									
									<!--- column 2 -->
									<table align="left" class="column">
										<tr>
											<td>				
																			
												<h5 class="">Contact Info:</h5>												
												<p>+(76) 0001010110<br>info@msj.com</p>
												
                
											</td>
										</tr>
									</table><!-- /column 2 -->
									
									<span class="clear"></span>	
									
								</td>
							</tr>
						</table><!-- /social & contact -->
					
					
					</td>
				</tr>
			</table>
			</div>
									
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap">
	<tr>
		<td></td>
		<td class="container">
			
				<!-- content -->
				<div class="content">
				<table>
				<tr>
					<td align="center">
						<p>
							
						</p>
					</td>
				</tr>
			</table>
				</div><!-- /content -->
				
		</td>
		<td></td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>';
}


function sendEmail($data = array(), $files = false)
{


    $CI = &get_Instance();
    $config["protocol"] = "SSL";
    $config["smtp_host"] = "smtp.zoho.com";
    $config["smtp_port"] = 465;
    $config["smtp_user"] = "dkakeen@schopfen.com";
    $config["smtp_pass"] = "h5ZU+:$6M-:5n:Qs";
    $config["charset"] = "utf-8";
    $config["mailtype"] = "html";
//print_rm($data);
    
    $CI->load->library('email', $config);
    
   
    $CI->email->from('dkakeen@schopfen.com');
    $CI->email->to($data['to']);
    $CI->email->subject($data['subject']);
    $CI->email->message($data['body']);
    $CI->email->set_mailtype('html');


    if ($files) {
        foreach ($files as $file) {
            $CI->email->attach(base_url() . $file);
        }
    }


    if ($CI->email->send()) {
        //echo 'yes';exit();
        return true;

    } else {
        // show_error($CI->email->print_debugger());
        //echo 'no';exit();
        return false;
    }

}

function getQueuedChat()
{
    $CI = &get_Instance();
    $CI->load->model('Model_chat_request');
    $Chat_request = $CI->Model_chat_request->getMultipleRows(array('is_admin' => '0', 'is_closed' => '0'), true);
    return $Chat_request;
}


function sendSMS($data)
{

    $curl = curl_init();
    $data['sms'] = str_replace(' ', '%20', $data['sms']);
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'http://www.3m-sms.com/api.php?comm=sendsms&user=M.S.J&pass=123123&to=' . $data['to'] . '&message=' . $data['sms'] . '&sender=MSJ-AD',
        CURLOPT_USERAGENT => 'Codular Sample cURL Request'
    ));

    $resp = curl_exec($curl);

    curl_close($curl);
    /*$email['from'] = 'np-reply@msj.com';
    $email['to'] = 'bilal.e@zynq.net';
    $email['subject'] = 'Send SMS Response';
    $email['body'] = $resp;
    sendEmail($email);*/
    //print_r($resp);exit;
    return true;
}

function RandomString()
{
    $characters = '0123456789012345678901234567890123456789012345678912';
    $randstring = '';
    for ($i = 0; $i < 4; $i++) {
        $randstring .= $characters[rand(0, 50)];
    }
    return $randstring;
}

function sendPushNotification($data, $ticket_id = '', $status = 0)
{
    $key = 'AAAAQTjjRRA:APA91bGOF-ronCg_8a6aQIvRYhaxRPV0i8x5EF5AvP4XI5gvBrdLSRy6sIYfXsMjwjWKN3aRxDs9pRCZlRBvkdM1Ukj1Wyv31b_-nBapaXNrAKEbxMWsEQ0276PGbNkjHk1xJbF-9UWH';
    $gcm_ios_mobile_reg_key = array();

    $gcm_ios_mobile_reg_key[] = $data['device_token'];

    //$gcm_ios_mobile_reg_key = array('dM8RVb_8DsU:APA91bHm0OsfTWvipMciO0rF5u3axDuf2Eq2g4AwH2T6rwQZCf4njWYnR3EJBSD4luHCTkxjODCFUAx1ZqnzGq1GLQdso3iUhZ4kgpt0Zakpa05iZAN9QAURNdf_Hu0tLcbPr68X-nvH');


    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $gcm_ios_mobile_reg_key, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($data['message']),
            "title" => $data['title'],
            "sound" => "default",
            "ticket_id" => $ticket_id,
            "status" => $status
        )

    );
    //print_r($fields);exit();

    if ($data['device_type'] == 'ios') {
        $res = send_packet_ios($key, $fields);
    } elseif ($data['device_type'] == 'android') {
        $res = send_packet($data['title'], $data['message'], $gcm_ios_mobile_reg_key, $ticket_id, $status);
    }

    return $res;
}


function send_packet_ios($authkey, $fields)
{

    $url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.

    //print_r($fields);exit();
    $headers = array(
        'Authorization: key=' . $authkey,
        'Content-Type: application/json'
    );

    //print_r($fields);exit();
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);

    return $result;
    //end comment date tue march 11 2014
}

function send_packet($title, $message, $registatoin_ids, $ticket_id = '', $status)
{
    $url = 'https://android.googleapis.com/gcm/send';


    $message1 = array("message" => $message, "title" => $title, "ticket_id" => $ticket_id, "status" => $status);

    $fields = array(
        'registration_ids' => $registatoin_ids,
        'data' => $message1
    );
    /*
    $message = array("massage_type" => "notification" , "title" => "Cricket App" ,"message" => "Test Notification Cricket App form astutesolution. This app will provide you live cricket scores and live streaming on different channels.", "date" => $row["created_at"]);

$fields = array(
'registration_ids' => $registatoin_ids,
'data' => $message,
);
*/

    //echo json_encode($fields);

    //AIzaSyDVE9nJSuFiO-uW4hlZF9TZoRceGRkNw98 server key // AIzaSyDjMdq8qZhuTVxDZuaZoCu085RQEsUUME0 browser key
    $headers = array(

        // 'Authorization: key=AIzaSyBS8bcV65GlZ0qsSa53VNPQRAzXY900zmg',

        'Authorization: key=AAAAQTjjRRA:APA91bGOF-ronCg_8a6aQIvRYhaxRPV0i8x5EF5AvP4XI5gvBrdLSRy6sIYfXsMjwjWKN3aRxDs9pRCZlRBvkdM1Ukj1Wyv31b_-nBapaXNrAKEbxMWsEQ0276PGbNkjHk1xJbF-9UWH',
        'Content-Type: application/json'
    );
    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    //print_r($result);exit;


    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }


    curl_close($ch);

    return $result;
}

function getOrderDetail($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_order');
    $result = $CI->Model_order->getOrderDetail($order_id);
    return $result;
}

function getOrderItems($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_order');
    $result = $CI->Model_order->getOrderItems($order_id);
    return $result;
}

// getDetail
function getDetail($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_order');
    $result = $CI->Model_order->getDetail($order_id);
    return $result;
}


function getEmailTemplate($email_template_id){
    $CI = &get_Instance();
    $CI->load->model('Model_email_template');
    $result = $CI->Model_email_template->get($email_template_id,false,'email_template_id');
    return $result;

}

function email_format($content)
{
    $CI = &get_Instance();
    $data['content'] = $content;
    $html = $CI->load->view('emails/general_template', $data, true);
    return $html;
}

function orderTotalSumPaid($order_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_order');
    $ordered_items = $CI->Model_order->getOrderItems($order_id);
    $total = 0;
    foreach ($ordered_items as $item) {
        $total = $total + ($item['palce_order_price'] * $item['quantity']);
    }
    return $total;
}

function getAllKsaCities()
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $ksa_cities = $CI->Model_general->getAll('city');
    return $ksa_cities;
}

function getAllDistricts($city_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $districts = $CI->Model_general->getMultipleRows('districts',array('city_id' => $city_id),true,'asc','eng_name');
    return $districts;
}

function getDeliveryUsersForOrder($order_id)
{
    $CI = &get_Instance();
    $users = array();
    $CI->load->model('Model_general');
    $CI->load->model('Model_order');
    $CI->load->model('Model_address');
    $CI->load->model('Model_user');
    $order_detail = $CI->Model_order->get($order_id, false, 'order_id');
    $address_detail = $CI->Model_address->get($order_detail->address_id, false, 'address_id');
    $fetch_by['user_type'] = 'delivery';
    $fetch_by['district_id'] = $address_detail->district_id;
    $delivery_users_for_city = $CI->Model_general->getMultipleRows('user_districts', $fetch_by);
    foreach ($delivery_users_for_city as $delivery_user) {
        $user_detail = $CI->Model_user->get($delivery_user->user_id, false, 'user_id');
        $users[] = $user_detail->user_id . '|' . $user_detail->full_name;
    }
    return $users;
}

function checkIfOrderAssinged($order_id,$type = 'delivery')
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $assgined = $CI->Model_general->getSingleRow('assigned_orders_for_delivery', array('order_id' => $order_id,'assigned_as' => $type));
    if ($assgined) {
        $user_detail = $CI->Model_general->getSingleRow('users', array('user_id' => $assgined->user_id));
        return $user_detail->full_name;
    } else {
        return false;
    }
}

function convertToEnNumbers($string)
{
    $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;

}

function captchaVerify($siteKey)
{
    $secret = '6LcR41UUAAAAAKEuKHvBrDWvQD6b-E1p8kVQ5atN';
    $data = array(
        'secret' => $secret,
        'response' => $siteKey
    );

    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);
    $captcha_success = json_decode($response);
    return $captcha_success;
}

function getAllUnreadChatsForAdmin()
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $fetch_by = array();
    $fetch_by['is_admin'] = '0';
    $fetch_by['to_be_notified'] = 'yes';
    $fetch_by['is_closed'] = 0;
    $result = $CI->Model_general->getMultipleRows('chat_request', $fetch_by, false, 'DESC');
    $result_count = $CI->Model_general->getRecordsCountWhere('chat_request', $fetch_by);
    $retArr['result'] = $result;
    $retArr['result_count'] = $result_count;
    return $retArr;
}

function getAllUnreadCareerRequestsForAdmin()
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $fetch_by = array();
    $fetch_by['to_be_notified'] = 'yes';
    $result = $CI->Model_general->getMultipleRows('careers', $fetch_by, false, 'DESC');
    $result_count = $CI->Model_general->getRecordsCountWhere('careers', $fetch_by);
    $retArr['result'] = $result;
    $retArr['result_count'] = $result_count;
    return $retArr;
}

function truncate_text($str, $width)
{
    return strtok(wordwrap($str, $width, "...\n"), "\n");
}

function homeVideoURL()
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $result = $CI->Model_general->getSingleRow('home_video', array('id' => 1));
    $video_url = $result->video_url;
    $video = explode('=', $video_url);
    return $video[1];
}

function ticket_type_detail($id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_general');
    $result = $CI->Model_general->getSingleRow('complaint_types', array('complaint_type_id' => $id));
    return $result;
}

function pusher($data, $channel, $event, $debug = false)
{
    require FCPATH . '/vendor/autoload.php';

    $cluster = 'ap2';
    $key = 'cdeb4b42705f7d980ced'; // app key
    $secret = '99443431ecbfd9c927fd';
    $app_id = '683458';

    $options = array(
        'cluster' => $cluster,
        'encrypted' => true
    );

    $pusher = new Pusher\Pusher(
        $key,
        $secret,
        $app_id,
        $options
    );

    $channel = 'MSJ_channel_' . $channel;
    $event = 'MSJ_event_' . $event;

    $response = $pusher->trigger($channel, $event, $data);
    if ($debug) {
        echo '<pre>';
        print_r($response);
        exit();
    }
    return true;
}

function formatted_message($message, $user_id)
{
    $message['created_at'] = date("h:i a", strtotime($message['created_at']));
    $message['who_said'] = ($message['user_id'] == $user_id) ? "Support: " : "Client: ";
    $message['even_odd'] = ($message['user_id'] == $user_id) ? "even" : "odd";
    return $message;
}

function sendNotification($title, $message, $data, $user_id)
{
    $CI = &get_Instance();
    $CI->load->model('Model_user');
    $res = 'Device token not found!';
    $user = $CI->Model_user->get($user_id, true, 'user_id');
    if ($user['device_type'] != '' && $user['device_token'] != '') {
        $token = array($user['device_token']);
        if (strtolower($user['device_type']) == 'android') {
            $res = sendPushNotificationToAndroid($title, $message, $token, $data);
        } elseif (strtolower($user['device_type']) == 'ios') {
            $res = sendPushNotificationToIOS($title, $message, $token, $data);
        }
    }
    return $res;
}

function sendPushNotificationToAndroid($title, $message, $registatoin_ids, $data = array())
{
    $android_fcm_key = 'AAAAssFt9hI:APA91bEFAFOcj1YoOt4sJRDo_bex6i9U0UZxwnF8ccAznbAn2dFQR8aYSpWprcn93jmUz2PMBgLpu1RgTfvW_DTFA8VvVsNOsTkma4rvLmWRZaajGekttO-NF9Mr2T_31yW97O_7rIpn';

    $sendData['title'] = $title;
    $sendData['body'] = $message;
    $url = 'https://fcm.googleapis.com/fcm/send';
    $fields = array(
        "registration_ids" => $registatoin_ids,
        "content_available" => true,
        "priority" => "high",
        "notification" => array
        (
            "title" => $title,
            "body" => $message,
            "sound" => "default"
        ),
        "data" => array
        (
            "body" => $message,
            "notificationKey" => $registatoin_ids,
            "priority" => "high",
            "sound" => "default",
            "notification_data" => $data
        ),
    );

    $headers = array(
        'Authorization:key=' . $android_fcm_key,
        'Content-Type: application/json'
    );

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);

    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

function sendPushNotificationToIOS($title, $message, $register_keys, $data = array())
{
    $ios_fcm_key = 'AAAAssFt9hI:APA91bEFAFOcj1YoOt4sJRDo_bex6i9U0UZxwnF8ccAznbAn2dFQR8aYSpWprcn93jmUz2PMBgLpu1RgTfvW_DTFA8VvVsNOsTkma4rvLmWRZaajGekttO-NF9Mr2T_31yW97O_7rIpn';

    $fields = array(
        //"to" => $gcm_ios_mobile_reg_key,
        "registration_ids" => $register_keys, //1000 per request logic is pending
        "content_available" => true,
        "priority" => "high",
        "notification" => array(
            "body" => strip_tags($message),
            "title" => $title,
            "sound" => "default",
            "priority" => "high"
        ),
        "data" => $data
    );


    $url = 'https://gcm-http.googleapis.com/gcm/send'; //note: its different than android.


    $headers = array(
        'Authorization: key=' . $ios_fcm_key,
        'Content-Type: application/json'
    );


    // Open connection
    $ch = curl_init();

    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

    // Execute post
    $result = curl_exec($ch);
    if ($result === FALSE) {
        // echo 'abc';
        die('Curl failed: ' . curl_error($ch));
    }
    // echo 'cdf';
    //print_r($result);exit();
    // Close connection
    curl_close($ch);
    return $result;
}

function sendChatRequestNotificationToAdminSupportUsers($chat_id)
{
    $CI = &get_Instance();
    $users = $CI->Model_user->getUsers("role_id = 1 OR role_id = 2");
    if (count($users) > 0) {
        foreach ($users as $user) {
            if ($user['role_id'] == 2) { // if user role is 2 (support user), checking his assigned complaint types
                $assigned_complaint_types = assigned_complaint_types($user['user_id']);
                if ($assigned_complaint_types !== 'None') {
                    continue;
                }
            }
            $title = 'Chat Request';
            $message = "Dear " . $user['full_name'] . "\nA new chat request is received";
            $data['chat_id'] = $chat_id;
            $data['type'] = 'chat_request';
            sendNotification($title, $message, $data, $user['user_id']);
        }
    }
}