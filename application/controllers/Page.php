<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller
{

    function __construct()
    {
        parent::__construct();
        
        $this->load->model('Model_page');

        // $this->session->set_userdata('lang','en');
    }

    public function index()
    {
        $data = array();
        $data['result'] = $this->Model_page->get(1,true,'page_id');
        $data['view'] = 'front/pages/home';
        $this->load->view('front/layouts/default', $data);
    }


}
