<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		//checkUserSession();
		$this->load->model('Model_user');
		$this->load->model('Model_product');
		$this->load->model('Model_temp_order');
		$this->load->model('Model_order');
		$this->load->model('Model_order_item');
		$this->load->model('Model_address');
		

	}
	 
	public function index()
	{
		$data = array();
		if(!get_cookie('temp_order_key') && !$this->session->userdata['user']['user_id']){
             redirect(base_url());
             
        }else{
            if(get_cookie('temp_order_key')){
               $user_id = get_cookie('temp_order_key'); 
            }else{
               $user_id = $this->session->userdata['user']['user_id'];
            }
            
            
        }
        
		
		$data['cart_products'] = $this->Model_temp_order->getJoinedData(false,'product_id','products','temp_orders.user_id = '.$user_id.'');

		if(!$data['cart_products']){
            redirect(base_url());
        }

		
        $data['user_id'] = $user_id;//$user['user_id'];
		
		$data['view'] = 'front/checkout/index';
		$data['isLogin'] = TRUE;
		$this->load->view('front/layouts/default',$data);
	}
    
    
    public function delete_order_product()
	{
		if(!get_cookie('temp_order_key') && !$this->session->userdata('user')){
             redirect(base_url());
             
        }
        $delete_by = array();
        $delete_by['temp_order_id'] = $this->input->post('temp_order_id');
        
        $this->Model_temp_order->delete($delete_by);
        
        $success['status']   = 'TRUE';
        $success['reload']   =  TRUE;
        //$success['check_out']   =  TRUE;
        $success['message'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
        
		
	}
    
    
    public function update_order_product()
	{
		if(!get_cookie('temp_order_key') && !$this->session->userdata('user')){
             redirect(base_url());
             
        }
        $update_by = array();
		$update = array();
        $update_by['temp_order_id'] = $this->input->post('temp_order_id');
        $update['product_quantity'] = $this->input->post('product_quantity');
        
        $this->Model_temp_order->update($update,$update_by);
        
        $success['status']   = 'TRUE';
        $success['reload']   = TRUE;
        $success['message']  = lang('updated_successfully');
        
        echo json_encode($success);
        exit;
        
		
	}
    
    
            
            
        
    public function place_order(){
       
        $data = array();
		if(!get_cookie('temp_order_key') && !$this->session->userdata('user')){
            $this->session->set_flashdata('message',lang('no_product_in_cart'));
            redirect(base_url('address'));
        }else{
            if($this->session->userdata('user')){
               $user_id = $this->session->userdata['user']['user_id']; 
            }else{
               $user_id = get_cookie('temp_order_key');
            }
        }
        
        $user = $this->session->userdata('user');
		$data['user'] = $user;
        $fetch_by = array();
		$fetch_by['user_id'] = $user['user_id'];
		
		$cart_products = $this->Model_temp_order->getJoinedData(false,'product_id','products','temp_orders.user_id = '.$user_id.'');
        
        if($cart_products){
            
            $fetch_address_by = array();
            $fetch_address_by['user_id'] = $user['user_id'];
            $fetch_address_by['is_default'] = 1;
            $add_res = $this->Model_address->getWithMultipleFields($fetch_address_by);
            if($add_res){
               $address_id = $add_res->address_id; 
            }else{
                $this->session->set_flashdata('message',lang('set_default_address'));
                redirect(base_url('address?checkout'));
            }
            
            $order_data = array();
            
            $order_data['user_id'] = $user['user_id'];
            $order_data['address_id'] = $address_id;
            $order_data['created_at'] = $order_data['updated_at'] = date('Y-m-d H:i:s');
            $order_data['order_track_id'] = RandomString();
            $insert_id = $this->Model_order->save($order_data);
            if($insert_id > 0){
                $update_order = array();
                $update_order_by = array();
                $update_order_by['order_id'] = $insert_id;
                $update_order['order_track_id'] = $order_data['order_track_id'].$insert_id;
                $this->Model_order->update($update_order,$update_order_by);
                $this->sendThankyouEmailToCustomer($insert_id);
               $order_item_data     = array();
                foreach($cart_products as $product){
                    
                    $order_item_data[] = [ 'order_id' => $insert_id,
                                            'product_id' =>  $product->product_id,
                                            'quantity'  => $product->product_quantity,
                                             'price'    => $product->price
                                         ];
                    
                }
                
                $this->Model_order_item->insert_batch($order_item_data);
                
                $deleted_by = array();
                $deleted_by['user_id'] = $user['user_id'];
                $this->Model_temp_order->delete($deleted_by);
                //delete_cookie('temp_order_key');
                redirect(base_url('checkout/thankyou/'.$update_order['order_track_id']));
                 /*$data['track_order_id'] = $update_order['order_track_id'];
                 $data['view'] = 'front/checkout/thankyou';
		         $data['isLogin'] = TRUE;
		         $this->load->view('front/layouts/default',$data);*/
            }else{
                 
                $this->session->set_flashdata('message',lang('something_went_wrong'));
                redirect(base_url('address?checkout'));
               
            }
            
            
        }else{
            $this->session->set_flashdata('message',lang('no_product_in_cart'));
            redirect(base_url('address'));
        }
        
        
        

		
    }
    
    
    public function thankyou($order_track_id){
        $data = array();
        $data['view'] = 'front/checkout/thankyou';
		$data['isLogin'] = TRUE;
        $data['track_order_id'] = $order_track_id;
		$this->load->view('front/layouts/default',$data); 
    }
    
    
    public function proceed(){
        $data = array();
		if($this->session->userdata('user')){
            $user = $this->session->userdata('user');
            $data['user'] = $user;
            $fetch_by = array();
            $fetch_by['user_id'] = $user['user_id'];

            $data['address'] = $this->Model_address->getMultipleRows($fetch_by);




            $data['user_id'] = $user['user_id'];
            
            $data['view'] = 'front/address/index';
            $data['isLogin'] = TRUE;
            $this->load->view('front/layouts/default',$data);  
        }else{
            $url = 'address?checkout';
            $this->session->set_userdata('url', $url);
            redirect(base_url().'page/login');
        }
    }

    private function sendThankyouEmailToCustomer($order_id)
    {
        $order_details = $this->Model_order->getDetail($order_id);
        $data['order_details'] = $order_details;
        $message = $this->load->view('front/emails/order_confirmation',$data, true);
        $email_data['to'] = $order_details[0]['email'];
        $email_data['subject'] = 'Order received at MSJ : Order # ' . $order_details[0]['order_track_id'];
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = $message;
        sendEmail($email_data);
        return true;
    }
    
    
}