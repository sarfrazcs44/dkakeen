<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_user');
       // $this->load->model('Model_system_language');
        $this->load->model('Model_category');
       // $this->load->model('Model_category_text');
		
	}
	 
    
    public function index($parent_id = 0)
	{
		
        $this->data['view'] = 'backend/category/manage';
        $fetch_by = array();
        $fetch_by['parent_id'] = $parent_id;
        $this->data['parent_id'] = $parent_id;
        if($parent_id != 0){
            $this->data['parent_cat_data'] = $this->Model_category->get($parent_id,true,'category_id');
        }
        $this->data['categories'] = $this->Model_category->getMultipleRows($fetch_by);
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add($parent_id = 0)
	{
		
        $this->data['view'] = 'backend/category/add';
        $this->data['parent_id'] = $parent_id;
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($category_id)
	{
        
        $this->data['result']		 = $this->Model_category->get($category_id,false,'category_id');
		
        if(!$this->data['result']){
           redirect(base_url('cms/category')); 
        }
        $this->data['view'] = 'backend/category/edit';
        
		
		
		
        
		
		$this->data['category_id'] 	 = $category_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('title_ar', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
		
		
		unset($post_data['form_type']);
		$post_data['created_at']    = date('Y-m-d H:i:s');		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
        $post_data['image']         = $this->uploadImage("image", "uploads/category_images/");
        $post_data['image_mbl']     = $this->uploadImage("image_mbl", "uploads/category_images/");
		$insert_id = $this->Model_category->save($post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
            if($post_data['parent_id'] == 0){
              $success['url'] = 'cms/category';  
            }else{
               $success['url'] = 'cms/category/index/'.$post_data['parent_id'];  
            }
			
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
		
		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		if(isset($post_data['is_active'])){
             $post_data['is_active'] = 1;  
        }else{
             $post_data['is_active'] = 0;
        }
       unset($post_data['form_type']);
        if(isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != ''){
            $post_data['image']         = $this->uploadImage("image", "uploads/category_images/");
        }
        if(isset($_FILES['image_mbl']["name"][0]) && $_FILES['image_mbl']["name"][0] != ''){
            $post_data['image_mbl']         = $this->uploadImage("image_mbl", "uploads/category_images/");
        }
        $update_by = array();
		$update_by['category_id'] = $post_data['category_id'];
        unset($post_data['form_type']);
        $this->Model_category->update($post_data,$update_by);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        if($post_data['parent_id'] == 0){
              $success['url'] = 'cms/category';  
        }else{
               $success['url'] = 'cms/category/index/'.$post_data['parent_id'];  
        }
        echo json_encode($success);
        exit;
	}
    
    private function uploadImage($file_key, $path,$id=false,$multiple = false)
    {


       
		  $data = array();
		 $extension=array("jpeg","jpg","png","gif");
		 foreach($_FILES[$file_key]["tmp_name"] as $key=>$tmp_name)
            {
                $file_name= rand(9999,99999999999).date('Ymdhsi').str_replace(' ','_',$_FILES[$file_key]['name'][$key]);
                $file_tmp=$_FILES[$file_key]["tmp_name"][$key];
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                if(in_array($ext,$extension))
                {
                       
                        move_uploaded_file($file_tmp=$_FILES[$file_key]["tmp_name"][$key], $path.$file_name);
                        if(!$multiple){
                            return $path.$file_name;
                        }
                        /*$data['DestinationID'] = $id; 
					    $data['ImagePath'] = $path.$file_name; 
					    $this->Model_destination_image->save($data);*/
                    
                }
               
            }
			return true;


    }
    
    
    public function getAllSubCategories()
    {
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $fetch_by['parent_id'] = $this->input->post('category_id');
        $sub_categories = $this->Model_category->getMultipleRows($fetch_by);
        $option = '<option value="">Select Sub Category</option>';
        if($sub_categories){
            foreach($sub_categories as $category){
                $option .= '<option value="'.$category->category_id.'">'.$category->title_en.'</option>';
            }
        }
        
         $success['html'] = $option;
         echo json_encode($success);
         exit;
    }
    
    
    private function delete(){
        
        $get_data = $this->Model_category->get($this->input->post('id'),false,'category_id');
        if(file_exists($get_data->image)) {
              unlink($get_data->image);
        }
      
        $deleted_by = array();
        $deleted_by['category_id'] = $this->input->post('id');
        $this->Model_category->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}