<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse_user extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        $this->load->model('Model_general');
    }

    public function index()
    {
        $this->data['view'] = 'backend/warehouse_user/manage';
        $this->data['warehouse_users'] = $this->Model_general->getAllWarehouseUsers();
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        $ksa_cities = getAllKsaCities();
        $this->data['view'] = 'backend/warehouse_user/add';
        $this->data['ksa_cities'] = $ksa_cities;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function save()
    {
        $posted_data = $this->input->post();
        $username_count = 0;
        $email_count = 0;
        $districts = $posted_data['districts'];
        unset($posted_data['districts']);
        $posted_data['is_verified'] = 1;
        $posted_data['is_approved'] = 1;

        $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $posted_data['password'] = md5($posted_data['password']);
            $posted_data['role_id'] = 4;
            $posted_data['created_at'] = date('Y-m-d H:i:s');
            $user_id = $this->Model_user->save($posted_data);
            foreach ($districts as $district_id) {
                $city_data['user_id'] = $user_id;
                $city_data['district_id'] = $district_id;
                $city_data['user_type'] = 'warehouse';
                $this->Model_general->save('user_districts', $city_data);
            }
            $response['error'] = 'false';
            $response['success'] = 'Warehouse user created successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/warehouse_user';
            echo json_encode($response);
            exit();
        }

    }

    public function edit($user_id)
    {

        $user_district_arr = array();
        $ksa_cities = getAllKsaCities();
        $this->data['ksa_cities'] = $ksa_cities;
        $this->data['user_data'] = $this->Model_user->get($user_id, false, 'user_id');
        $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
        $this->data['user_district_arr'] = array();
        if($user_districts){
            foreach ($user_districts as $district)
            {
                $user_district_arr[] = $district->district_id;
            }

            $this->data['user_district_arr'] = $user_district_arr;
        }
        
        

         $this->data['districts']  = $this->Model_general->getMultipleRows('districts', array('city_id' => $this->data['user_data']->city_id),false,'asc','eng_name');
        $this->data['view'] = 'backend/warehouse_user/edit';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function update()
    {
        $username_count = 0;
        $email_count = 0;
        $posted_data = $this->input->post();
        $user_id = $this->input->post('user_id');
        $user_data = $this->Model_user->get($user_id, false, 'user_id');
        $districts = $posted_data['districts'];
        unset($posted_data['districts']);

        if ($user_data->email != $posted_data['email']) {
            $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        }

        if ($user_data->username != $posted_data['username']) {
            $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        }

        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            $this->Model_user->update($posted_data, array('user_id' => $user_id));
            $this->Model_general->deleteMultipleRow('user_districts', 'user_id', $user_id);
            foreach ($districts as $district) {
                $city_data['user_id'] = $user_id;
                $city_data['district_id'] = $district;
                $city_data['user_type'] = 'warehouse';
                $this->Model_general->save('user_districts', $city_data);
            }
            $response['error'] = 'false';
            $response['success'] = 'Warehouse user updated successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/warehouse_user';
            echo json_encode($response);
            exit();
        }

    }

    public function delete()
    {
        $user_id = $this->input->post('id');
        $this->Model_general->deleteMultipleRow('user_districts', 'user_id', $user_id);
        $this->Model_user->delete(array('user_id' => $user_id));
        $response['error'] = 'false';
        $response['success'] = 'Warehouse user deleted successfully.';
        $response['redirect'] = true;
        $response['url'] = 'cms/warehouse_user';
        echo json_encode($response);
        exit();
    }
}