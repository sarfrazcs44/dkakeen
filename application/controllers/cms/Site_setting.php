<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('UTC');

class Site_setting extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Site_setting_model');
        
       // $this->data['language'] = $this->language;
    }

    public function index() {
        $this->edit(1);
    }
    
    public function edit($SiteSettingID) {

        $this->data['result'] = $this->Site_setting_model->get($SiteSettingID);

        if (!$this->data['result']) {
            redirect(base_url('cms/user'));
        }
        $this->data['view'] = 'backend/site_setting/edit';


        $this->data['id'] = $SiteSettingID;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {

            case 'update';
                //$this->validate();
                $this->update();
                break;
        }
    }

    private function validate() {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('SiteName', lang('SiteName'), 'required');
        $this->form_validation->set_rules('Email', lang('email'), 'valid_email');




        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function update() {
        $post_data = $this->input->post();
        
        unset($post_data['form_type']);
        $this->data['result'] = $this->Site_setting_model->get(1);
        

        // echo date_default_timezone_get();exit();
        $update_by['id'] = $post_data['id'];
        $this->Site_setting_model->update($post_data, $update_by);

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        echo json_encode($success);
        exit;
    }

    
}
