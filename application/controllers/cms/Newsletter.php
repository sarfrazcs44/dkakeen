<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_newsletter');
       // $this->load->model('Model_category_text');
		
	}
	
	 public function index()
	{
		
        $this->data['view'] = 'backend/newsletter/manage';
        
        $this->data['newsletters'] = $this->Model_newsletter->getAll();
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	 public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }


	 private function delete(){
        
        $get_data = $this->Model_newsletter->get($this->input->post('id'),false,'newsletter_id');
        
        $deleted_by = array();
        $deleted_by['newsletter_id'] = $this->input->post('id');
        $this->Model_newsletter->delete($deleted_by);
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }
    
  
}