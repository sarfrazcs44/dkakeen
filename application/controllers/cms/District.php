<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        
        $this->load->model('Model_general');
		
	}
	 
    
    public function index($city_id = 0)
	{
        if($city_id == 0){
            redirect('cms/city');
        }
		
        $this->data['view'] = 'backend/district/manage';
        $this->data['city_id'] = $city_id;
        $fetch_by = array();
        $fetch_by['city_id'] = $city_id;
        $this->data['city_data']        = $this->Model_general->getRow($city_id,'city');
        $this->data['districts'] = $this->Model_general->getMultipleRows('districts',$fetch_by,false,'asc','district_id');
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add($city_id = 0)
	{
		
        $this->data['view'] = 'backend/district/add';
        $this->data['city_id'] = $city_id;
         $this->data['city_data']        = $this->Model_general->getRow($city_id,'city');
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($district_id)
	{

        $this->data['result']        = $this->Model_general->getRow($district_id,'districts',false,'district_id');
        
        if(!$this->data['result']){
           redirect(base_url('cms/city')); 
        }

         $this->data['city_data']        = $this->Model_general->getRow($this->data['result']->city_id ,'city');
        $this->data['view'] = 'backend/district/edit';
        
        
        
        
        
        
        $this->data['district_id']    = $district_id;
        
        
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
        $this->form_validation->set_rules('arb_name', 'Arabic Title', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	

        unset($post_data['form_type']);
		
        $insert_id = $this->Model_general->save('districts',$post_data);
		
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
            $success['url'] = 'cms/district/index/'.$post_data['city_id']; 
			
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
    
        unset($post_data['form_type']);
        $this->Model_general->updateRow('districts',$post_data,$post_data['district_id'],'district_id');
        
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/district/index/'.$post_data['city_id'];  
        echo json_encode($success);
        exit;
	}
    
   
    
    public function getDistricts()
    {
        $fetch_by = array();
        $fetch_by['city_id'] = $this->input->post('city_id');
        $districts = $this->Model_general->getMultipleRows('districts',$fetch_by,false,'asc','eng_name');
        $option = '<option value="">Select Districts</option>';
        if($districts){
            foreach($districts as $value){
                $option .= '<option value="'.$value->district_id.'">'.$value->eng_name.'</option>';
            }
        }
        //echo $options;exit;
         $success['html'] = $option;
         echo json_encode($success);
         exit;
    }
    
    
    private function delete(){
        
        $this->Model_general->deleteRow('districts',$this->input->post('id'),'district_id');
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}