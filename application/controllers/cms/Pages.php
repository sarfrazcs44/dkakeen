<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        /*if ($this->session->userdata['admin']['role_id'] == 2) {
            redirect(base_url('cms/support'));
        }*/
        $this->load->model('Model_page');
        $this->load->model('Model_page_image');
        $this->load->model('Model_general');


    }


    public function index($parent_id = 0)
    {

        $this->data['view'] = 'backend/page/manage';

        $fetch_by = array();
        $fetch_by['parent_id'] = $parent_id;
        $this->data['pages'] = $this->Model_page->getMultipleRows($fetch_by);


        $this->data['parent_id'] = $parent_id;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function edit($page_id)
    {

        $this->data['result'] = $this->Model_page->get($page_id, false, 'page_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/pages'));
        }
        $this->data['view'] = 'backend/page/edit';
        if ($page_id == 4 || $page_id == 5 || $page_id == 6 || $page_id == 10) {
            $fetch_images = array();
            $fetch_images['page_id'] = $page_id;
            $this->data['images'] = $this->Model_page_image->getMultipleRows($fetch_images);
        }


        $this->data['page_id'] = $page_id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function editCatelog($page_id)
    {

        $this->data['result'] = $this->Model_page->get($page_id, false, 'page_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/pages'));
        }
        $this->data['view'] = 'backend/page/download_center/edit';


        $this->data['page_id'] = $page_id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function editProject($page_id)
    {

        $this->data['result'] = $this->Model_page->get($page_id, false, 'page_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/pages'));
        }
        $this->data['view'] = 'backend/page/projects/edit';
        $fetch_images = array();
        $fetch_images['page_id'] = $page_id;
        $this->data['images'] = $this->Model_page_image->getMultipleRows($fetch_images);

        $this->data['page_id'] = $page_id;
        $this->load->view('backend/layouts/default', $this->data);

    }

    public function editEvent($page_id)
    {

        $this->data['result'] = $this->Model_page->get($page_id, false, 'page_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/pages'));
        }
        $this->data['view'] = 'backend/page/projects/edit';
        $fetch_images = array();
        $fetch_images['page_id'] = $page_id;
        $this->data['images'] = $this->Model_page_image->getMultipleRows($fetch_images);

        $this->data['page_id'] = $page_id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {


            case 'save';
                $this->validate();
                $this->save();
                break;
            case 'update';
                $this->validate();
                $this->update();
                break;
            case 'delete';
                //$this->validate();
                $this->delete();
                break;
            case 'delete_image':
                $this->deleteImage();
                break;
        }
    }

    public function add($parent_id)
    {
        if ($parent_id == 9) {
            $this->data['view'] = 'backend/page/download_center/add';
        } elseif ($parent_id == 14) {
            $this->data['view'] = 'backend/page/projects/add';
        } elseif ($parent_id == 15) {
            $this->data['view'] = 'backend/page/events/add';
        }
        $this->data['parent_id'] = $parent_id;
        $this->load->view('backend/layouts/default', $this->data);
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('page_title_en', 'Page Title', 'required');
        $this->form_validation->set_rules('page_title_ar', 'Arabic Title', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        $post_data = $this->input->post();
        if ($post_data['parent_id'] == 14) {
            $this->saveProject();
        } elseif ($post_data['parent_id'] == 15) {
            $this->saveEvent();
        }

        $post_data['updated_at'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);

        $post_data['image'] = $this->uploadImage("image", "uploads/page_images/");


        $this->Model_page->save($post_data);

        $success['error'] = 'false';
        $success['success'] = 'Save Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/pages/index/' . $post_data['parent_id'];
        echo json_encode($success);
        exit;
    }

    private function saveProject()
    {
        $post_data = $this->input->post();

        $post_data['updated_at'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);


        $saved_id = $this->Model_page->save($post_data);

        $this->uploadImage("image", "uploads/page_images/", $saved_id, true);

        $success['error'] = 'false';
        $success['success'] = 'Save Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/pages/index/' . $post_data['parent_id'];
        echo json_encode($success);
        exit;
    }

    public function saveEvent()
    {
        $post_data = $this->input->post();

        $post_data['updated_at'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);


        $saved_id = $this->Model_page->save($post_data);

        $this->uploadImage("image", "uploads/page_images/", $saved_id, true);

        $success['error'] = 'false';
        $success['success'] = 'Save Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/pages/index/' . $post_data['parent_id'];
        echo json_encode($success);
        exit;
    }

    private function update()
    {
        $post_data = $this->input->post();


        $post_data['updated_at'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);
        if (isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != '') {

            if ($post_data['page_id'] == 4 || $post_data['page_id'] == 5 || $post_data['page_id'] == 6 || $post_data['page_id'] == 10 || $post_data['parent_id'] == 14 || $post_data['parent_id'] == 15) {
                $this->uploadImage("image", "uploads/page_images/", $post_data['page_id'], true);
            } else {
                $post_data['image'] = $this->uploadImage("image", "uploads/page_images/");
            }

        }
        $update_by = array();
        $update_by['page_id'] = $post_data['page_id'];
        unset($post_data['form_type']);
        $this->Model_page->update($post_data, $update_by);

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        
        $success['reload'] = true;
        echo json_encode($success);
        exit;
    }

    private function uploadImage($file_key, $path, $id = false, $multiple = false)
    {


        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif", "pdf", "doc", "docx");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                if ($id == 10) {

                    list($width, $height) = getimagesize($tmp_name);

                    if ($width >= 150 && $height >= 150) {
                        move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                        if (!$multiple) {
                            return $path . $file_name;
                        } else {
                            $image_data = array();
                            $image_data['page_id'] = $id;
                            $image_data['image'] = $path . $file_name;

                            $this->Model_page_image->save($image_data);
                        }
                    }
                } else {
                    move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                    if (!$multiple) {
                        return $path . $file_name;
                    } else {
                        $image_data = array();
                        $image_data['page_id'] = $id;
                        $image_data['image'] = $path . $file_name;

                        $this->Model_page_image->save($image_data);
                    }
                }


                /*$data['DestinationID'] = $id;
                $data['ImagePath'] = $path.$file_name;
                $this->Model_destination_image->save($data);*/

            }

        }
        return true;


    }


    private function deleteImage()
    {
        $deleted_by = array();
        $deleted_by['page_image_id'] = $this->input->post('id');
        $image = $this->Model_page_image->get($this->input->post('id'), false, 'page_image_id');
        if (file_exists($image->image)) {
            unlink($image->image);
        }
        $this->Model_page_image->delete($deleted_by);
        $success['error'] = 'false';
        $success['success'] = 'Deleted Successfully';

        echo json_encode($success);
        exit;

    }


    private function delete()
    {
        if ($this->input->post('id') > 11) {
            $deleted_by = array();
            $deleted_by['page_id'] = $this->input->post('id');
            $image = $this->Model_page->get($this->input->post('id'), false, 'page_id');
            if (file_exists($image->image)) {
                unlink($image->image);
            }
            $this->Model_page->delete($deleted_by);
            $success['error'] = 'false';
            $success['success'] = 'Deleted Successfully';

            echo json_encode($success);
            exit;
        } else {
            $success['error'] = 'You cann\'t delete this page';
            $success['success'] = false;
            echo json_encode($success);
            exit;
        }


    }

    public function refreshNotifications()
    {
        $notify = false;
        $count = 0;
        $postedUnreadChatsCount = (int)$this->input->post('unreadChatsCount');

        $retArr = getAllUnreadChatsForAdmin();
        $retArrCareers = getAllUnreadCareerRequestsForAdmin();
        $unreadChats = $retArr['result'];
        $unreadChatsCount = $retArr['result_count'];

        $unreadCareers = $retArrCareers['result'];
        $unreadCareersCount = $retArrCareers['result_count'];
        $total_count = (int)$unreadChatsCount + (int)$unreadCareersCount;
        if ($total_count > $postedUnreadChatsCount) {
            $notify = true;
        }
        $html = '<a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                                    <i class="mdi mdi-bell"></i>
                                    <span class="badge up bg-success unreadChatsCountForRing">' . $total_count . '</span>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right dropdown-lg user-list notify-list">';
        if ($unreadChatsCount > 0 || $unreadCareersCount > 0) {


            $html .= '<li>
                                        <h5>Notifications</h5>
                                    </li>';
            foreach ($unreadChats as $unreadChat) {
                $html .= '<li>
                                        <a href="' . base_url('cms/myChat') . '?r=' . $unreadChat->id . '" class="user-list-item" title="' . $unreadChat->username . ' has requested a chat with subject ' . $unreadChat->subject . '">
                                                <span class="name">' . truncate_text($unreadChat->username . ' has requested a chat with subject ' . $unreadChat->subject, 30) . '</span>
                                        </a>
                                    </li>	';
            }
            foreach ($unreadCareers as $unreadCareer) {
                $html .= '<li>
                                        <a href="' . base_url('cms/career/view') . '/' . $unreadCareer->id . '" class="user-list-item" title="' . $unreadCareer->full_name . ' has submitted a career request">
                                                <span class="name">' . truncate_text($unreadCareer->full_name . ' has submitted a career request', 30) .'</span>
                                        </a>
                                    </li>	';
            }
            $html .= '<li class="all-msgs text-center">
									<br>
                                        <p class="m-0"><a href="' . base_url('cms/myChat') . '">See all chat requests</a></p>
                                    </li>';
            $html .= '<li class="all-msgs text-center">
									<br>
                                        <p class="m-0"><a href="' . base_url('cms/career') . '">See all career requests</a></p>
                                    </li>';
        } else {
            $html .= '<li class="all-msgs text-center">
                                        <p class="m-0"><a href="javascript:void(0);">No new chat or career requests</a></p>
                                    </li>';
        }
        $html .= '</ul>';
        $response['html'] = $html;
        $response['notify'] = $notify;
        $response['count'] = $total_count > 0 ? $total_count : 0;
        echo json_encode($response);
        exit;
    }
	
	public function home_video()
	{
		$this->data['view'] = 'backend/home_video/video';
        $this->data['video'] = $this->Model_general->getRow(1, 'home_video', true);
        $this->load->view('backend/layouts/default', $this->data);
	}
	
	public function save_video()
	{
		$data['video_url'] = $this->input->post('video_url');
		$this->Model_general->updateRow('home_video', $data, 1);
		$success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = false;
        echo json_encode($success);
        exit;
	}


}