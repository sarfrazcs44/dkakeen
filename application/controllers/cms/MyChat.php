<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MyChat extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        //$this->load->model('Model_system_language');
        $this->load->model('Model_category');
        //$this->load->model('Model_category_text');
        $this->load->model('Model_chat_request');

    }


    public function index()
    {
        // updating that admin has seen chats that they are not needed to be notified anymore
        if (isset($_REQUEST['r']) && $_REQUEST['r'] > 0) {
            $this->Model_chat_request->update(array("to_be_notified" => 'no'), array("id" => $_REQUEST['r']));
        }
        $this->Model_chat_request->update(array("to_be_notified" => 'no'), array("to_be_notified" => 'yes'));

        $this->data['chat_requests'] = $this->Model_chat_request->getMultipleRows(array('is_admin' => '0'), true, 'desc');
        $this->data['view'] = 'backend/chat/manage';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function delete()
    {


        $deleted_by = array();
        $deleted_by['id'] = $this->input->post('id');
        $this->Model_chat_request->delete($deleted_by);
        $success['error'] = 'false';
        $success['success'] = 'Deleted Successfully';

        echo json_encode($success);
        exit;
    }

    public function get_users_data()
    {
        $user_ids = $this->input->post('user_ids');
        $response = $this->Model_chat_request->get_users_data($user_ids);
        echo json_encode($response);
    }

    public function change_status_closed()
    {
        $user_id = $this->input->post('chat_request_id');
        $update = $this->Model_chat_request->update(array("is_closed" => '1'), array("id" => $user_id));
        $Chat_request = $this->Model_chat_request->getMultipleRows(array('is_admin' => '0', 'is_closed' => '0'), true);
        if ($Chat_request[0]['id'] != '') {
            $response['receiver'] = $Chat_request[0]['id'];
        } else {
            $response['receiver'] = 0;
        }
        $response['success'] = 1;
        echo json_encode($response);
    }

    public function getLatestChat()
    {
        $chat_id = $this->input->post('lastChatId');
        $NewChats = $this->Model_chat_request->getLatestChat($chat_id);
        $html = '';
        $LastChatId = 0;
        $hasNewChat = 0;
        if ($NewChats) {
            foreach ($NewChats as $request) {
                $html = '<tr class="chat_row">
                            <td class="user">' . $request['username'] . '</td>
                            <td>' . $request['email'] . '</td>
                            <td class="subject">' . $request['subject'] . '</td>
                            
                            <td>
                                <button type="button" class="btn btn-default btn-rounded w-md waves-effect m-b-5 QueuedMsg' . $request['id'] . ' ' . ($request['is_closed'] == 0 ? 'NotClosed' : '') . '" disabled="disabled">' . ($request['is_closed'] == 1 ? 'Closed' : '0 Queued Message') . '</button>
                            </td>
                            <td id="' . $request['id'] . '">
                                <button data-chat_request_id="'.$request['id'].'" data-customer_full_name="' . $request['username'] . '"  data-customer_email="' . $request['email'] . '" type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5 show_friend_messages chat_button startchat' . $request['id'] . '" data-attr="' . $request['id'] . '" ' . ($request['is_closed'] == 1 ? 'disabled="disabled"' : '') . '>Start Chat</button>
                            </td>
                        </tr>
                        <script>
                        $(document).ready(function(){
                        $("#' . $request['id'] . '").on("click",".chat_button",function(){
                            $(".show_username").html($( this ).parent().parent().find(".user").html());
                            $(".show_subject").html("Sub : "+$( this ).parent().parent().find(".subject").html());
                        });
                    });
                        </script>
                        ';
                $LastChatId = ($LastChatId < $request['id'] ? $request['id'] : $LastChatId);
                $hasNewChat = 1;
            }
        }
        $response['html'] = $html;
        $response['LastChatId'] = $LastChatId;
        $response['hasNewChat'] = $hasNewChat;
        $response['success'] = 1;
        echo json_encode($response);
    }


    public function checkIFChatExist()
    {
        $chat_request_id = $this->input->post('chat_request_id');
        // $chat_request_id = explode('-', $chat_request_id);
        // $chat_request_id = $chat_request_id[1];
        $get_chat = $this->Model_chat_request->get($chat_request_id);
        $response = array();
        if ($get_chat) {
            $response['exist'] = true;
        } else {
            $response['exist'] = false;
        }
        $response['is_closed'] = $get_chat->is_closed;
        $response['is_in_progress'] = $get_chat->is_in_progress;

        echo json_encode($response);
        exit;
    }

    public function mark_chat_as_closed()
    {
        $chat_id = $this->input->post('chat_id');
        $chat = $this->Model_chat_request->get($chat_id, true);
        if ($chat) {
            if ($chat['is_in_progress'] == 0) { // if 120 seconds timer passed and no one has still attended chat then mark chat as closed
                $this->Model_chat_request->update(array("is_closed" => 1), array("id" => $chat_id));
            }
        }
        $response['chat_detail'] = $chat;
        echo json_encode($response);
        exit;
    }

    public function mark_chat_in_progress()
    {
        $chat_id = $this->input->post('chat_id');
        // $chat_id = explode('-', $chat_id);
        // $chat_id = $chat_id[1];
        $chat = $this->Model_chat_request->get($chat_id, true);
        if ($chat) {
            if ($chat['is_in_progress'] == 0) { // if 120 seconds timer passed and no one has still attended chat then mark chat as closed
                $this->Model_chat_request->update(array("is_in_progress" => 1), array("id" => $chat_id));
            }
        }
        $response['chat_detail'] = $chat;
        echo json_encode($response);
        exit;
    }


}