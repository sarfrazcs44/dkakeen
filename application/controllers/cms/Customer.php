<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        $this->load->model('Model_general');

    }

    public function index()
    {
        $this->data['view'] = 'backend/customer/manage';
        $fetch_by = array();
        $fetch_by['role_id'] = 3;
        $this->data['customers'] = $this->Model_user->getMultipleRows($fetch_by);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add($parent_id = 0)
    {

        $this->data['view'] = 'backend/category/add';
        $this->data['parent_id'] = $parent_id;
        $this->load->view('backend/layouts/default', $this->data);
    }


    public function edit($category_id)
    {

        $this->data['result'] = $this->Model_category->get($category_id, false, 'category_id');

        if (!$this->data['result']) {
            redirect(base_url('cms/category'));
        }
        $this->data['view'] = 'backend/category/edit';


        $this->data['category_id'] = $category_id;
        $this->load->view('backend/layouts/default', $this->data);

    }


    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save';
                $this->validate();
                $this->save();
                break;
            case 'update';
                $this->validate();
                $this->update();
                break;
            case 'delete';
                $this->delete();
                break;
            case 'suspend';
                $this->suspend();
                break;
            case 'activate';
                $this->activate();
                break;
            case 'verifyUser';
                $this->verifyUser();
                break;    
        }
    }


    private function validate()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('title_en', 'Eng Title', 'required');
        $this->form_validation->set_rules('title_ar', 'Arabic Title', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save()
    {
        $post_data = $this->input->post();

        if (isset($post_data['is_active'])) {
            $post_data['is_active'] = 1;
        } else {
            $post_data['is_active'] = 0;
        }


        unset($post_data['form_type']);
        $post_data['created_at'] = date('Y-m-d H:i:s');
        $post_data['updated_at'] = date('Y-m-d H:i:s');

        $post_data['image'] = $this->uploadImage("image", "uploads/category_images/");
        $post_data['image_mbl'] = $this->uploadImage("image_mbl", "uploads/category_images/");
        $insert_id = $this->Model_category->save($post_data);
        if ($insert_id > 0) {

            $success['error'] = 'false';
            $success['success'] = 'Save Successfully';
            $success['redirect'] = true;
            if ($post_data['parent_id'] == 0) {
                $success['url'] = 'cms/category';
            } else {
                $success['url'] = 'cms/category/index/' . $post_data['parent_id'];
            }

            echo json_encode($success);
            exit;


        } else {
            $errors['error'] = 'There is something went wrong';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        $post_data = $this->input->post();


        $post_data['updated_at'] = date('Y-m-d H:i:s');
        if (isset($post_data['is_active'])) {
            $post_data['is_active'] = 1;
        } else {
            $post_data['is_active'] = 0;
        }
        unset($post_data['form_type']);
        if (isset($_FILES['image']["name"][0]) && $_FILES['image']["name"][0] != '') {
            $post_data['image'] = $this->uploadImage("image", "uploads/category_images/");
        }
        if (isset($_FILES['image_mbl']["name"][0]) && $_FILES['image_mbl']["name"][0] != '') {
            $post_data['image_mbl'] = $this->uploadImage("image_mbl", "uploads/category_images/");
        }
        $update_by = array();
        $update_by['category_id'] = $post_data['category_id'];
        unset($post_data['form_type']);
        $this->Model_category->update($post_data, $update_by);

        $success['error'] = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        if ($post_data['parent_id'] == 0) {
            $success['url'] = 'cms/category';
        } else {
            $success['url'] = 'cms/category/index/' . $post_data['parent_id'];
        }
        echo json_encode($success);
        exit;
    }

    private function uploadImage($file_key, $path, $id = false, $multiple = false)
    {


        $data = array();
        $extension = array("jpeg", "jpg", "png", "gif");
        foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
            $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
            $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {

                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                }
                /*$data['DestinationID'] = $id;
                $data['ImagePath'] = $path.$file_name;
                $this->Model_destination_image->save($data);*/

            }

        }
        return true;


    }


    public function getAllSubCategories()
    {
        $fetch_by = array();
        $fetch_by['is_active'] = 1;
        $fetch_by['parent_id'] = $this->input->post('category_id');
        $sub_categories = $this->Model_category->getMultipleRows($fetch_by);
        $option = '<option value="">Select Sub Category</option>';
        if ($sub_categories) {
            foreach ($sub_categories as $category) {
                $option .= '<option value="' . $category->category_id . '">' . $category->title_en . '</option>';
            }
        }

        $success['html'] = $option;
        echo json_encode($success);
        exit;
    }


    private function delete()
    {
        $deleted_by = array();
        $deleted_by['user_id'] = $this->input->post('id');
        $this->Model_user->delete($deleted_by);
        $success['error'] = 'false';
        $success['success'] = 'Deleted Successfully';
        echo json_encode($success);
        exit;
    }

    private function suspend()
    {
        $update_by = array();
        $update_by['user_id'] = $this->input->post('id');
        $this->Model_user->update(array('status' => 'inactive'), $update_by);

        // send suspension email
        $user_data = $this->Model_user->get($update_by['user_id'], false, 'user_id');
        $email_data['to'] = $user_data->email;
        $email_data['subject'] = 'Account suspended at MSJ';
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = "Dear $user_data->full_name , Your account is suspended at MSJ. Please contact admin for details and further action.";
        $email_data['body'] = emailTemplate($email_data['body']);
        sendEmail($email_data);

        $success['error'] = 'false';
        $success['success'] = 'Suspended Successfully';
        echo json_encode($success);
        exit;
    }

    private function activate()
    {
        $update_by = array();
        $update_by['user_id'] = $this->input->post('id');
        $this->Model_user->update(array('status' => 'active'), $update_by);

        // send activation email
        $user_data = $this->Model_user->get($update_by['user_id'], false, 'user_id');
        $email_data['to'] = $user_data->email;
        $email_data['subject'] = 'Account activated at MSJ';
        $email_data['from'] = 'noreply@msj.com.sa';
        $email_data['body'] = "Dear $user_data->full_name , Your account is activated at MSJ. Now you can login and make orders from website and apps.";
        $email_data['body'] = emailTemplate($email_data['body']);
        sendEmail($email_data);

        $success['error'] = 'false';
        $success['success'] = 'Activated Successfully';
        echo json_encode($success);
        exit;
    }

    private function verifyUser()
    {
        $update_by = array();
        $update_by['user_id'] = $this->input->post('id');
        $this->Model_user->update(array('is_verified' => 1), $update_by);

        

        $success['error'] = 'false';
        $success['success'] = 'Verified Successfully';
        echo json_encode($success);
        exit;
    }

}