<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
        if($this->session->userdata['admin']['role_id'] == 2){
            redirect(base_url('cms/support'));
        }
        $this->load->model('Model_faq');
       // $this->load->model('Model_category_text');
		
	}
	 
    
    public function index()
	{
		
        $this->data['view'] = 'backend/faq/manage';
        
        $this->data['faqs'] = $this->Model_faq->getAll();
        $this->load->view('backend/layouts/default',$this->data);
	}
    
	public function add()
	{
		
        $this->data['view'] = 'backend/faq/add';
        $this->load->view('backend/layouts/default',$this->data);
	}
    
    
    public function edit($faq_id)
	{
        
        $this->data['result']		 = $this->Model_faq->get($faq_id,false,'faq_id');
		
        if(!$this->data['result']){
           redirect(base_url('cms/faq')); 
        }
        $this->data['view'] = 'backend/faq/edit';
        
		
		
		
        
		
		$this->data['faq_id'] 	 = $faq_id;
		$this->load->view('backend/layouts/default',$this->data);
		
	}
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
          case 'save';
                $this->validate();
                $this->save();
          break; 
          case 'update';
                $this->validate();
                $this->update();
          break;
          case 'delete';
                //$this->validate();
                $this->delete();
          break;        
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('question_en', 'Eng Question', 'required');
        $this->form_validation->set_rules('question_ar', 'Arabic Question', 'required');



        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
	{
		$post_data = $this->input->post();
	
		
		
		
		unset($post_data['form_type']);
		$post_data['created_at']    = date('Y-m-d H:i:s');		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
        //$post_data['image']         = $this->uploadImage("image", "uploads/faq_images/");
		$insert_id = $this->Model_faq->save($post_data);
		if($insert_id > 0)
		{
			
			$success['error']   = 'false';
			$success['success'] = 'Save Successfully';
			$success['redirect'] = true;
			$success['url'] = 'cms/faq';
			echo json_encode($success);
			exit;
			
			
		}else
		{
			$errors['error'] = 'There is something went wrong';
			$errors['success'] = 'false';
			echo json_encode($errors);
			exit;
		}
	}
    
    private function update()
	{
		$post_data = $this->input->post();
	
		
		
		$post_data['updated_at']    = date('Y-m-d H:i:s');
		
       unset($post_data['form_type']);
       
        $update_by = array();
		$update_by['faq_id'] = $post_data['faq_id'];
        unset($post_data['form_type']);
        $this->Model_faq->update($post_data,$update_by);
		
        $success['error']   = 'false';
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/faq';
        echo json_encode($success);
        exit;
	}
    
    
    
    
    
    private function delete(){
        
        
      
        $deleted_by = array();
        $deleted_by['faq_id'] = $this->input->post('id');
        $this->Model_faq->delete($deleted_by);
        
        $success['error']   = 'false';
        $success['success'] = 'Deleted Successfully';
        
        echo json_encode($success);
        exit;
    }

}