<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery_user extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkAdminSession();
        $this->load->model('Model_user');
        $this->load->model('Model_general');
    }

    public function index()
    {
        $this->data['view'] = 'backend/delivery_user/manage';
        $where = 'is_approved != 0';
        if(isset($_GET['status']) && $_GET['status'] == 0){
            $where = 'is_approved = 0';
        }
        $this->data['delivery_users'] = $this->Model_general->getAllDeliveryUsers($where);
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        $ksa_cities = getAllKsaCities();
        $this->data['view'] = 'backend/delivery_user/add';
        $this->data['branches'] = $this->Model_general->getAllWarehouseUsers(true);
        $this->data['ksa_cities'] = $ksa_cities;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function save()
    {
        $posted_data = $this->input->post();
        //$districts = $posted_data['districts'];
       // unset($posted_data['districts']);
        $posted_data['is_verified'] = 1;
        $posted_data['is_approved'] = 1;

        $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            if (isset($_FILES['profile_pic']["name"][0]) && $_FILES['profile_pic']["name"][0] != '') {
                $posted_data['profile_pic']         = uploadImage("profile_pic", "uploads/images/");
            }

            if (isset($_FILES['registration_license_img']["name"][0]) && $_FILES['registration_license_img']["name"][0] != '') {
                $posted_data['registration_license_img']         = uploadImage("registration_license_img", "uploads/images/");
            }

            if (isset($_FILES['driving_license_img']["name"][0]) && $_FILES['driving_license_img']["name"][0] != '') {
                $posted_data['driving_license_img']         = uploadImage("driving_license_img", "uploads/images/");
            }
            $posted_data['password'] = md5($posted_data['password']);
            $posted_data['role_id'] = 5;
            $posted_data['created_at'] = date('Y-m-d H:i:s');
            $user_id = $this->Model_user->save($posted_data);
            /*foreach ($districts as $district) {
                $city_data['user_id'] = $user_id;
                $city_data['district_id'] = $district;
                $city_data['user_type'] = 'delivery';
                $this->Model_general->save('user_districts', $city_data);
            }*/
            $response['error'] = 'false';
            $response['success'] = 'Delivery user created successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/delivery_user';
            echo json_encode($response);
            exit();
        }

    }

    public function edit($user_id)
    {
        $user_cities_arr = array();
        $ksa_cities = getAllKsaCities();
        $this->data['ksa_cities'] = $ksa_cities;
        $this->data['user_data'] = $this->Model_user->get($user_id, false, 'user_id');
        $user_districts = $this->Model_general->getMultipleRows('user_districts', array('user_id' => $user_id));
        $this->data['user_district_arr'] = array();
        if($user_districts){
            foreach ($user_districts as $district)
            {
                $user_district_arr[] = $district->district_id;
            }

            $this->data['user_district_arr'] = $user_district_arr;
        }
        
        

         $this->data['districts']  = $this->Model_general->getMultipleRows('districts', array('city_id' => $this->data['user_data']->city_id),false,'asc','eng_name');
         $this->data['branches'] = $this->Model_general->getAllWarehouseUsers(true);
        $this->data['user_cities_arr'] = $user_cities_arr;
        $this->data['view'] = 'backend/delivery_user/edit';
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function update()
    {
        $posted_data = $this->input->post();
        $user_id = $this->input->post('user_id');
        $user_data = $this->Model_user->get($user_id, false, 'user_id');
        //$districts = $posted_data['districts'];
        //unset($posted_data['districts']);
        $email_count  = 0;
        $username_count = 0;

        if ($user_data->email != $posted_data['email']) {
            $email_count = $this->Model_user->checkIfEmailAvailable($posted_data['email']);
        }

        if ($user_data->username != $posted_data['username']) {
            $username_count = $this->Model_user->checkIfUsernameAvailable($posted_data['username']);
        }

        if ($username_count > 0 && $email_count > 0) {
            $response['error'] = 'User already exists with entered username and email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count > 0 && $email_count == 0) {
            $response['error'] = 'Entered username already exists with some other email address.';
            echo json_encode($response);
            exit();
        } elseif ($username_count == 0 && $email_count > 0) {
            $response['error'] = 'Entered email address already exists with some other username.';
            echo json_encode($response);
            exit();
        } else {
            if (isset($_FILES['profile_pic']["name"][0]) && $_FILES['profile_pic']["name"][0] != '') {
                $posted_data['profile_pic']         = uploadImage("profile_pic", "uploads/images/");
            }

            if (isset($_FILES['registration_license_img']["name"][0]) && $_FILES['registration_license_img']["name"][0] != '') {
                $posted_data['registration_license_img']         = uploadImage("registration_license_img", "uploads/images/");
            }

            if (isset($_FILES['driving_license_img']["name"][0]) && $_FILES['driving_license_img']["name"][0] != '') {
                $posted_data['driving_license_img']         = uploadImage("driving_license_img", "uploads/images/");
            }
            $this->Model_user->update($posted_data, array('user_id' => $user_id));
           /* $this->Model_general->deleteMultipleRow('user_districts', 'user_id', $user_id);
            foreach ($districts as $district) {
                $city_data['user_id'] = $user_id;
                $city_data['district_id'] = $district;
                $city_data['user_type'] = 'delivery';
                $this->Model_general->save('user_districts', $city_data);
            }*/
            $response['error'] = 'false';
            $response['success'] = 'Delivery user updated successfully.';
            $response['redirect'] = true;
            $response['url'] = 'cms/delivery_user';
            echo json_encode($response);
            exit();
        }

    }


    public function approveUser()
    {
        $posted_data = $this->input->post();
        $user_id = $this->input->post('user_id');
        //$user_data = $this->Model_user->get($user_id, false, 'user_id');
        
        $this->Model_user->update(array('is_approved' => $this->input->post('is_approved')), array('user_id' => $user_id));
        if($this->input->post('is_approved') == 1){
            $this->approvalEmail($user_id);
        }
        
        $response['error'] = 'false';
        $response['success'] = 'Delivery user updated successfully.';
        $response['reload'] = true;
        
        echo json_encode($response);
        exit();
        

    }

     public function approvalEmail($user_id)
    {
        $user = $this->Model_user->get($user_id, true, 'user_id');
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MSJ Secutiry Systems</title>
<style>

p{
margin-bottom:5px !important;
margin-top:5px !important;  
}

h4{
    
margin-bottom:20px;

    
}


#wrap {
    float: left;
    position: relative;
    left: 50%;
}

#content {
    float: left;
    position: relative;
    left: -50%;
}

</style>

</head>



<body>

<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td align="left">

<h4 style="font-family:sans-serif;">Hi ' . $user['full_name'] . ' ,</h4>
<p style="font-family:sans-serif;font-size:14px;">Your account is approved by admin.Now you can login to app to get orders</p>

<div><!--[if mso]>
  <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="http://" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="223%" stroke="f" fillcolor="#10b26a">
    <w:anchorlock/>
    <center>
  <![endif]-->
  <!--[if mso]>
    </center>
  </v:roundrect>
<![endif]--></div>
<p style="font-family:sans-serif;font-size:14px;">Didnt request this? <a href="' . base_url() . '" style="color:#10b26a;text-decoration:none;">Help</a></p>
<br />
<hr width="100%" align="left">
<table cellpadding="0" cellspacing="0">
<tr>
<td>
<img src="' . base_url() . 'assets/front/images/msj_new_logo.png" width="60" height="60" alt="Site Logo">
</td>
<td>&nbsp;&nbsp;</td>
<td>
<h4 style="font-family:sans-serif;margin-bottom:0px;margin-top:0px;">MSJ Security Systems</h4>
<span style="font-family:sans-serif;color:grey;font-size:12px;">Kingdom of Saudi arabia</span><br>
<span style="font-family:sans-serif;">
<a href="' . base_url() . '" style="color:grey;font-size:10px;text-decoration: none;">www.msj.com.sa</a>
</span>
</td>
</tr>
</table>


</td>
</tr>
</table>

</body>
</html>
';
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <noreply@ecom.com.sa>' . "\r\n";
        mail($user['email'], 'Welcome at Ecom  Systems', $message, $headers);

    }

    public function delete()
    {
        $user_id = $this->input->post('id');
        $this->Model_general->deleteMultipleRow('user_districts', 'user_id', $user_id);
        $this->Model_user->delete(array('user_id' => $user_id));
        $response['error'] = 'false';
        $response['success'] = 'Delivery user deleted successfully.';
        $response['redirect'] = true;
        $response['url'] = 'cms/delivery_user';
        echo json_encode($response);
        exit();
    }
}