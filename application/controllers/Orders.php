<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_order');
    }

    public function index()
    {
        $user_data = $this->session->userdata('user');
        $user_id = $user_data['user_id'];
        $current_orders = $this->Model_order->getAllCurrentOrdersForUser($user_id);
        $history_orders = $this->Model_order->getAllHistoryOrdersForUser($user_id);
        // dump($current_orders);
        $data['current_orders'] = $current_orders;
        $data['history_orders'] = $history_orders;
        $data['view'] = 'front/orders/index';
        $data['isLogin'] = TRUE;
        $data['active'] 	 = 'orders';
        $this->load->view('front/layouts/default', $data);
    }

}
