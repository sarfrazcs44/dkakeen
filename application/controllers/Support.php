<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        checkUserSession();
        $this->load->model('Model_user');
        $this->load->model('Model_complaint_type');
        $this->load->model('Model_complaint_type');
        $this->load->model('Model_support_tickets');
        $this->load->model('Model_ticket_comments');
        $this->load->model('Model_support_user');

    }

    public function index()
    {
        $data = array();
        $user = $this->session->userdata('user');
        $data['user'] = $user;

        $fetch_open['user_id'] = $user['user_id'];
        $fetch_open['status'] = 'open';
        $open_tickets = $this->Model_support_tickets->getMultipleRows($fetch_open, true);

        /*$fetch_closed['user_id'] = $user['user_id'];
        $fetch_closed['status'] = 'closed';
        $data['tickets_closed'] = $this->Model_support_tickets->getMultipleRows($fetch_closed, true);*/

        if ($open_tickets) {
            foreach ($open_tickets as $key => $ticket) {
                $open_tickets[$key]['unread'] = $this->check_unread_comments($ticket['ticket_id'], $user['user_id']);
            }
        }

        //echo "<pre>"; print_r($data['tickets_closed']); exit;
        $data['user_id'] = $user['user_id'];
        $data['tickets_open'] = $open_tickets;
        $data['view'] = 'front/support/index';
        $data['active'] = 'support';
        $data['isLogin'] = TRUE;
        $this->load->view('front/layouts/default', $data);
    }


    public function closed_reopen_by_user()
    {
        $post_data = $this->input->post();

        if ($post_data['closed_request'] == 2) {
            $update_data['status'] = 'closed';
        } else if ($post_data['closed_request'] == 3) {
            $update_data['status'] = 'open';
        }

        $update_data['closed_request'] = $post_data['closed_request'];
        $update_by['ticket_id'] = $post_data['ticket_id'];
        $this->Model_support_tickets->update($update_data, $update_by);

        if ($post_data['closed_request'] == 3) {
            $where = 'support_tickets.ticket_id = ' . $post_data['ticket_id'] . ' ';
            $getUserData = $this->Model_support_tickets->getJoinedData(true, 'user_id', 'users', $where);
            $getUserData = $getUserData[0];

            // sending email to all admins that a ticket is reopened
            $this->sendEmailToAdminsForReopenedTicket($getUserData, $post_data['ticket_id']);
            $this->sendReopenedTicketNotificationToAdminUsers($getUserData['id']);
        }
        $response['status'] = 'TRUE';
        echo json_encode($response);
        exit;


    }

    public function create()
    {
        $data = array();

        $user = $this->session->userdata('user');

        // get complaint types
        $complain_types = $this->Model_complaint_type->getAll();

        // echo "<pre>"; print_r($visit_complain_types); exit;

        $data['complain_types'] = $complain_types;
        $data['complain_types_new'] = $this->Model_complaint_type->getMultipleRows(array('parent_id' => 0), true);
        $data['placeholder'] = $this->Model_complaint_type->get(1, true, 'complaint_type_id');
        $data['complain_types_new_child'] = $this->Model_complaint_type->getMultipleRows(array('parent_id' => 1), true);
        $data['user'] = $user;
        $data['lang'] = $this->session->userdata('lang');

        $data['view'] = 'front/support/create';
        $data['isLogin'] = TRUE;
        $data['active'] = 'support';
        $this->load->view('front/layouts/default', $data);
    }


    public function create_ticket()
    {
        $post_data = $this->input->post();
        $ticket_type = $post_data['ticket_type'];
        $this->validate($ticket_type);
        $user = $this->session->userdata('user');

//echo "<pre>"; print_r($post_data); exit;


        if (!empty($post_data['visit_time'])) {
            $post_data['visit_time'] = strtotime($post_data['visit_time']);
        }
        $post_data['user_id'] = $user['user_id'];
        $post_data['timestamp'] = time();
        $post_data['created_at'] = date('Y-m-d H:i:s');
        $post_data['ticket_id'] = time();
        $post_data['full_name'] = $user['full_name'];
        $post_data['email'] = $user['email'];
        $post_data['phone'] = $user['phone'];
        $post_data['city'] = $user['city'];

        // check complaint type by ticket type
        /*if ($ticket_type == 'complaint') {
            $post_data['type_id'] = $post_data['complaint_type_id'];
        } else if ($ticket_type == 'visit') {
            $post_data['type_id'] = $post_data['visit_complaint_type_id'];
        }
        unset($post_data['complaint_type_id']);
        unset($post_data['visit_complaint_type_id']);*/


        $insert_id = $this->Model_support_tickets->save($post_data);
        if ($insert_id > 0) {
            $get_ticket = $this->Model_support_tickets->get($insert_id, true);
            // check complaint type by ticket type and send email to all support users against this ticket type in support_users table.
            $fetch_by['complaint_type_id'] = $get_ticket['type_id']; // complaint child id

            // fetching all support users against this ticket type
            $support_users = $this->Model_support_user->getMultipleRows($fetch_by);
            if ($support_users) { // if any support users found for this type of ticket
                // sending email to all support users against this ticket
                $this->sendEmailToSupportUsers($support_users, $get_ticket['ticket_id'], $get_ticket['user_id']);
            } else {
                // sending email to admins against this ticket.
                $this->sendEmailToAdmins($get_ticket['ticket_id'], $get_ticket['user_id']);
            }

            $this->sendPushNotificationToAdminsAndSupportUsers($insert_id);

            // save message in comments
            $store_data['message'] = trim($post_data['message']);
            $store_data['user_id'] = $user['user_id'];
            $store_data['ticket_id'] = $post_data['ticket_id'];
            $store_data['timestamp'] = time();
            $this->Model_ticket_comments->save($store_data);
            $success['error'] = 'false';
            $success['success'] = 'Support Ticket created successfully.';
            $success['redirect'] = true;
            $success['url'] = 'support';
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = 'There is something went wrong';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }

    }

    public function get_comments()
    {
        $post_data = $this->input->post();
        if (!empty($post_data['ticket_id'])) {
            $user = $this->session->userdata('user');
            $ticket_id = $post_data['ticket_id'];
            $fetch_by['ticket_id'] = $ticket_id;
            $chat = $this->Model_ticket_comments->getMultipleRows($fetch_by, true);
            if ($chat) {
                $comments = [];
                foreach ($chat as $item) {
                    // update is_read status if i am not the user who posted the comment
                    if ($item['user_id'] != $user['user_id']) {
                        $update_data['is_read'] = 1;
                        $update_by['comment_id'] = $item['comment_id'];
                        $this->Model_ticket_comments->update($update_data, $update_by);
                    }
                    $comments[] = $this->formatted_message($item, $user['user_id']);
                }
                $response['status'] = 'TRUE';
                $response['messages'] = $comments;
            } else {
                $response['status'] = 'FALSE';
            }
            echo json_encode($response);
            exit;

        }
    }

    public function store_message()
    {
        $post_data = $this->input->post();
        if (!empty($post_data['message']) && !empty($post_data['ticket_id'])) {
            $user = $this->session->userdata('user');
            $store_data['message'] = trim($post_data['message']);
            $store_data['user_id'] = $user['user_id'];
            $store_data['ticket_id'] = $post_data['ticket_id'];

            $insert_id = $this->Model_ticket_comments->save($store_data);
            if ($insert_id > 0) {
                $msg = $this->Model_ticket_comments->get($insert_id, true, 'comment_id');

                // pusher call here
                pusher($msg, 'ticket_' . $post_data['ticket_id'], 'ticket_' . $post_data['ticket_id'], false);
                $this->sendTicketCommentReplyNotificationToAdminUsers($msg['id']);

                $formatted_message = $this->formatted_message($msg, $user['user_id']);
                $response['status'] = 'TRUE';
                $response['message'] = $formatted_message;
            } else {
                $response['status'] = 'FALSE';
            }
            echo json_encode($response);
            exit;
        }

    }


    private function check_unread_comments($ticket_id, $user_id)
    {
        if ($ticket_id) {
            $sql = "SELECT count(comment_id) AS unread FROM ticket_comments WHERE ticket_id = $ticket_id AND is_read = 0 AND user_id <> $user_id ";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['unread'];
        }
    }

    private function formatted_message($message, $user_id)
    {
        $message['created_at'] = date("h:i a", strtotime($message['created_at']));
        $message['who_said'] = ($message['user_id'] != $user_id) ? "Support: " : "You: ";
        $message['even_odd'] = ($message['user_id'] == $user_id) ? "even" : "odd";
        return $message;
    }

    private function validate($ticket_type)
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

//		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
//		$this->form_validation->set_rules('email', 'Email Address', 'required');
//		$this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('type_id', 'Complaint Type', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('location', 'Location', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($ticket_type == 'visit') {
            $this->form_validation->set_rules('apartment_no', 'Apartment', 'required');
            $this->form_validation->set_rules('visit_time', 'Visit Timing', 'required');
        }


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function sendEmailToSupportUsers($support_users, $ticket_id, $user_id)
    {
        $user_details = $this->Model_user->get($user_id, true, 'user_id');
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>MSJ user, ' . $user_details['full_name'] . ' has created a new support ticket with ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($support_users as $support_user) {
            $support_user_details = $this->Model_user->get($support_user->user_id, false, 'user_id');
            $email_data['to'] = $support_user_details->email;
            $email_data['subject'] = 'Support Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    private function sendEmailToAdmins($ticket_id, $user_id)
    {
        $user_details = $this->Model_user->get($user_id, true, 'user_id');
        $admins = $this->Model_user->getMultipleRows(array('role_id' => 1)); // fetching all admin users
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>MSJ user, ' . $user_details['full_name'] . ' has created a new support ticket with ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($admins as $admin) {
            $email_data['to'] = $admin->email;
            $email_data['subject'] = 'Support Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    private function sendEmailToAdminsForReopenedTicket($user_details, $ticket_id)
    {
        $admins = $this->Model_user->getMultipleRows(array('role_id' => 1)); // fetching all admin users
        $body = '';
        $body .= '<p>Hi, Admin</p>';
        $body .= '<p>MSJ user, ' . $user_details['full_name'] . ' want to reopen his ticket no ' . $ticket_id . ' . Please have a look.</p>';
        $message = emailTemplate($body);

        foreach ($admins as $admin) {
            $email_data['to'] = $admin->email;
            $email_data['subject'] = 'Reopen Request : Ticket ' . $ticket_id;
            $email_data['from'] = $user_details['email'];
            $email_data['body'] = $message;
            sendEmail($email_data);
        }
    }

    public function getChildOptions()
    {
        $lang = $this->session->userdata('lang');
        $parent_id = $this->input->post('type_id');
        $complaint_type = $this->Model_complaint_type->get($parent_id, true, 'complaint_type_id');
        $complain_types_new = $this->Model_complaint_type->getMultipleRows(array('parent_id' => $parent_id), true);
        if ($complain_types_new) {
            $html = '<option value="">' . ($lang == 'en' ? $complaint_type['placeholder_en'] : $complaint_type['placeholder_ar']) . '</option>';
            foreach ($complain_types_new as $type) {
                $html .= '<option value="' . $type['complaint_type_id'] . '">' . ($lang == 'en' ? $type['complaint_title_en'] : $type['complaint_title_ar']) . '</option>';
            }
        } else {
            $html = '<option value="">' . ($lang == 'en' ? 'No option found for this type' : 'لم يتم العثور على خيار لهذا النوع') . '</option>';
        }
        echo $html;
        exit();
    }

    public function sendPushNotificationToAdminsAndSupportUsers($ticket_id)
    {
        $get_ticket = $this->Model_support_tickets->get($ticket_id, true);
        $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
        // sending notifications to all admin type users
        foreach ($admin_users as $admin_user) {
            $title = "Ticket Received : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA new ticket is generated at MSJ with ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'new_ticket_request';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }

        // fetching support type users for this ticket type
        $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
        // sending notification
        foreach ($support_users as $support_user) {
            $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
            $title = "Ticket Received : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA new ticket is generated at MSJ with ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'new_ticket_request';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }


        return true;
    }

    public function sendReopenedTicketNotificationToAdminUsers($ticket_id)
    {
        $get_ticket = $this->Model_support_tickets->get($ticket_id, true);
        $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
        // sending notifications to all admin type users
        foreach ($admin_users as $admin_user) {
            $title = "Ticket Reopen Request : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA request to reopen ticket is generated at MSJ for ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }

        // fetching support type users for this ticket type
        $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
        // sending notification
        foreach ($support_users as $support_user) {
            $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
            $title = "Ticket Reopen Request : Ticket " . $get_ticket['ticket_id'];
            $message = "Dear " . $admin_user['full_name'] . "\nA request to reopen ticket is generated at MSJ for ticket no " . $get_ticket['ticket_id'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }


        return true;
    }

    public function sendTicketCommentReplyNotificationToAdminUsers($comment_id)
    {
        $get_comment = $this->Model_ticket_comments->get($comment_id, true, 'comment_id');
        $get_ticket = $this->Model_support_tickets->get($get_comment['ticket_id'], true);
        $admin_users = $this->Model_user->getMultipleRows(array('role_id' => 1), true); // fetching admin users
        // sending notifications to all admin type users
        foreach ($admin_users as $admin_user) {
            $title = "User Reply : Ticket " . $get_ticket['ticket_id'];
            $message = $get_comment['message'];
            $data['ticket_id'] = $get_ticket['id'];
            $data['type'] = 'ticket_reopened';
            sendNotification($title, $message, $data, $admin_user['user_id']);
        }

        if ($get_ticket['status'] == 'open' && ($get_ticket['closed_request'] == 0 || $get_ticket['closed_request'] == 1 || $get_ticket['closed_request'] == 2)) {
            // fetching support type users for this ticket type
            $support_users = $this->Model_support_user->getMultipleRows(array('complaint_type_id' => $get_ticket['type_id']));
            // sending notification
            foreach ($support_users as $support_user) {
                $admin_user = $this->Model_user->get($support_user->user_id, true, 'user_id');
                $title = "User Reply : Ticket " . $get_ticket['ticket_id'];
                $message = $get_comment['message'];
                $data['ticket_id'] = $get_ticket['id'];
                $data['type'] = 'ticket_reopened';
                sendNotification($title, $message, $data, $admin_user['user_id']);
            }
        }


        return true;
    }
}