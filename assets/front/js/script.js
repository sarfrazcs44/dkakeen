$(document).ready(function () {
    //$('#openAddToCartConfirmationModal').click();
    loadDragableMap();


    $(document).on('click', '#close_modal', function () {
        $('#addToCartConfirmationModal').modal('toggle');
    });

    $(document).on('click', '.add_to_cart', function () {
        var product_id = $(this).attr('data-pro-id');
        var product_quantity = $('#quantity_field').val();
        $.ajax({
            type: "POST",
            url: base_url + "page/add_to_cart",
            data: {'product_id': product_id, product_quantity: product_quantity},
            dataType: "json",
            complete: function () {


            },
            success: function (res) {
                // update chat & disable submit btn
                if (res.status == 'TRUE') {

                    if ($(".msg").hasClass('alert-danger')) {
                        $(".msg").removeClass('alert-danger');
                    }
                    $(".msg").addClass('alert-success');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');

                    if (res.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                $('#openAddToCartConfirmationModal').click();

                } else {

                    if ($(".msg").hasClass('alert-success')) {
                        $(".msg").removeClass('alert-success');
                    }
                    $(".msg").addClass('alert-danger');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');
                    if (res.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + res.url;
                        }, 1000);
                    }
                }

            }
        });
    });

    $(document).on('click', '.add_to_cart_on_products_page', function () {
        var product_id = $(this).attr('data-pro-id');
        $.ajax({
            type: "POST",
            url: base_url + "page/add_to_cart",
            data: {'product_id': product_id},
            dataType: "json",
            complete: function () {


            },
            success: function (res) {
                // update chat & disable submit btn
                if (res.status == 'TRUE') {
                    alert(res.message);
                    /*if($(this).parent('p').siblings(".msg").hasClass('alert-danger')){
                        $(this).parent('p').siblings(".msg").removeClass('alert-danger');
                    }
                    $(this).parent('p').siblings(".msg").addClass('alert-success');
                    $(this).parent('p').siblings(".msg").show();*/
                    //$(this).parent('p').siblings(".msg_text").html('<p>'+res.message+'</p>');

                    if (res.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 200);
                    }


                } else {

                    if ($(this).parent('p').siblings(".msg").hasClass('alert-success')) {
                        $(this).parent('p').siblings(".msg").removeClass('alert-success');
                    }
                    $(this).parent('p').siblings(".msg").addClass('alert-danger');
                    $(this).parent('p').siblings(".msg").show();
                    //$(this).parent('p').siblings(".msg_text").html('<p>'+res.message+'</p>');
                    if (res.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + res.url;
                        }, 1000);
                    }
                }

            }
        });
    });

    $(document).on('change', '.quantity', function () {
        var temp_order_id = $(this).attr('data-id');
        var product_quantity = $(this).val();
        // alert(temp_order_id);
        $.ajax({
            type: "POST",
            url: base_url + "checkout/update_order_product",
            data: {'temp_order_id': temp_order_id, 'product_quantity': product_quantity},
            dataType: "json",
            complete: function () {


            },
            success: function (res) {
                // update chat & disable submit btn
                if (res.status == 'TRUE') {

                    if ($(".msg").hasClass('alert-danger')) {
                        $(".msg").removeClass('alert-danger');
                    }
                    $(".msg").addClass('alert-success');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');

                    if (res.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }


                } else {

                    if ($(".msg").hasClass('alert-success')) {
                        $(".msg").removeClass('alert-success');
                    }
                    $(".msg").addClass('alert-danger');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');
                    if (res.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + res.url;
                        }, 1000);
                    }
                }

            }
        });
    });


    $(document).on('click', '.quantity_delete', function () {
        var temp_order_id = $(this).attr('data-id');

        if (confirm(delete_message)) {
            // alert(temp_order_id);
            $.ajax({
                type: "POST",
                url: base_url + "checkout/delete_order_product",
                data: {'temp_order_id': temp_order_id},
                dataType: "json",
                complete: function () {


                },
                success: function (res) {
                    // update chat & disable submit btn
                    if (res.status == 'TRUE') {

                        if ($(".msg").hasClass('alert-danger')) {
                            $(".msg").removeClass('alert-danger');
                        }
                        $(".msg").addClass('alert-success');
                        $(".msg").show();
                        $(".msg_text").html('<p>' + res.message + '</p>');

                        if (res.reload) {
                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }


                    } else {

                        if ($(".msg").hasClass('alert-success')) {
                            $(".msg").removeClass('alert-success');
                        }
                        $(".msg").addClass('alert-danger');
                        $(".msg").show();
                        $(".msg_text").html('<p>' + res.message + '</p>');
                        if (res.redirect) {
                            setTimeout(function () {
                                window.location.href = base_url + res.url;
                            }, 1000);
                        }
                    }

                }
            });
        }
    });

    $(document).on('click', '.use_this_address', function () {
        var address_id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: base_url + "address/set_default",
            data: {'address_id': address_id},
            dataType: "json",
            complete: function () {


            },
            success: function (res) {
                // update chat & disable submit btn
                if (res.status == 'TRUE') {

                    if ($(".msg").hasClass('alert-danger')) {
                        $(".msg").removeClass('alert-danger');
                    }
                    $(".msg").addClass('alert-success');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');

                    if (res.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }


                } else {

                    if ($(".msg").hasClass('alert-success')) {
                        $(".msg").removeClass('alert-success');
                    }
                    $(".msg").addClass('alert-danger');
                    $(".msg").show();
                    $(".msg_text").html('<p>' + res.message + '</p>');
                    if (res.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + res.url;
                        }, 1000);
                    }
                }

            }
        });
    });

    // on change hide/show fields
    /*$(".ticket_type").on('change', function (e) {

        /!*if($(this).val() == "complain"){
            //$(".visit_complain_fields").find(".form-control").val("");

        }else *!/
        if ($(this).val() == "visit") {
            //$(".visit_complain_fields").find(".form-control").val("");
            $(".complain_fields").hide();
            $(".visit_complain_fields").show();
        } else {
            $(".visit_complain_fields").hide();
            $(".complain_fields").show();
        }


    });*/

    $(".ticket_type").on('change', function (e) {
        var type_id = $(this).val();
        $.ajax({
            type: "POST",
            url: base_url + "support/getChildOptions",
            data: {'type_id': type_id},
            success: function (res) {
                $('.complain_child_options').html('');
                $('.complain_child_options').html(res);
            }
        });


    });

    // handle ticketing
    $(document).on('click', '.ticket-opener', function () {
        if ($(this).hasClass('ticket-open')) {
            $('.ticket-open.ticket-opener').removeClass('active-ticket');
            $(this).addClass('active-ticket');
            var ticket_id = $(this).attr('data-ticket_id');
            var target = $(".chatCommentsOpen");
            $(target).html("<div class='chat-loader'></div>");
            loadChat(ticket_id, target, $(this));

        } else if ($(this).hasClass('ticket-closed')) {
            $('.ticket-closed.ticket-opener').removeClass('active-ticket');
            $(this).addClass('active-ticket');
            var ticket_id = $(this).attr('data-ticket_id');
            var target = $(".chatCommentsClosed");
            $(target).html("<div class='chat-loader'></div>");
            loadChat(ticket_id, target, $(this));
        }
    });

    //closed ticket request by user

    $(document).on('click', '.closed_tic', function () {
        var ticket_id = $(this).attr('data-ticket_id');

        // OLD code, bilal ejaz commented on 11-04-2018
        /*if($(this).hasClass('yes')){
            var closed_request = 2;// full closed

		}else if($(this).hasClass('no')){
            var closed_request = 3;//wants to reopen
		}*/

        if ($(this).hasClass('yes')) {
            var closed_request = 3;//wants to reopen
        } else if ($(this).hasClass('no')) {
            var closed_request = 2;// full closed
        }


        $.ajax({
            type: "POST",
            url: base_url + "support/closed_reopen_by_user",
            data: {'closed_request': closed_request, 'ticket_id': ticket_id},
            dataType: "json",
            complete: function () {


            },
            success: function (res) {
                // update chat & disable submit btn
                if (res.status == 'TRUE') {
                    window.location.reload();
                }

            },
            error: function (error) {
                console.error(error);
            }
        });
    });

    // end closed ticket request by user


    $(document).on('click', '.sendChat', function () {
        var message = $("#chatMsgBox").val();
        var ticket_id = $('.ticket-open.active-ticket').attr("data-ticket_id");
        var elem = $(this);
        if (message.length > 0) {
            $(this).html("<div class='chat-loader-small'></div>");
            $.ajax({
                type: "POST",
                url: base_url + "support/store_message",
                data: {'message': message, 'ticket_id': ticket_id},
                dataType: "json",
                complete: function () {
                    $(elem).html("Send");
                    $("#chatMsgBox").val("");
                },
                success: function (res) {
                    // update chat & disable submit btn
                    if (res.status == 'TRUE') {
                        if (res.message) {
                            var msg = res.message;

                            // append new message
                            var new_msg_elem = getMsgElement(msg.message, msg.created_at, msg.who_said, msg.even_odd, msg.ticket_id, msg.comment_id);
                            $('.chatCommentsOpen').append(new_msg_elem);

                            // scroll div
                            var parent_elem = $(".tabs-content-ticketing");
                            $(parent_elem).animate({scrollTop: $(parent_elem).prop("scrollHeight")}, 1000);
                        }
                    }

                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    });


    // on window load show first ticket's chat
    loadChatOnWindowLoad();

    function loadChatOnWindowLoad() {
        var ticket_open = $('.ticket-open.ticket-opener').first();
        var ticket_closed = $('.ticket-closed.ticket-opener').first();
        if (ticket_open) {
            $('.ticket-open.ticket-opener').removeClass('active-ticket');
            $(ticket_open).addClass('active-ticket');
            var ticket_id = $(ticket_open).attr('data-ticket_id');
            var target = $(".chatCommentsOpen");
            loadChat(ticket_id, target, ticket_open);
        }
        if (ticket_closed) {
            var ticket_id = $(ticket_closed).attr('data-ticket_id');
            var target = $(".chatCommentsClosed");
            $(ticket_closed).removeClass('active-ticket');
            $(ticket_closed).addClass('active-ticket');
            loadChat(ticket_id, target, ticket_closed);
        }
    }

    function loadChat(ticket_id, target, thisElem=false) {
        if (ticket_id && target) {
            $.ajax({
                type: "POST",
                url: base_url + "support/get_comments",
                data: {'ticket_id': ticket_id},
                dataType: "json",
                success: function (res) {
                    if (res.status == 'TRUE') {
                        if (res.messages) {
                            $(target).html("");
                            $.each(res.messages, function (index, item) {
                                // append new message
                                var new_msg_elem = getMsgElement(item.message, item.created_at, item.who_said, item.even_odd);
                                $(target).append(new_msg_elem);
                            });
                            // scroll div
                            var parent_elem = $(".tabs-content-ticketing");
                            $(parent_elem).animate({scrollTop: $(parent_elem).prop("scrollHeight")}, 1000);

                            // remove unread elem
                            if (thisElem) {
                                thisElem.find('.no_unread').remove();
                            }
                        }
                    } else if (res.status == "FALSE") {
                        $(target).html("");
                    }

                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    }


    function checkNewMessage(ticket_id) {

    }


    $(document).on('change', ".main_categories", function () {
        var value = $(this).val();
        var lang = $(this).attr('data-lang');
        if (value) {
            $.ajax({
                type: "POST",
                url: base_url + "page/categories_get",
                data: {'ajax_call': true, 'parent_id': value},
                dataType: "json",
                success: function (res) {
                    if (res.status == 'TRUE') {
                        if (res.data) {
                            var opts = "";
                            $.each(res.data, function (index, item) {
                                if (lang == 'en') {
                                    opts += "<option value='" + item.category_id + "'> " + item.title_en + " </option>";
                                } else if (lang == 'ar') {
                                    opts += "<option value='" + item.category_id + "'> " + item.title_ar + " </option>";
                                }
                            });
                            $(".sub_categories option[value='-1']").nextAll().remove();
                            $(opts).insertAfter(".sub_categories option[value='-1']");
                            console.log(opts);
                        }
                    } else if (res.status == "FALSE") {
                        $(".sub_categories option[value='-1']").nextAll().remove();
                    }

                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    });


    function getMsgElement(msg, time, who_said, even_odd) {
        var left_right_class = (even_odd == 'even') ? 'rightside-left-chat' : 'rightside-right-chat';
        return '<div class=" ' + left_right_class + ' text-field-border">' +
            '<span style="margin:5px;"><small style="color:blue !important;font-size:14px !important;">' + who_said + '</small> </span>' +
            '<p>' + msg + '</p>' +
            '</div>';
    }



    var unsaved = false;
    $(".form_data").submit(function (e) {

        e.preventDefault();
        $form = $(this);

        //console.log('form', $form[0]);

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {


                if (result.error != 'false') {
                    if (!$($form[0]).find('.validatio-msg').hasClass('alert-danger')) {
                        $($form[0]).find('.validatio-msg').addClass('alert-danger');
                    }

                    $($form[0]).find(".validatio-msg").html(result.error);
                    $($form[0]).find('.validatio-msg').show();

                } else {
                    if ($($form[0]).find('.validatio-msg').hasClass('alert-danger')) {
                        $($form[0]).find('.validatio-msg').removeClass('alert-danger');
                    }
                    if (!$($form[0]).find('.validatio-msg').hasClass('alert-success')) {
                        $($form[0]).find('.validatio-msg').addClass('alert-success');
                    }

                    $($form[0]).find(".validatio-msg").html(result.success);
                    $($form[0]).find('.validatio-msg').show();
                    if (result.reset)
                        $(".form_data")[0].reset();
                    if (result.reload)
                        setTimeout(function () {
                            window.location.reload();
                        }, 1500);

                    if (result.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 3000);

                    }
                    //document.getElementById("show_success_messge").click();

                }


            }
        });
    });

});


function deleteRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete this?")) {
        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {'id': id, 'form_type': 'delete'},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != 'false') {
                    if ($('.validatio-msg').hasClass('alert-success')) {
                        $('.validatio-msg').removeClass('alert-success');
                    }
                    if (!$('.validatio-msg').hasClass('alert-danger')) {
                        $('.validatio-msg').addClass('alert-danger');
                    }

                    $(".validatio-msg").html(result.error);
                    $('.validatio-msg').show();

                } else {
                    $('#' + id).remove();
                    if ($('.validatio-msg').hasClass('alert-danger')) {
                        $('.validatio-msg').removeClass('alert-danger');
                    }
                    if (!$('.validatio-msg').hasClass('alert-success')) {
                        $('.validatio-msg').addClass('alert-success');
                    }

                    $(".validatio-msg").html(result.success);
                    $('.validatio-msg').show();
                    if (reloadUrl != "") setTimeout(function () {
                        document.location.href = reloadUrl;
                    }, 5000);
                }

            }
        });
        return true;
    } else {
        return false;
    }

}

$(".check_unique").keyup(function () {
    setTimeout(checkUnique(), 4000);
});

function checkUnique() {
    var username = $('.username').val();
    var email = $('.email').val();
    $.ajax({
        type: 'POST',
        url: base_url + "account/checkIfUsernameAvailable",
        data: {username: username, email: email},
        dataType: "json",
        cache: false,
        success: function (response) {
            if (response.status == false) {
                if (!$('.validatio-msg').hasClass('alert-danger')) {
                    $('.validatio-msg').addClass('alert-danger');
                }

                $(".validatio-msg").html(response.message);
                $('.validatio-msg').show();
                $('.submit_reg_form_btn').attr('disabled', true);

            } else {
                if (!$('.validatio-msg').hasClass('alert-danger')) {
                    $('.validatio-msg').removeClass('alert-danger');
                }

                $(".validatio-msg").html('');
                $('.validatio-msg').hide();
                $('.submit_reg_form_btn').attr('disabled', false);
            }
        }
    });
}

/*function deleteRecord(id,actionUrl,reloadUrl)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
				
			if(result.error != 'false'){
				$("#mySmallModalLabel").html('Fehler');
				$("#message").html(result.error);
				document.getElementById("show_success_messge").click();
				
			}else{
				$('#'+id).remove();
				$("#mySmallModalLabel").html('Erfolg');
				$("#message").html(result.success);
				document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			}
		});
		return true;
	} else {
		return false;
	}
	
}*/

function deleteImage(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete?")) {
        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {'id': id, 'form_type': 'deleteImage'},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != 'false') {
                    $("#mySmallModalLabel").html('Fehler');
                    $("#message").html(result.error);
                    document.getElementById("show_success_messge").click();

                } else {
                    $('#' + id).remove();
                    $("#mySmallModalLabel").html('Erfolg');
                    $("#message").html(result.success);
                    document.getElementById("show_success_messge").click();
                    if (reloadUrl != "") document.location.href = reloadUrl;
                }

            }
        });
        return true;
    } else {
        return false;
    }

}


function redirect(redirect_to) {
    redirect_to = base_url + redirect_to;
    window.location.href = redirect_to;
}

function changeStatus(id, actionUrl, reloadUrl, value) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to Change Status?")) {
        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {'id': id, 'value': value, 'form_type': 'changeStatus'},
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != 'false') {
                    $("#mySmallModalLabel").html('Fehler');
                    $("#message").html(result.error);
                    document.getElementById("show_success_messge").click();

                } else {
                    $("#mySmallModalLabel").html('Erfolg');
                    $("#message").html(result.success);
                    document.getElementById("show_success_messge").click();
                    if (reloadUrl != "") document.location.href = reloadUrl;
                }

            }
        });
        return true;
    } else {
        return false;
    }

}

$(document).on('submit', '.sendForgotPasswordEmail', function (e) {
    e.preventDefault();
    $form = $(this);
    $($form[0]).find('.validatio-msg').removeClass('alert-success');
    $($form[0]).find('.validatio-msg').removeClass('alert-danger');
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: new FormData(this),
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            if (response.status == false) {
                $($form[0]).find('.validatio-msg').addClass('alert-danger');
            } else {
                $($form[0]).find('.validatio-msg').addClass('alert-success');
            }

            $($form[0]).find(".validatio-msg").html(response.message);
            $($form[0]).find('.validatio-msg').show();
        }
    });
});

$(document).on('submit', '.forgotPassword', function (e) {
    e.preventDefault();
    $form = $(this);
    $($form[0]).find('.validatio-msg').removeClass('alert-success');
    $($form[0]).find('.validatio-msg').removeClass('alert-danger');
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: new FormData(this),
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            if (response.status == false) {
                $($form[0]).find('.validatio-msg').addClass('alert-danger');
            } else {
                $($form[0]).find('.validatio-msg').addClass('alert-success');
            }

            $($form[0]).find(".validatio-msg").html(response.message);
            $($form[0]).find('.validatio-msg').show();

            if (response.status == true) {
                setTimeout(function () {
                    window.location.href = base_url + 'page/login';
                }, 2000);
            }

        }
    });
});

function resendCode(user_id) {
    $('.validatio-msg').removeClass('alert-success');
    $('.validatio-msg').removeClass('alert-danger');
    $('.validatio-msg').html('');
    $('.validatio-msg').hide();
    $.ajax({
        type: "POST",
        url: base_url + 'account/resendVerificationCode',
        data: {user_id: user_id},
        success: function (response) {
            if (response == 1) {
                $('.validatio-msg').addClass('alert-success');
                $('.validatio-msg').html('Varification code is sent to your registered phone no in sms. Please check.');
                $('.validatio-msg').show();
            } else {
                $('.validatio-msg').addClass('alert-danger');
                $('.validatio-msg').html('Something went wrong. Please try again later.');
                $('.validatio-msg').show();
            }
        }
    });
}


$(document).on('keydown', '.only_number', function (event) {
    // Allow only backspace and delete
    if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 107) {
        // let it happen, don't do anything

    }
    else {
        // Ensure that it is a number and stop the keypress
        if ((event.keyCode !== 9) && (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 106)) {
            event.preventDefault();
        }
        else {

            if ($.trim($(this).val()) == '') {
                if (event.keyCode == 48) {
                    event.preventDefault();
                }
            }

        }
    }
});




// auto complete with map and draggable
function loadDragableMap() {
    if ($('.geocomplete_map_draggable').length > 0) {
        $(".geocomplete_map_draggable").geocomplete({
            details: ".GeoDetails",
            detailsAttribute: "data-geo",
            map: ".address_map",
            location: [24.7501875, 46.6879165],
            mapOptions: {
                zoom: 9
            },
            markerOptions: {
                draggable: true
            }
        });
        // draggable change lat long
        $(".geocomplete_map_draggable").bind("geocode:dragged", function (event, latLng) {
            $("input[name=lat]").val(latLng.lat());
            $("input[name=lng]").val(latLng.lng());
            var lat = latLng.lat();
            var lng = latLng.lng();

            $.ajax({
                url: 'http://maps.googleapis.com/maps/api/geocode/json',
                type: 'GET',
                data: {
                    'latlng': lat + ',' + lng,
                    'sensor': 'true'
                },
                success: function (data) {
                    console.log('Location: ', data['results']);
                    // var city = data['results'][0].address_components[3].long_name;
                    var address = data['results'][0].formatted_address;
                    $("input[name='location']").val(address);
                    $("input[name='geocomplete_map_draggable']").val(address);
                    $("input[name='formatted_address']").val(address);
                }

            });

        });
    }
}


$(function () {
    $('.marquee').marquee({
        duration: 10000,
        duplicated: true,
        gap: 20,
        pauseOnHover: true
    });
});