$(document).ready(function (e) {

    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCyZXiLUBHpAWvUhlDhlGcucX4pskYpzNs",
        authDomain: "msjapp-2e1d3.firebaseapp.com",
        databaseURL: "https://msjapp-2e1d3.firebaseio.com",
        projectId: "msjapp-2e1d3",
        storageBucket: "msjapp-2e1d3.appspot.com",
        messagingSenderId: "329086401422"
    };
    firebase.initializeApp(config);


    var
        user_id = USER_ID,
        user_full_name = USER_FULL_NAME,
        user_email = USER_EMAIL,
        receiver = RECEIVER,
        is_read = 0,
        total_unread_count = 0,
        chat_id = '',
        cps_data = [], // conversation partners data
        database = {},
        RootRef = firebase.database().ref('conversations'),
        chat_obj = {},
        is_end = 0,
        prev_chat_ids = [];


    complete_message_data();

    //on press enter send message
    $('.message').keyup(function (e) {
        if (e.keyCode == 13) {
            SaveData($('.message').val());
            $('.message').val('');
        }
    });


    $('.SendMessage').click(function () {
        SaveData($('.message').val());
        $('.message').val('');
    });


    $(document).on('click', '.delete_message', function (e) {
        e.preventDefault();
        var unique_id = $(this).attr('data-attr');
        database.child(unique_id).remove();
    });

    $(document).on('click', '.show_friend_messages', function (e) {
        e.preventDefault();

        $(".showChatBTN").attr('disabled', true);
        var customer_full_name = $(this).data('customer_full_name');
        var customer_email = $(this).data('customer_email');
        setCookie('chat_customer_full_name', customer_full_name, 1);
        setCookie('chat_customer_email', customer_email, 1);

        receiver = $(this).attr('data-attr');
        chat_id = user_id < receiver ? user_id + '-' + receiver : receiver + '-' + user_id;
        var chat_request_id = $(this).data('chat_request_id');
        $.ajax({
            url: base_url + 'cms/myChat/checkIFChatExist',
            type: 'post',
            dataType: 'json',
            data: {
                chat_request_id: chat_request_id
            },
            success: function (res) {
                if (res.exist) {

                    if (res.is_closed == 0 && res.is_in_progress == 0)
                    {
                        // send 1st default message
                        SaveData('Welcome to MSJ Customer Care Service, how may I help you?');
                        markChatIsInProgress(chat_request_id);

                        $('.show_messages').html('');
                        $('#chatDiv').modal({backdrop: 'static', keyboard: false});


                        $(this).addClass('active').siblings().removeClass('active');
                        // set_request_parms(chat_id);

                        // Detaches all callbacks
                        detach_all_conversations();

                        RootRef.child(chat_id).on('value', function (snapshot) {
                            var obj = snapshot.val();
                            ShowMessages(obj);
                        });

                        set_request_parms(chat_id);
                        set_messages_read(chat_obj, chat_id);
                    } else {
                        alert('Chat is in progress or closed');
                    }


                } else {
                    var chatTextId = "QueuedMsg" + receiver;
                    $("." + chatTextId).html("Closed");
                    $("." + chatTextId).removeClass("NotClosed");

                    $(".startchat" + receiver).attr('disabled', 'disabled');
                    alert('User left this chat');

                }
            }
        });


    });

    $(document).on('click', '.closeChat', function (e) {
        e.preventDefault();
        var chatTextId = "QueuedMsg" + $(this).attr('id');
        $("." + chatTextId).html("Closed");
        $("." + chatTextId).removeClass("NotClosed");
        $('#chatDiv').modal('hide');
        $(".startchat" + $(this).attr('id')).attr('disabled', 'disabled');
        is_end = 1;
        SaveData('Chat End');
        $.ajax({
            url: base_url + 'cms/myChat/change_status_closed',
            type: 'post',
            dataType: 'json',
            data: {
                chat_request_id: $(this).attr('id')
            },
            success: function (res) {
                receiver = res.receiver;
                is_end = 0;
            }
        });
    });

    function closeChatFromApi(api_chat_id) {
        // alert('here');
        //e.preventDefault();
        //alert(api_chat_id);
        var chatTextId = "QueuedMsg" + api_chat_id;
        $("." + chatTextId).html("Closed");
        $("." + chatTextId).removeClass("NotClosed");
        $('#chatDiv').modal('hide');
        $(".startchat" + api_chat_id).attr('disabled', 'disabled');
        is_end = 1;
        // firebase.database().ref('conversations/' + '0-'+api_chat_id).child(i).update({is_read : 1});
        $.ajax({
            url: base_url + 'cms/myChat/change_status_closed',
            type: 'post',
            dataType: 'json',
            data: {
                chat_request_id: api_chat_id
            },
            success: function (res) {
                is_end = 0;
                alert('User left this chat');
            }
        });
    }


    function detach_all_conversations() {
        for (var i in chat_obj) {
            RootRef.child(i).off('value');
        }
    }

    function set_messages_read(chat, chatID) {
        if (chat) {
            chatID = chatID === undefined ? Object.keys(chat)[0] : chatID;
            // var chatID = Object.keys(chat)[0];

            for (var i in chat[chatID]) {
                // Update status of unread message
                firebase.database().ref('conversations/' + chatID).child(i).update({is_read: 1});
            }
        }
    }

    function show_notifications(chatID) {
        RootRef.child(chatID).orderByChild('receiver').equalTo(user_id).on('child_added', function (data) {

            if (Notification.permission !== 'default') {
                if (data.val().is_read === 0) {
                    // var notify = new Notification(cps_data[receiver].email,{
                    var add_anchar = "<a href=" + base_url + 'user/messages' + ">";
                    var notify = new Notification(cps_data[get_cp_user_id(chatID)].email, {
                        'body': data.val().message,
                        'icon': base_url + 'assets_new/images/watermark_logo.png',
                        'tag': data.getKey()
                    });
                    notify.onclick = function (event) {
                        location.href = base_url + 'user/messages';
                        notify.close();
                        //window.open('http://www.mozilla.org', '_blank');
                    }
                    /*notify.onclose = function(event) {
                        console.log(notify);
                    }*/
                }
            } else {
                Notification.requestPermission().then(function (p) {
                });
            }
        });
    }

    function complete_message_data() {
        RootRef.on('value', function (snapshot) {
            var
                obj = snapshot.val(),
                msg_keys_tmp = [],
                latestMsgOfConversation = {},
                chatIdsWithLatestMsgTime = {};  //  i.e: {14-16: "2017-6-1 14:29:22", 15-16: "2017-6-2 5:41:49", 16-45: "2017-6-6 13:27:56"}

            for (var i in obj) {
                // Conversation user ids
                var conv_user_ids = i.split('-');

                // If logged in user is one of conversation users
                if (conv_user_ids.indexOf(user_id) !== -1) {
                    chat_obj[i] = obj[i];

                    // console.log(obj[i]);
                    msg_keys_tmp = Object.keys(obj[i]);
                    // console.log(msg_keys_tmp);
                    latestMsgOfConversation = obj[i][msg_keys_tmp[msg_keys_tmp.length - 1]];
                    // console.log(latestMsgOfConversation);
                    chatIdsWithLatestMsgTime[i] = latestMsgOfConversation.sending_date;
                }
            }

            chatIdsWithLatestMsgTime = sortChats(chatIdsWithLatestMsgTime);

            // Rearrange chat object as "chatIdsWithLatestMsgTime"
            chat_obj = sortChatsObject(chat_obj, chatIdsWithLatestMsgTime);

            ShowFriends(chat_obj);
            if (chat_obj) {
                var keys = Object.keys(chat_obj);
                if (keys.length && receiver === 0) {
                    // chat_id = keys[0];
                    chat_id = typeof requestedChatId === 'undefined' || requestedChatId === '' ? keys[0] : requestedChatId;
                    set_request_parms(chat_id);

                    RootRef.child(chat_id).on('value', function (snapshot) {
                        ShowMessages(snapshot.val());
                    });

                    // Set message as read, on page load
                    set_messages_read(chat_obj);
                }
            }

        });

    }

    function ShowMessages(obj) {
        if (obj) {
            var html = '';
            var cp_id = 0;
            var cp_user_ids = [];
            var keys = Object.keys(obj);
            cp_id = obj[keys[0]].sender === user_id ? obj[keys[0]].receiver : obj[keys[0]].sender;
            cp_user_ids[0] = cp_id;
            cp_user_ids[1] = user_id;
            for (var i in obj) {
                if (obj[i].sender == user_id) {
                    html += "<li class='clearfix'>";
                    html += "<div class='chat-avatar'>";
                    // html+= "<i>"+ getformatedDate(obj[i].sending_date) +"</i>";
                    html += "<i>" + convertTimestamp(obj[i].sending_date) + "</i>";
                    html += "</div><div class='conversation-text'><div class='ctext-wrap'>";
                    html += "<p>" + obj[i].message + "</p>";
                    html += "</div></div></li>";
                } else {
                    if (obj[i].is_end == '1') {
                        closeChatFromApi(obj[i].sender);
                        // here
                    }
                    html += "<li class='clearfix odd'>";
                    html += "<div class='chat-avatar'>";
                    // html+= "<i>"+ getformatedDate(ShowLocalDate()) +"</i>";
                    html += "<i>" + convertTimestamp(ShowLocalDateTimestamp()) + "</i>";
                    html += "</div><div class='conversation-text'><div class='ctext-wrap'>";
                    html += "<p>" + obj[i].message + "</p>";
                    html += "</div></div></li>";

                    // Update status of unread message
                    // database.child(i).update({is_read : 1});
                }
            }

            // Set conversation partner id
            cp_id = obj[keys[0]].sender === user_id ? obj[keys[0]].receiver : obj[keys[0]].sender;

            $('.show_messages').html(html);

            var el = document.querySelectorAll('.show_messages');
            $(".messages-main-div, .show_messages").animate({scrollTop: Object.keys(obj).length * 100});
            // alert('admin scroll here');

            var cp_user_interval = setInterval(function () {
                if (cps_data.length && cps_data[cp_id] !== undefined) {
                    $('.show_username').html(cps_data[cp_id].username);
                    $('.show_username').next('span').children('button.closeChat').attr('id', cp_id);
                    clearInterval(cp_user_interval);
                }
            }, 100);
        }
    }

    function ShowFriends(obj) {
        if (obj) {
            var
                html = '',
                dropdown_html = '',
                cp_user_id = '',
                cp_user_ids = [];

            for (var i in obj) {
                cp_user_id = get_cp_user_id(i);
                cp_user_ids.push(cp_user_id);


            }

            $.ajax({
                url: base_url + 'cms/myChat/get_users_data',
                type: 'post',
                dataType: 'json',
                data: {
                    user_ids: cp_user_ids
                },
                success: function (res) {
                    set_users_data(res);

                    total_unread_count = 0;
                    var unread_count = 0;
                    // Loop through object to create conversations list
                    for (var i in obj) {

                        // Attach notification on new chats
                        if (prev_chat_ids.indexOf(i) === -1) {
                            //show_notifications(i);
                        }

                        unread_count = 0;
                        // Loop through object of single conversation to get counter of unread messages
                        for (var j in obj[i]) {
                            var msg = obj[i][j];
                            if (msg.is_read == 0 && msg.receiver == user_id) {
                                unread_count++;
                            }
                        }

                        cp_user_id = get_cp_user_id(i);
                        //var email = cps_data[cp_user_id].email;
                        //var username = cps_data[cp_user_id].username;
                        var username = '';
                        $('.startchat' + cp_user_id).attr('data-attr', i);
                        if ($('.QueuedMsg' + cp_user_id).hasClass("NotClosed")) {
                            $('.QueuedMsg' + cp_user_id).html(unread_count + " Queued Message");
                        }
                        html += "<a href='' class='show_friend_messages' data-attr=" + i + " >";
                        html += "<span class='user-circle'><i class='fa fa-user user-icon' aria-hidden='true'></i></span>" + username;
                        html += "<span class='badge pull-right'>" + unread_count + "</span>";
                        html += "</a>";

                        if (unread_count > 0) {
                            dropdown_html += "<li>";
                            dropdown_html += "<a href='" + base_url + "user/messages?chat_id=" + i + "' class='' data-attr=" + i + " >";
                            dropdown_html += username;
                            dropdown_html += "<span class='badge pull-right'>" + unread_count + "</span>";
                            dropdown_html += "</a>";
                            dropdown_html += "</li>";
                        } else {

                            dropdown_html = "<li>You have no message yet</li>";
                        }

                        total_unread_count += unread_count;
                    }
                    // Update "prev_chat_ids"
                    prev_chat_ids = Object.keys(chat_obj);
                    $('.show_friends').html(html);
                    $('.unread_msg_count').html(total_unread_count);
                    $('.msg-menu').html(dropdown_html);

                    $('button.show_friend_messages').removeClass('active')
                    $('button.show_friend_messages[data-attr="' + chat_id + '"]').addClass('active');
                }
            });
        }

    }

    function get_cp_user_id(c_id) {
        var cp_user_id = c_id.split('-');
        return cp_user_id[0] == user_id ? cp_user_id[1] : cp_user_id[0];
    }

    function set_request_parms(chat_id) {
        receiver = get_cp_user_id(chat_id);
        database = firebase.database().ref('conversations/' + chat_id);
    }

    function SaveData(message) {
        if (message) {
            chat_id = user_id < receiver ? user_id + '-' + receiver : receiver + '-' + user_id;
            set_request_parms(chat_id);
            database.push().set({
                'sender': user_id,
                'receiver': receiver,
                // 'sender_email': user_email,
                'message': message,
                'is_read': is_read,
                // 'sending_date': ShowLocalDate(),
                'sending_date': ShowLocalDateTimestamp(),
                'is_end': is_end,
                // 'username': 'Admin',
                'customer_email': getCookie('chat_customer_email'),
                'customer_fullname': getCookie('chat_customer_full_name'),
                'support_email': user_email,
                'support_fullname': user_full_name
            });
        }
    }

    /**
     * @usage: It rearrange object of chat ids
     * @arg (chatIdsWithLatestMsgTime) (object):
     *  Structure should be like this: {14-16: "2017-6-1 14:29:22", 15-16: "2017-6-2 5:41:49", 16-45: "2017-6-6 13:27:56"}
     *  Afer sort it will rearranged as: {16-45: "2017-6-6 13:27:56", 15-16: "2017-6-2 5:41:49", 14-16: "2017-6-1 14:29:22"}
     */
    function sortChats(chatIdsWithLatestMsgTime) {
        var sortable = [];
        for (var i in chatIdsWithLatestMsgTime) {
            sortable.push([i, chatIdsWithLatestMsgTime[i]]);
        }

        sortable.sort(function (a, b) {
            return new Date(a[1]) > new Date(b[1]) ? -1 : 1;
        });

        chatIdsWithLatestMsgTime = {};
        for (var i = 0; i < sortable.length; i++) {
            chatIdsWithLatestMsgTime[sortable[i][0]] = sortable[i][1];
        }

        return chatIdsWithLatestMsgTime;
    }

    function sortChatsObject(obj, chatIdsWithLatestMsgTime) {
        var tmp_obj = {};

        for (var i in chatIdsWithLatestMsgTime) {
            tmp_obj[i] = obj[i];
        }

        return tmp_obj;
    }

    function get_email_by_id(users, user_id) {
        for (var i = users.length - 1; i >= 0; i--) {
            if (users[i].id == user_id) {
                return users[i].email;
            }
        }
    }

    function set_users_data(users) {
        for (var i = users.length - 1; i >= 0; i--) {
            cps_data[users[i].id] = {
                email: users[i].email,
                username: users[i].username
            };
        }
    }

    function ShowLocalDate() {
        var dNow = new Date();
        return localdate = dNow.getFullYear() + '-' + (dNow.getMonth() + 1) + '-' + dNow.getDate() + ' ' + dNow.getHours() + ':' + dNow.getMinutes() + ':' + dNow.getSeconds();
    }

    function ShowLocalDateTimestamp() {
        var dNow = new Date();
        var milliseconds = Math.floor((dNow.getTime()) / 1000);
        return milliseconds;
    }

    function convertTimestamp(timestamp) {
        var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
            dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh == 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        // time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
        time = dd + '-' + mm + '-' + yyyy + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    }


    function getformatedDate(dateString = new Date()) {
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        var monthNamesPrefix = [
            "Jan", "Feb", "Mar",
            "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oc",
            "Nov", "Dec"
        ];

        //2018-3-1 0:11 AM

        var dateTimeArr = dateString.split(" ");
        var dateArr = dateTimeArr[0].split('-');
        var timeArr = dateTimeArr[1].split(':');

        var
            year = dateArr[0],
            month = monthNamesPrefix[parseInt(dateArr[1]) - 1],
            date = dateArr[2],
            hours = timeArr[0],
            minutes = timeArr[1],
            am_pm = 'AM';

        if (hours >= 12) {
            hours -= 12;
            am_pm = 'PM';
        }
        if (hours == 0) {
            hours = 12;
        } else if (hours < 10) {
            hours = '0' + hours;
        }
        dateString = month + ' ' + date + ', ' + year + ' | ';
        dateString += hours + ':' + minutes + ' ' + am_pm;

        return dateString;
    }

    var email_user_id = {};
    /*Autocomplete Email Start*/
    var cache = {};
    var url = base_url + 'user/autocomplete';
    /*$('.new_msg_form input[name="new_user_name"]').autocomplete({
        minLength: 2,
        source: function( request, response ) {
            var term = request.term;
            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }
            $.getJSON(url, request, function( data, status, xhr ) {
                cache[ term ] = data;
                response( data );

                for(i in data){
                    email_user_id[data[i].id] = data[i].value;
                }
            });
        }
    });*/

    /*Autocomplete Email End*/


    $(document).on('submit', '.new_msg_form', function (e) {
        e.preventDefault();
        var
            form = $(this),
            new_user_name = form.find('[name="new_user_name"]').val(),
            new_message = form.find('[name="new_message"]').val();

        if (new_user_name && new_message) {

            $.each(email_user_id, function (key, valueObj) {

                if (valueObj == new_user_name) {
                    receiver = key;
                }
            });

            chat_id = user_id < receiver ? user_id + '-' + receiver : receiver + '-' + user_id;
            set_request_parms(chat_id);
            database.push().set({
                'sender': user_id,
                'receiver': receiver,
                // 'sender_email': user_email,
                'message': new_message,
                'is_read': is_read,
                //'sending_date': ShowLocalDate(),
                'sending_date': ShowLocalDateTimestamp(),
                // 'username': 'Admin',
                'customer_email': getCookie('chat_customer_email'),
                'customer_fullname': getCookie('chat_customer_full_name'),
                'support_email': user_email,
                'support_fullname': user_full_name
            });
            setTimeout(function () {
                $('#new_message_modal').modal('hide');
            }, 1000);


            // Detaches all callbacks
            detach_all_conversations();

            RootRef.child(chat_id).on('value', function (snapshot) {
                ShowMessages(snapshot.val());
            });

            $('#email').val('');
            $('#new_message').val('');
        }


    });

    setInterval(getTheLatestChats, 5000);

    function getTheLatestChats() {
        var lastChatId = $('#getChatId').val();
        $.ajax({
            url: base_url + 'cms/myChat/getLatestChat',
            type: 'post',
            dataType: 'json',
            data: {
                lastChatId: lastChatId

            },
            success: function (res) {
                if (res.hasNewChat) {
                    $('#newChatTbody').append(res.html);
                    $('#getChatId').val(res.LastChatId)
                }
            }
        });
    }

    /*setInterval(function() {
        console.log('chat_id: ' + chat_id);
        console.log('user_id' + user_id);
        console.log('receiver' + receiver);
    }, 2000);*/
});

function markChatIsInProgress(chat_id) {
    $.ajax({
        url: base_url + 'cms/myChat/mark_chat_in_progress',
        type: 'post',
        dataType: 'json',
        data: {
            chat_id: chat_id

        },
        success: function (res) {
            console.log("Chat marked as is in progress");
        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}