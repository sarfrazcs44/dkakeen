$(document).ready(function () {

    $(document).on('click', '.map_modal', function () {
        var lat = $(this).attr('data-lat');
        var lng = $(this).attr('data-lng');

        $(".map_with_pin").geocomplete({
            map: ".map_with_pin",
            location: [lat,lng],
            mapOptions: {
                zoom: 16
            }
        });
    });

    $(document).on('click', '.sendChat', function () {
        var message = $("#chatMsgBox").val();
        var ticket_id = $(this).attr("data-ticketId");
        var elem = $(this);
        if(message.length > 0){
            $(this).html("<div class='chat-loader'></div>");
            $.ajax({
                type: "POST",
                url: base_url + "cms/support/store_message",
                data: {'message': message, 'ticket_id': ticket_id},
                dataType: "json",
                complete: function () {
                    $(elem).html("Send");
                    $("#chatMsgBox").val("");
                },
                success: function(res){
                    // update chat & disable submit btn
                    if(res.status == 'TRUE'){
                        if(res.message){
                            var msg = res.message;

                            // append new message
                            var new_msg_elem = getMsgElement(msg.message, msg.created_at, msg.who_said, msg.even_odd, msg.ticket_id, msg.comment_id);
                            // $('.conversation-list').append(new_msg_elem); // commented by Bilal on 03-01-2019

                            // scroll div
                            var bottomCoord = $('.conversation-list')[0].scrollHeight;
                            // $('.conversation-list').slimScroll({scrollTo: bottomCoord}); // commented by Bilal on 03-01-2019
                        }
                    }

                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    });

    //update status
    $(document).on('click', '.updateStatus', function () {
        var ticket_id = $(this).attr('data-ticketid');
        var conf = confirm("Are you sure to close this ticket?");
        if (conf == true){
            $.ajax({
                type: "POST",
                url: base_url + "cms/support/close_status",
                data: {'ticket_id': ticket_id},
                dataType: "json",
                success: function (res) {
                    if (res.status == 'TRUE') {
                        location.reload();
                    }
                },
                error: function (error) {
                    console.error(error);
                }
            });
        }
    });

    // setInterval(updateChat, 10000);

    function updateChat(){
        var ticket_id = $('.item_message').last().attr('data-ticket_id');
        var lastCommentId = $('.item_message').last().attr('data-comment_id');

        $.ajax({
            type: "POST",
            url: base_url + "cms/support/check_new_message",
            data: {'ticket_id': ticket_id, 'lastCommentId': lastCommentId},
            dataType: "json",
            success: function(res){
                // update chat & disable submit btn
                if(res.status == 'TRUE'){
                    if(res.message){
                        var msg = res.message;
                        // append new message
                        var new_msg_elem = getMsgElement(msg.message, msg.created_at, msg.who_said, msg.even_odd, msg.ticket_id, msg.comment_id);
                        $('.conversation-list').append(new_msg_elem);

                        // scroll div
                        var bottomCoord = $('.conversation-list')[0].scrollHeight;
                        $('.conversation-list').slimScroll({scrollTo: bottomCoord});
                    }
                }

            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    function getMsgElement(msg, time, who_said, even_odd, ticketId, commentId) {
        return  '<li class="clearfix item_message '+even_odd+' " data-ticket_id="'+ticketId+'" data-comment_id="'+commentId+'">' +
            '<div class="chat-avatar">' +
            '<span><strong>'+who_said+':</strong></span>' +
            '<i>'+time+'</i>' +
            '</div>' +
            '<div class="conversation-text">' +
            '<div class="ctext-wrap">' +
            '<p>'+msg+'</p>' +
            '</div>' +
            '</div>' +
            '</li>';
    }

});