-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Sep 04, 2020 at 06:40 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `dkakeen`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location` varchar(500) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `building_no` varchar(255) NOT NULL,
  `floor_no` varchar(255) NOT NULL,
  `apartment_no` varchar(255) NOT NULL,
  `is_default` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`address_id`, `user_id`, `location`, `city_id`, `district_id`, `country`, `lat`, `lng`, `building_no`, `floor_no`, `apartment_no`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 31, '2444 شارع طه خصيفان, الشاطى, جدّة‎, 23511, منطقة مكة, المملكة العربية السعودية', 1, 2, '', '21.602263685809543', '39.10833853759481', '34', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `ad_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `ad_title_en` varchar(255) NOT NULL,
  `ad_title_ar` varchar(255) NOT NULL,
  `ad_description_en` varchar(2000) NOT NULL,
  `ad_description_ar` varchar(2000) NOT NULL,
  `image` varchar(2000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_orders_for_delivery`
--

CREATE TABLE `assigned_orders_for_delivery` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `assigned_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `description_en` varchar(2000) NOT NULL,
  `description_ar` varchar(2000) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `title_en`, `title_ar`, `description_en`, `description_ar`, `image`, `created_at`, `updated_at`, `is_active`) VALUES
(10, 'Vimto', 'فيمتو', '', '', 'uploads/brand_images/3852559165120200422013101meat_and_pultry_mobile.jpg', '2018-02-28 11:22:43', '2020-04-22 13:01:31', 1),
(22, ' Suprema Biostar', ' Suprema Biostar', '', '', 'uploads/brand_images/8515350385020180814103823ss.png', '2018-08-14 10:23:38', '2018-08-14 10:23:38', 1),
(23, 'EAS Systems - Ad Guard ', 'EAS Systems - Ad Guard ', '', '', 'uploads/brand_images/6216264037620181105084531logo.png', '2018-11-05 08:31:45', '2018-11-05 08:31:45', 1),
(24, 'Goody', 'قودي', '', '', 'uploads/brand_images/6026280102202008180218011.jpg', '2020-08-18 14:01:18', '2020-08-18 14:01:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `id` int(11) NOT NULL,
  `full_name` varchar(60) NOT NULL,
  `company_name` varchar(255) NOT NULL COMMENT 'for become a seller request',
  `director_name` varchar(255) NOT NULL COMMENT 'for become a seller request',
  `email` varchar(120) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `request_for` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for career 1 for become a seller',
  `nationality` varchar(120) NOT NULL,
  `file` varchar(255) NOT NULL,
  `to_be_notified` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `description_en` varchar(2000) NOT NULL,
  `title_ar` varchar(255) NOT NULL,
  `description_ar` varchar(2000) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image_mbl` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `hide` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `parent_id`, `title_en`, `description_en`, `title_ar`, `description_ar`, `image`, `image_mbl`, `created_at`, `updated_at`, `hide`, `is_active`) VALUES
(4, 2, 'Child Category', '', 'Child Category Arb', '', 'uploads/category_images/88091285925201802230533561516149531946.JPEG.jpg', '', '2018-02-23 09:19:53', '2018-02-24 20:18:04', 0, 1),
(5, 2, 'Child Category 2 ', '', 'Child Category 2 arb', '', 'uploads/category_images/6015417576201802230926200fb16035-95a7-42aa-8f8f-41a1fb136162.jpg', '', '2018-02-23 09:20:26', '2018-02-23 09:20:26', 0, 1),
(8, 7, 'Sub tITL ENGLISH', '', 'Sub Title Arabic', '', 'uploads/category_images/37819141825201802240127531516149532113.JPEG.jpg', '', '2018-02-24 13:53:27', '2018-02-24 13:53:27', 0, 1),
(9, 2, ' basittaha', '', 'hihihihihi', '', 'uploads/category_images/986383905132018022403330103Success.jpg', '', '2018-02-24 15:01:33', '2018-02-24 15:01:33', 0, 1),
(12, 11, 'jjj', '', 'kk\\', '', 'uploads/category_images/7759672003320180224082446cctv_2.png', '', '2018-02-24 20:46:24', '2018-02-24 20:46:24', 0, 1),
(14, 13, 'Basit', '', 'nfv', '', 'uploads/category_images/5560315197020180225013851logoHIKVision.png', '', '2018-02-25 13:51:38', '2018-02-25 13:51:38', 0, 1),
(16, 15, 'abc', '', 'abc', '', 'uploads/category_images/8678642485120180227093238City.jpg', '', '2018-02-27 21:38:32', '2018-02-27 21:38:32', 0, 1),
(17, 0, 'Fruits & vegetables', '', 'فواكه وخضراوات', '', 'uploads/category_images/38481861171202007151201502020-07-15.jpg', 'uploads/category_images/4441862143202007151249502020-07-15_(1).jpg', '2018-02-28 07:00:20', '2020-07-15 12:50:49', 0, 1),
(31, 30, 'Coming soon', '', 'Coming soon', '', 'uploads/category_images/8282157512720180712050425Product-Image-Coming-Soon.png', 'uploads/category_images/395066944120180712050425Product-Image-Coming-Soon.png', '2018-03-01 07:29:50', '2018-07-12 17:25:04', 0, 1),
(40, 18, 'Network Video Recorder ', '', 'Network Video Recorder ', '', 'uploads/category_images/8572475144820180812014403xrn-1610_f.png', 'uploads/category_images/2825553817920180812014403xrn-1610_f.png', '2018-08-12 13:03:44', '2018-08-12 13:03:44', 0, 1),
(41, 18, 'Wisenet X Series', '', 'Wisenet X Series', '', 'uploads/category_images/7534191999320180813074232x_ser.png', 'uploads/category_images/1585319548320180813074232x_ser.png', '2018-08-13 07:31:18', '2018-08-13 07:32:42', 0, 1),
(42, 18, 'Wisenet P Series', '', 'Wisenet P Series', '', 'uploads/category_images/6611042654920180813073839p_ser.png', 'uploads/category_images/6935601568720180813073839p_ser.png', '2018-08-13 07:39:38', '2018-08-13 07:39:50', 0, 1),
(43, 18, 'Wisenet Q Series', '', 'Wisenet Q Series', '', 'uploads/category_images/5079337110020180813071746q_ser.png', 'uploads/category_images/5889719069220180813071746q_ser.png', '2018-08-13 07:46:17', '2018-08-13 07:46:17', 0, 1),
(44, 18, 'Wisenet T Series', '', 'Wisenet T Series', '', 'uploads/category_images/9667542973220180813105004t_ser.png', 'uploads/category_images/4921271018920180813105004t_ser.png', '2018-08-13 10:04:50', '2018-08-13 10:04:50', 0, 1),
(45, 18, 'Wisenet L Series', '', 'Wisenet L Series', '', 'uploads/category_images/136270522120180813105633l_ser.png', 'uploads/category_images/9875634404220180813105633l_ser.png', '2018-08-13 10:33:56', '2018-08-13 10:33:56', 0, 1),
(46, 19, 'Network Integrated Controller', '', 'Network Integrated Controller', '', 'uploads/category_images/39685018617201808131128031331.png', 'uploads/category_images/11843648752201808131128031331.png', '2018-08-13 11:03:28', '2018-08-13 11:03:28', 0, 1),
(47, 36, 'Zigbee ', '', 'Zigbee ', '', 'uploads/category_images/6813528744620180813024703or.png', 'uploads/category_images/9572606515920180813024703or.png', '2018-08-13 14:03:47', '2018-08-13 14:03:47', 0, 1),
(54, 19, 'HID Readers', '', 'HID Readers', '', 'uploads/category_images/5097191102220180813043754readers.png', 'uploads/category_images/8134541400020180813043754readers.png', '2018-08-13 16:54:37', '2018-08-13 16:54:37', 0, 1),
(57, 19, 'Suprema Biostar', '', 'Suprema Biostar', '', 'uploads/category_images/4206045742220180813054316bio.png', 'uploads/category_images/1440402599020180813054316bio.png', '2018-08-13 17:16:43', '2018-08-13 17:16:43', 0, 1),
(58, 19, 'HID Cards', '', 'HID Cards', '', 'uploads/category_images/2236061122720180813051517card.png', 'uploads/category_images/6458416809520180813051517card.png', '2018-08-13 17:17:15', '2018-08-13 17:17:15', 0, 1),
(59, 19, 'Hidden Gates', '', 'Hidden Gates', '', 'uploads/category_images/7126322410520180813054317sgate.png', 'uploads/category_images/7534173768720180813054317sgate.png', '2018-08-13 17:17:43', '2018-08-14 12:25:45', 0, 1),
(60, 49, 'Firebrand Fire Panels', '', 'Firebrand Addressable Fire Panel', '', 'uploads/category_images/4423326587920180814071102f1.png', 'uploads/category_images/1915135353820180814071102f1.png', '2018-08-14 07:02:11', '2018-08-14 07:35:22', 0, 1),
(61, 48, 'UTC Intrusion Alarm System', '', 'UTC Intrusion Alarm System', '', 'uploads/category_images/3980418472820180814070747sfas.png', 'uploads/category_images/2508503300420180814070747sfas.png', '2018-08-14 07:47:07', '2018-08-14 07:47:07', 0, 1),
(62, 51, 'Metal Detection Technology', '', 'Metal Detection Technology', '', 'uploads/category_images/8775950406120180814122047hf.png', 'uploads/category_images/7215543723620180814122047hf.png', '2018-08-14 12:47:20', '2018-08-14 12:47:20', 0, 1),
(63, 53, 'Pedestal EAS Systems - Ad Guard  ', '', 'Pedestal EAS Systems - Ad Guard  ', '', 'uploads/category_images/798086962201811050830111-160415154Z9457.png', 'uploads/category_images/48465416238201811050830111-160415154Z9457.png', '2018-11-05 08:11:30', '2018-11-05 08:37:24', 0, 1),
(64, 0, 'Meat & Poultry', '', 'لحم و دواجن', '', 'uploads/category_images/4149079959020200418073325meat_and_pultry.jpg', 'uploads/category_images/7668659589520200418073325meat_and_pultry_mobile.jpg', '2020-04-18 19:25:33', '2020-04-18 19:30:05', 0, 1),
(65, 0, 'Milk & Dairy', '', 'الحليب والألبان', '', 'uploads/category_images/6401795992920200418073429milk.jpg', 'uploads/category_images/1206025643420200418073429milk_mobile.jpg', '2020-04-18 19:29:34', '2020-04-18 19:29:34', 0, 1),
(66, 0, 'Frozen Items', '', 'منتجات مجمدة', '', 'uploads/category_images/9846332191020200418075734frozen.jpg', 'uploads/category_images/562592757120200418075734frozen_mobile.jpg', '2020-04-18 19:34:57', '2020-04-18 19:34:57', 0, 1),
(67, 0, 'Beverages', '', 'مشروبات', '', 'uploads/category_images/3851443408120200418073838beverages.jpg', 'uploads/category_images/8287383028120200418073838beverages_mobile.jpg', '2020-04-18 19:38:38', '2020-04-18 19:38:38', 0, 1),
(68, 0, 'Snacks', '', 'وجبات خفيفة', '', 'uploads/category_images/8839745441920200418073943snacks.jpg', 'uploads/category_images/1311129972220200418073943snacks_mobile.jpg', '2020-04-18 19:43:39', '2020-04-18 19:43:39', 0, 1),
(69, 17, 'Fruits', '', 'الفاكهة', '', 'uploads/category_images/7278188008020200418112609fruits.jpg', 'uploads/category_images/7605922531320200418112609fruits_mobile.jpg', '2020-04-18 23:09:26', '2020-04-18 23:09:26', 0, 1),
(70, 17, 'Vegetables', '', 'خضروات', '', 'uploads/category_images/8618783697720200418113611veges_pc.jpg', 'uploads/category_images/2198032804420200418113611veges.jpg', '2020-04-18 23:11:36', '2020-04-18 23:11:36', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat_request`
--

CREATE TABLE `chat_request` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_admin` int(11) NOT NULL,
  `is_closed` int(11) NOT NULL DEFAULT '0',
  `is_in_progress` int(11) NOT NULL,
  `to_be_notified` enum('yes','no') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `eng_name` char(35) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `countrycode` char(3) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `arb_name` varchar(500) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=FIXED;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `eng_name`, `countrycode`, `arb_name`) VALUES
(1, 'Jeddah', '', 'جدة'),
(3, 'Makkah', '', 'مكة');

-- --------------------------------------------------------

--
-- Table structure for table `complaint_types`
--

CREATE TABLE `complaint_types` (
  `complaint_type_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `complaint_title_en` varchar(255) NOT NULL,
  `complaint_title_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `placeholder_en` varchar(255) NOT NULL,
  `placeholder_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint_types`
--

INSERT INTO `complaint_types` (`complaint_type_id`, `parent_id`, `complaint_title_en`, `complaint_title_ar`, `placeholder_en`, `placeholder_ar`, `is_active`) VALUES
(1, 0, 'Maintenance Request', 'طلب صيانة', 'Service Type', 'نوع الخدمة', 1),
(2, 0, 'Sale and Marketing Request', 'طلبات المبيعات والتسويق', 'Request Type', 'نوع الطلب', 1),
(3, 0, 'Technical Support', 'الدعم الفني', 'Request Type', 'نوع الطلب', 1),
(4, 0, 'Complaints', 'الشكاوى', 'Select Department', 'الرجاء إختيار الإدارة', 1),
(5, 1, 'CCTV Systems', 'كاميرات المراقبة', '', '', 1),
(6, 1, 'Access Control Systems', 'نظام التحكم بالدخول والخروج', '', '', 1),
(7, 1, 'Smart Home Systems', 'أنظمة البيت الذكي', '', '', 1),
(8, 1, 'Intrusion Alarm systems', 'نظام الإنذار ضد السرقة', '', '', 1),
(9, 1, 'Fire Alarm Systems', 'نظام الإنذار ضد الحريق', '', '', 1),
(10, 1, 'Time Attendance systems', 'أنظمة الحضور والإنصراف', '', '', 1),
(11, 1, 'Metal and Contraband Detector', 'أنظمة فحص وتفتيش الحقائب والطرود وكشف المعادن', '', '', 1),
(12, 1, 'Paging and Music Systems', 'نظام صوتي للنداء والوسائط', '', '', 1),
(13, 1, 'Anti-Shoplifting Systems', 'أنظمة حماية المعروضات من السرقة', '', '', 1),
(14, 1, 'Other Systems', 'أنظمة أخرى', '', '', 1),
(15, 2, 'Quotation Request', 'طلب عرض سعر', '', '', 1),
(16, 2, 'Site survey Request', 'طلب مسح موقع', '', '', 1),
(17, 2, 'Become a re-seller', 'طلب توزيع لمنتجات الشركة', '', '', 1),
(18, 2, 'Others', 'أخرى', '', '', 1),
(19, 3, 'Remote Support', 'دعم عن بعد', '', '', 1),
(20, 3, 'Technical Enquiries', 'إستفسارات فنيةّ', '', '', 1),
(21, 3, 'Others', 'أخرى', '', '', 1),
(22, 4, 'Sales and marketing Team', 'طاقم المبيعات والتسويق', '', '', 1),
(23, 4, 'Maintenance Team', 'طاقم الصيانة', '', '', 1),
(24, 4, 'Project Team', 'طاقم المشاريع', '', '', 1),
(25, 4, 'Support Team', 'طاقم الدعم', '', '', 1),
(26, 4, 'IT Team', 'الشبكات وتكنولوجيا المعلومات', '', '', 1),
(27, 4, 'Admin Team', 'طاقم  الإدارة', '', '', 1),
(28, 4, 'Others', 'أخرى', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `complaint_types_new`
--

CREATE TABLE `complaint_types_new` (
  `complaint_type_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `complaint_title_en` varchar(255) NOT NULL,
  `complaint_title_ar` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complaint_types_new`
--

INSERT INTO `complaint_types_new` (`complaint_type_id`, `parent_id`, `complaint_title_en`, `complaint_title_ar`) VALUES
(1, 0, 'Maintenance Request', 'طلب صيانة'),
(2, 0, 'Sale and Marketing Request', 'طلبات المبيعات والتسويق'),
(3, 0, 'Technical Support', 'الدعم الفني'),
(4, 0, 'Complaints', 'الشكاوى'),
(5, 1, 'CCTV Systems', 'كاميرات المراقبة'),
(6, 1, 'Access Control Systems', 'نظام التحكم بالدخول والخروج'),
(7, 1, 'Smart Home Systems', 'أنظمة البيت الذكي'),
(8, 1, 'Intrusion Alarm systems', 'نظام الإنذار ضد السرقة'),
(9, 1, 'Fire Alarm Systems', 'نظام الإنذار ضد الحريق'),
(10, 1, 'Time Attendance systems', 'أنظمة الحضور والإنصراف'),
(11, 1, 'Metal and Contraband Detector', 'أنظمة فحص وتفتيش الحقائب والطرود وكشف المعادن'),
(12, 1, 'Paging and Music Systems', 'نظام صوتي للنداء والوسائط'),
(13, 1, 'Anti-Shoplifting Systems', 'أنظمة حماية المعروضات من السرقة'),
(14, 1, 'Other Systems', 'أنظمة أخرى'),
(15, 2, 'Quotation Request', 'طلب عرض سعر'),
(16, 2, 'Site survey Request', 'طلب مسح موقع'),
(17, 2, 'Become a re-seller', 'طلب توزيع لمنتجات الشركة'),
(18, 2, 'Others', 'أخرى'),
(19, 3, 'Remote Support', 'دعم عن بعد'),
(20, 3, 'Technical Enquiries', 'إستفسارات فنيةّ'),
(21, 3, 'Others', 'أخرى'),
(22, 4, 'Sales and marketing Team', 'طاقم المبيعات والتسويق'),
(23, 4, 'Maintenance Team', 'طاقم الصيانة'),
(24, 4, 'Project Team', 'طاقم المشاريع'),
(25, 4, 'Support Team', 'طاقم الدعم'),
(26, 4, 'IT Team', 'الشبكات وتكنولوجيا المعلومات'),
(27, 4, 'Admin Team', 'طاقم  الإدارة'),
(28, 4, 'Others', 'أخرى');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `eng_country_name` varchar(100) NOT NULL DEFAULT '',
  `arb_country_name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `phone_code` varchar(500) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `eng_country_name`, `arb_country_name`, `code`, `phone_code`, `latitude`, `longitude`) VALUES
(1, 'United States', 'الولايات المتحدة', 'USA', '+1', '37.09024 ', '-95.712891  '),
(2, 'Canada', 'كندا', 'CAN', '+1', '56.130366 ', '-106.346771 '),
(3, 'Afghanistan', 'أفغانستان', 'AFG', '+93', '33.93911', '67.709953  '),
(4, 'Albania', 'ألبانيا', 'ALB', '+355', '41.153332', '20.168331  '),
(5, 'Algeria', 'الجزائر', 'DZA', '+213', '28.033886', '1.659626'),
(6, 'American Samoa', 'ساموا-الأمريكي', 'ASM', '', '', ''),
(7, 'Andorra', 'أندورا', 'AND', '+376', '42.546245', '1.601554'),
(8, 'Angola', 'أنغولا', 'AGO', '+244', '-11.202692  ', '17.873887 '),
(9, 'Anguilla', 'أنغيلا', 'AIA', '+1', '18.220554', '-63.068615 '),
(10, 'Antarctica', 'أنتاركتيكا', 'ATA', '', '-75.250973 ', '-0.071389 '),
(11, 'Antigua and/Barbuda', 'أنتيغوا وبربودا', 'ATG', '+1', '17.060816', '-61.796428  '),
(12, 'Argentina', 'الأرجنتين', 'ARG', '+54', '-38.416097  ', '-63.616672  '),
(13, 'Armenia', 'أرمينيا', 'ARM', '+374', '40.069099', '45.038189'),
(14, 'Aruba', 'أروبا', 'ABW', '+297', '12.52111 ', '-69.968338  '),
(15, 'Australia', 'أستراليا', 'AUS', '+61', '-25.274398 ', '133.775136  '),
(16, 'Austria', 'النمسا', 'AUT', '+43', '47.516231', '14.550072'),
(17, 'Azerbaijan', 'أذربيجان', 'AZE', '+994', '40.143105  ', '47.576927 '),
(18, 'Bahamas', 'الباهاماس', 'BHS', '+1', '25.03428 ', '-77.39628 '),
(19, 'Bahrain', 'البحرين', 'BHR', '+973', '25.930414', '50.637772 '),
(20, 'Bangladesh', 'بنغلاديش', 'BGD', '+880', '23.684994', '90.356331'),
(21, 'Barbados', 'بربادوس', 'BRB', '+1', '13.193887', '-59.543198 '),
(22, 'Belarus', 'روسيا البيضاء', 'BLR', '+375', '53.709807', '27.953389 '),
(23, 'Belgium', 'بلجيكا', 'BEL', '+32', '50.503887', '4.469936'),
(24, 'Belize', 'بيليز', 'BLZ', '+501', '17.189877 ', '-88.49765 '),
(25, 'Benin', 'بنين', 'BEN', '+229', '9.30769 ', '2.315834'),
(26, 'Bermuda', 'جزر برمود', 'BMU', '+1', '32.321384  ', '-64.75737 '),
(27, 'Bhutan', 'بوتان', 'BTN', '+975', '27.514162', '90.433601  '),
(28, 'Bolivia', 'بوليفيا', 'BOL', '+591', '-16.290154 ', '-63.588653  '),
(29, 'Bosnia and Herzegovina', 'البوسنة و الهرسك', 'BIH', '+387', '43.915886  ', '17.679076'),
(30, 'Botswana', 'بوتسوانا', 'BWA', '+267', '-22.328474 ', '24.684866 '),
(31, 'Bouvet Island', 'جزيرة بوفيت', 'BVT', '', '-54.423199 ', '3.413194  '),
(32, 'Brazil', 'البرازيل', 'BRA', '+55', '-14.235004  ', '-51.92528 '),
(33, 'British Indian Ocean Territory', 'إقليم المحيط الهندي البريطاني', 'IOT', '+246', '-6.343194 ', '71.876519'),
(34, 'Brunei Darussalam', 'بروناي دار السلام', 'BRN', '+673', '4.535277', '114.727669'),
(35, 'Bulgaria', 'بلغاريا', 'BGR', '+359', '', ''),
(36, 'Burkina Faso', 'بوركينا فاسو', 'BFA', '+226', '12.238333  ', '-1.561593 '),
(37, 'Burundi', 'بوروندي', 'BDI', '+257', '-3.373056  ', '29.918886'),
(38, 'Cambodia', 'كمبوديا', 'KHM', '+855', '12.565679 ', '104.990963'),
(39, 'Cameroon', 'كاميرون', 'CMR', '+237', '7.369722', '12.354722 '),
(40, 'Cape Verde', 'الرأس الأخضر', 'CPV', '+238', '16.002082  ', '-24.013197  '),
(41, 'Cayman Islands', 'جزر كايمان', 'CYM', '+1', '19.513469', '-80.566956  '),
(42, 'Central African Republic', 'جمهورية افريقيا الوسطى', 'CAF', '+236', '6.611111', '20.939444'),
(43, 'Chad', 'تشاد', 'TCD', '+235', '15.454166  ', '18.732207'),
(44, 'Chile', 'تشيلي', 'CHL', '+56', '-35.675147  ', '-71.542969  '),
(45, 'China', 'الصين', 'CHN', '+86', '35.86166  ', '104.195397  '),
(46, 'Christmas Island', 'جزيرة الكريسماس', 'CXR', '', '-10.447525  ', '105.690449  '),
(47, 'Cocos (Keeling) Islands', 'جزر كوكوس ( كيلينغ)', 'CCK', '', '-12.164165 ', '96.870956 '),
(48, 'Colombia', 'كولومبيا', 'COL', '+57', '4.570868  ', '-74.297333  '),
(49, 'Comoros', 'جزر القمر', 'COM', '+269', '-11.875001 ', '43.872219 '),
(50, 'Congo', 'الكونغو', 'COG', '+242', '-0.228021  ', '15.827659 '),
(51, 'Cook Islands', 'جزر كوك', 'COK', '+682', '-21.236736  ', '-159.777671 '),
(52, 'Costa Rica', 'كوستا ريك', 'CRI', '+506', '9.748917  ', '-83.753428'),
(53, 'Croatia (Hrvatska)', 'كرواتيا ( هرافاتسكا )', 'HRV', '+385', '45.1', '15.2  '),
(54, 'Cuba', 'كوبا', 'CUB', '+53', '21.521757', '-77.781167 '),
(55, 'Cyprus', 'قبرص', 'CYP', '+357', '35.126413', '33.429859 '),
(56, 'Czech Republic', 'جمهورية التشيك', 'CZE', '+420', '49.817492', '15.472962 '),
(57, 'Denmark', 'الدنمارك', 'DNK', '+45', '56.26392 ', '9.501785  '),
(58, 'Djibouti', 'جيبوتي', 'DJI', '+253', '11.825138  ', '42.590275'),
(59, 'Dominica', 'دومينيكا', 'DMA', '+1', '15.414999  ', '-61.370976  '),
(60, 'Dominican Republic', 'جمهورية الدومنيكان', 'DOM', '+1', '18.735693', '-70.162651  '),
(61, 'East Timor', 'تيمور الشرقية', 'TMP', '', '', ''),
(62, 'Ecuador', 'الإكوادور', 'ECU', '+593', '-1.831239  ', '-78.183406  '),
(63, 'Egypt', 'مصر', 'EGY', '+20', '26.820553', '30.802498'),
(64, 'El Salvador', 'السلفادور', 'SLV', '+503', '13.794185', '-88.89653 '),
(65, 'Equatorial Guinea', 'غينيا الإستوائية', 'GNQ', '+240', '1.650801', '10.267895'),
(66, 'Eritrea', 'إريتريا', 'ERI', '+291', '15.179384', '39.782334'),
(67, 'Estonia', 'استونيا', 'EST', '+372', '58.595272', '25.013607'),
(68, 'Ethiopia', 'أثيوبيا', 'ETH', '+251', '9.145', '40.489673  '),
(69, 'Falkland Islands (Malvinas)', 'جزر فوكلاند ( مالفيناس )', 'FLK', '+500', '-51.796253  ', '-59.523613  '),
(70, 'Faroe Islands', 'جزر فارو', 'FRO', '+298', '61.892635 ', '-6.911806 '),
(71, 'Fiji', 'فيجي', 'FJI', '+679', '-16.578193 ', '179.414413  '),
(72, 'Finland', 'فنلندا', 'FIN', '+358', '61.92411', '25.748151'),
(73, 'France', 'فرنسا', 'FRA', '+33', '46.227638  ', '2.213749  '),
(74, 'France, Metropolitan', 'فرنسا ، متروبوليتان', '', '', '', ''),
(75, 'French Guiana', 'جيانا الفرنسية', 'GUF', '+594', '3.933889', '-53.125782  '),
(76, 'French Polynesia', 'بولينيزيا الفرنسية', 'PYF', '+689', '-17.679742 ', '-149.406843 '),
(77, 'French Southern Territories', 'الاقاليم الجنوبية الفرنسية', 'ATF', '', '-49.280366  ', '69.348557'),
(78, 'Gabon', 'الغابون', 'GAB', '+241', '-0.803689  ', '11.609444 '),
(79, 'Gambia', 'غامبيا', 'GMB', '+220', '13.443182', '-15.310139  '),
(80, 'Georgia', 'جورجيا', 'GEO', '+995', '42.315407', '43.356892'),
(81, 'Germany', 'ألمانيا', 'DEU', '+49', '51.165691 ', '10.451526'),
(82, 'Ghana', 'غانا', 'GHA', '+233', '7.946527  ', '-1.023194 '),
(83, 'Gibraltar', 'جبل طارق', 'GIB', '+350', '36.137741', '-5.345374  '),
(246, 'Guernsey', 'غيرنسي', '', '', '', ''),
(84, 'Greece', 'اليونان', 'GRC', '+30', '39.074208', '21.824312'),
(85, 'Greenland', 'ارض خضراء', 'GRL', '+299', '71.706936  ', '-42.604303  '),
(86, 'Grenada', 'غرينادا', 'GRD', '+1', '12.262776', '-61.604171  '),
(87, 'Guadeloupe', 'جوادلوب', 'GLP', '+590', '16.995971 ', '-62.067641  '),
(88, 'Guam', 'غوام', 'GUM', '+1', '13.444304', '144.793731  '),
(89, 'Guatemala', 'غواتيمالا', 'GTM', '+502', '15.783471', '-90.230759  '),
(90, 'Guinea', 'غينيا', 'GIN', '+224', '9.945587', '-9.696645 '),
(91, 'Guinea-Bissau', 'غينيا بيساو', 'GNB', '+245', '11.803749', '-15.180413  '),
(92, 'Guyana', 'غيانا', 'GUY', '+592', '4.860416  ', '-58.93018 '),
(93, 'Haiti', 'هايتي', 'HTI', '+509', '18.971187  ', '-72.285215  '),
(94, 'Heard and Mc Donald Islands', 'سمع و ماك دونالد جزر', 'HMD', '', '-53.08181 ', '73.504158'),
(95, 'Honduras', 'هندوراس', 'HND', '+504', '15.199999', '-86.241905 '),
(96, 'Hong Kong', 'هونج كونج', 'HKG', '+852', '22.396428  ', '114.109497'),
(97, 'Hungary', 'هنغاريا', 'HUN', '+36', '47.162494', '19.503304'),
(98, 'Iceland', 'أيسلندا', 'ISL', '+354', '64.963051', '-19.020835  '),
(99, 'India', 'الهند', 'IND', '+91', '20.593684 ', '78.96288'),
(100, 'Indonesia', 'أندونيسيا', 'IDN', '+62', '-0.789275  ', '113.921327  '),
(101, 'Iran (Islamic Republic of)', 'إيران ( الجمهورية الإسلامية)', 'IRN', '+98', '32.427908  ', '53.688046'),
(102, 'Iraq', 'العراق', 'IRQ', '+964', '33.223191', '43.679291'),
(103, 'Ireland', 'أيرلندا', 'IRL', '+353', '53.41291', '-8.24389  '),
(104, 'Israel', 'إسرائيل', 'ISR', '+972', '31.046051  ', '34.851612'),
(105, 'Italy', 'إيطاليا', 'ITA', '+39', '41.87194', '12.56738'),
(106, 'Ivory Coast', 'ساحل العاج', 'CIV', '+225', '7.539989 ', '-5.54708  '),
(245, 'Jersey', 'جيرسي', '', '', '49.214439', '-2.13125 '),
(107, 'Jamaica', 'جامايكا', 'JAM', '+1', '18.109581', '-77.297508 '),
(108, 'Japan', 'اليابان', 'JPN', '+81', '36.204824', '138.252924'),
(109, 'Jordan', ' الأردن', 'JOR', '+962', '30.585164', '36.238414 '),
(110, 'Kazakhstan', 'كازاخستان', 'KAZ', '+7', '48.019573', '66.923684'),
(111, 'Kenya', 'كينيا', 'KEN', '+254', '-0.023559 ', '37.906193'),
(112, 'Kiribati', 'كيريباتي', 'KIR', '+686', '-3.370417 ', '-168.734039 '),
(114, 'Korea', 'جمهورية كوريا', 'KOR', '+82', '35.907757', '127.766922  '),
(115, 'Kosovo', 'كوسوفو', '', '+381', '42.602636  ', '20.902977'),
(116, 'Kuwait', 'الكويت', 'KWT', '+965', '29.31166  ', '47.481766'),
(117, 'Kyrgyzstan', 'قرغيزستان', 'KGZ', '+996', '41.20438', '74.766098  '),
(118, 'Lao People\'s Democratic Republic', 'جمهورية لاو الديمقراطية الشعبية', 'LAO', '+856', '19.85627', '102.495496'),
(119, 'Latvia', 'لاتفيا', 'LVA', '+371', '56.879635', '24.603189'),
(120, 'Lebanon', 'لبنان', 'LBN', '+961', '33.854721', '35.862285'),
(121, 'Lesotho', 'ليسوتو', 'LSO', '+266', '-29.609988 ', '28.233608'),
(122, 'Liberia', 'ليبيريا', 'LBR', '+231', '6.428055', '-9.429499 '),
(123, 'Libyan Arab Jamahiriya', 'الجماهيرية العربية الليبية', 'LBY', '+218', '26.3351', '17.228331'),
(124, 'Liechtenstein', 'ليختنشتاين', 'LIE', '+423', '47.166', '9.555373'),
(125, 'Lithuania', 'ليتوانيا', 'LTU', '+370', '55.169438', '23.881275'),
(126, 'Luxembourg', 'لوكسمبورغ', 'LUX', '+352', '49.815273', '6.129583'),
(127, 'Macau', 'ماكاو', 'MAC', '+853', '22.198745', '113.543873'),
(128, 'Macedonia', 'مقدونيا', 'MKD', '+389', '41.608635', '21.745275  '),
(129, 'Madagascar', 'مدغشقر', 'MDG', '+261', '-18.766947  ', '46.869107 '),
(130, 'Malawi', 'ملاوي', 'MWI', '+265', '-13.254308 ', '34.301525'),
(131, 'Malaysia', 'ماليزيا', 'MYS', '+60', '4.210484', '101.975766'),
(132, 'Maldives', 'جزر المالديف', 'MDV', '+960', '3.202778', '73.22068'),
(133, 'Mali', 'مالي', 'MLI', '+223', '17.570692', '-3.996166  '),
(134, 'Malta', 'مالطا', 'MLT', '+356', '35.937496', '14.375416'),
(135, 'Marshall Islands', 'جزر مارشال', 'MHL', '+692', '7.131474', '171.184478'),
(136, 'Martinique', 'مارتينيك', 'MTQ', '+596', '14.641528 ', '-61.024174  '),
(137, 'Mauritania', 'موريتانيا', 'MRT', '+222', '21.00789', '-10.940835 '),
(138, 'Mauritius', 'موريشيوس', 'MUS', '+230', '-20.348404 ', '57.552152'),
(139, 'Mayotte', 'مايوت', '', '', '', ''),
(140, 'Mexico', 'المكسيك', 'MEX', '+52', '23.634501 ', '-102.552784 '),
(141, 'Micronesia, Federated States of', 'ولايات ميكرونيزيا الموحدة من', 'FSM', '+691', '7.425554 ', '150.550812'),
(142, 'Moldova, Republic of', 'جمهورية مولدوفا', 'MDA', '+373', '47.411631', '28.369885'),
(143, 'Monaco', 'موناكو', 'MCO', '+377', '43.750298 ', '7.412841'),
(144, 'Mongolia', 'منغوليا', 'MNG', '+976', '46.862496', '103.846656'),
(145, 'Montenegro', 'الجبل الأسود', '', '+382', '42.708678  ', '19.37439'),
(146, 'Montserrat', 'مونتسيرات', 'MSR', '+1', '16.742498', '-62.187366  '),
(147, 'Morocco', 'المغرب', 'MAR', '+212', '31.791702', '-7.09262  '),
(148, 'Mozambique', 'موزمبيق', 'MOZ', '+258', '-18.665695 ', '35.529562'),
(149, 'Myanmar', 'ميانمار', 'MMR', '+95', '21.913965', '95.956223'),
(150, 'Namibia', 'ناميبيا', 'NAM', '+264', '-22.95764 ', '18.49041'),
(151, 'Nauru', 'ناورو', 'NRU', '+674', '-0.522778 ', '166.931503  '),
(152, 'Nepal', 'نيبال', 'NPL', '+977', '28.394857', '84.124008'),
(153, 'Netherlands', 'هولندا', 'NLD', '+31', '52.132633', '5.291266'),
(154, 'Netherlands Antilles', 'جزر الأنتيل الهولندية', 'ANT', '+599', '12.226079', '-69.060087  '),
(155, 'New Caledonia', 'كاليدونيا الجديدة', 'NCL', '+687', '-20.904305  ', '165.618042  '),
(156, 'New Zealand', 'نيوزيلندا', 'NZL', '+64', '-40.900557 ', '174.885971  '),
(157, 'Nicaragua', 'نيكاراغوا', 'NIC', '+505', '12.865416', '-85.207229 '),
(158, 'Niger', 'النيجر', 'NER', '+227', '17.607789', '8.081666  '),
(159, 'Nigeria', 'نيجيريا', 'NGA', '+234', '9.081999', '8.675277  '),
(160, 'Niue', 'نيوي', 'NIU', '+683', '-19.054445  ', '-169.867233 '),
(161, 'Norfolk Island', 'جزيرة نورفولك', 'NFK', '+672', '-29.040835 ', '167.954712'),
(162, 'Northern Mariana Islands', 'جزر مريانا الشمالية', 'MNP', '+1', '17.33083', '145.38469'),
(163, 'Norway', 'النرويج', 'NOR', '+47', '60.472024 ', '8.468946'),
(164, 'Oman', 'سلطنة عمان', 'OMN', '+968', '21.512583', '55.923255  '),
(165, 'Pakistan', 'باكستان', 'PAK', '+92', '30.375321', '69.345116'),
(166, 'Palau', 'بالاو', 'PLW', '+680', '7.51498', '134.58252'),
(243, 'Palestine', 'فلسطين', 'PSE', '+970', '31.952162', '35.233154'),
(167, 'Panama', 'بناما', 'PAN', '+507', '8.537981', '-80.782127 '),
(168, 'Papua New Guinea', 'بابوا غينيا الجديدة', 'PNG', '+675', '-6.314993  ', '143.95555'),
(169, 'Paraguay', 'باراغواي', 'PRY', '+595', '-23.442503  ', '-58.443832  '),
(170, 'Peru', 'بيرو', 'PER', '+51', '-9.189967  ', '-75.015152  '),
(171, 'Philippines', 'الفلبين', 'PHL', '+63', '12.879721  ', '121.774017  '),
(172, 'Pitcairn', 'بيتكيرن', 'PCN', '', '-24.703615 ', '-127.439308 '),
(173, 'Poland', 'بولندا', 'POL', '+48', '51.919438  ', '19.145136 '),
(174, 'Portugal', 'البرتغال', 'PRT', '+351', '39.399872 ', '-8.224454 '),
(175, 'Puerto Rico', 'بورتوريكو', 'PRI', '+1', '18.220833 ', '-66.590149  '),
(176, 'Qatar', 'دولة قطر', 'QAT', '+974', '25.354826', '51.183884 '),
(177, 'Reunion', 'جمع شمل', 'REU', '+262', '-21.115141  ', '55.536384'),
(178, 'Romania', 'رومانيا', 'ROM', '+40', '45.943161  ', '24.96676'),
(179, 'Russian Federation', 'الفيدرالية الروسية', 'RUS', '+7', '61.52401  ', '105.318756  '),
(180, 'Rwanda', 'رواندا', 'RWA', '+250', '-1.940278 ', '29.873888'),
(181, 'Saint Kitts and Nevis', 'سانت كيتس ونيفيس', 'KNA', '+1', '17.357822  ', '-62.782998  '),
(182, 'Saint Lucia', 'سانت لوسيا', 'LCA', '+1', '13.909444  ', '-60.978893  '),
(183, 'Saint Vincent and the Grenadines', 'سانت فنسنت وجزر غرينادين', 'VCT', '+1', '12.984305', '-61.287228 '),
(184, 'Samoa', 'ساموا', 'WSM', '+685', '-13.759029  ', '-172.104629 '),
(185, 'San Marino', 'سان مارينو', 'SMR', '+378', '43.94236', '12.457777'),
(186, 'Sao Tome and Principe', 'ساو تومي و برينسيبي', 'STP', '+239', '0.18636', '6.613081 '),
(187, 'Saudi Arabia', 'المملكة العربية السعودية', 'SAU', '+966', '23.885942', '45.079162'),
(188, 'Senegal', 'السنغال', 'SEN', '+221', '14.497401', '-14.452362 '),
(189, 'Serbia', 'صربيا', '', '+381', '44.016521 ', '21.005859'),
(190, 'Seychelles', 'سيشيل', 'SYC', '+248', '-4.679574  ', '55.491977'),
(191, 'Sierra Leone', 'سيرا ليون', 'SLE', '+232', '8.460555', '-11.779889 '),
(192, 'Singapore', 'سنغافورة', 'SGP', '+65', '1.352083', '103.819836  '),
(193, 'Slovakia', 'سلوفاكيا', 'SVK', '+421', '48.669026 ', '19.699024 '),
(194, 'Slovenia', 'سلوفينيا', 'SVN', '+386', '46.151241 ', '14.995463'),
(195, 'Solomon Islands', 'جزر سليمان', 'SLB', '+677', '-9.64571 ', '160.156194'),
(196, 'Somalia', 'الصومال', 'SOM', '+252', '5.152149', '46.199616'),
(197, 'South Africa', 'جنوب أفريقيا', 'ZAF', '+27', '-30.559482 ', '22.937506 '),
(198, 'South Georgia South Sandwich Islands', 'جزر ساندويتش الجنوبية جورجيا الجنوبية', 'SGS', '', '-54.429579 ', '-36.587909  '),
(199, 'Spain', 'إسبانيا', 'ESP', '+34', '40.463667', '-3.74922  '),
(200, 'Sri Lanka', 'سيريلانكا', 'LKA', '+94', '7.873054', '80.771797  '),
(201, 'St. Helena', 'سانت هيلانة', 'SHN', '+290', '-24.143474 ', '-10.030696  '),
(202, 'St. Pierre and Miquelon', 'سانت بيير و ميكلون', 'SPM', '+508', '46.941936', '-56.27111 '),
(203, 'Sudan', 'سودان', 'SDN', '+249', '12.862807', '30.217636'),
(204, 'Suriname', 'سورينام', 'SUR', '+597', '3.919305', '-56.027783 '),
(205, 'Svalbard and Jan Mayen Islands', 'جزر سفالبارد وجان ماين', 'SJM', '', '77.553604 ', '23.670272'),
(206, 'Swaziland', 'سوازيلاند', 'SWZ', '+268', '-26.522503  ', '31.465866 '),
(207, 'Sweden', 'السويد', 'SWE', '+46', '60.128161', '18.643501'),
(208, 'Switzerland', 'سويسرا', 'CHE', '+41', '46.818188 ', '8.227512'),
(209, 'Syrian Arab Republic', 'الجمهورية العربية السورية', 'SYR', '+963', '34.802075', '38.996815'),
(210, 'Taiwan', 'تايوان', 'TWN', '+886', '23.69781  ', '120.960515'),
(211, 'Tajikistan', 'طاجيكستان', 'TJK', '+992', '38.861034', '71.276093'),
(212, 'Tanzania, United Republic of', 'جمهورية تنزانيا المتحدة', 'TZA', '+255', '-6.369028  ', '34.888822 '),
(213, 'Thailand', 'تايلاند', 'THA', '+66', '15.870032', '100.992541'),
(214, 'Togo', 'توغو', 'TGO', '+228', '8.619543  ', '0.824782'),
(215, 'Tokelau', 'توكيلاو', 'TKL', '+690', '-8.967363 ', '-171.855881 '),
(216, 'Tonga', 'تونغا', 'TON', '+676', '-21.178986  ', '-175.198242 '),
(217, 'Trinidad and Tobago', 'ترينداد وتوباغو', 'TTO', '+1', '10.691803 ', '-61.222503  '),
(218, 'Tunisia', 'تونس', 'TUN', '+216', '33.886917  ', '9.537499  '),
(219, 'Turkey', 'تركيا', 'TUR', '+90', '38.963745', '35.243322'),
(220, 'Turkmenistan', 'تركمانستان', 'TKM', '+993', '38.969719', '59.556278  '),
(221, 'Turks and Caicos Islands', 'جزر تركس وكايكوس', 'TCA', '+1', '21.694025', '-71.797928 '),
(222, 'Tuvalu', 'توفالو', 'TUV', '+688', '-7.109535 ', '177.64933'),
(223, 'Uganda', 'أوغندا', 'UGA', '+256', '1.373333', '32.290275'),
(224, 'Ukraine', 'أوكرانيا', 'UKR', '+380', '48.379433  ', '31.16558'),
(225, 'United Arab Emirates', 'الإمارات العربية المتحدة', 'ARE', '+971', '23.424076', ' 53.847818'),
(226, 'United Kingdom', 'المملكة المتحدة', 'GBR', '+44', '55.378051 ', '-3.435973 '),
(227, 'United States minor outlying islands', 'الولايات المتحدة الجزر الصغيرة النائية', 'UMI', '', '', ''),
(228, 'Uruguay', 'أوروغواي', 'URY', '+598', '-32.522779 ', '-55.765835  '),
(229, 'Uzbekistan', 'أوزبكستان', 'UZB', '+998', '41.377491', '64.585262'),
(230, 'Vanuatu', 'فانواتو', 'VUT', '+678', '-15.376706  ', '166.959158'),
(231, 'Vatican City State', 'دولة مدينة الفاتيكان', 'VAT', '+39', '41.902916  ', '12.453389'),
(232, 'Venezuela', 'فنزويلا', 'VEN', '+58', '6.42375', '-66.58973 '),
(233, 'Vietnam', 'فيتنام', 'VNM', '+84', '14.058324', '108.277199 '),
(234, 'Virgin Islands (British)', 'جزر العذراء (البريطانية )', 'VGB', '+1', '18.420695', '-64.639968  '),
(235, 'Virgin Islands (U.S.)', 'جزر العذراء ( الولايات المتحدة )', 'VIR', '+1', '18.335765', '-64.896335  '),
(236, 'Wallis and Futuna Islands', 'واليس و فوتونا', 'WLF', '+681', '-13.768752 ', '-177.156097 '),
(237, 'Western Sahara', 'الصحراء الغربية', 'ESH', '', '24.215527  ', '-12.885834  '),
(238, 'Yemen', 'اليمن', 'YEM', '+967', '15.552727 ', '48.516388 '),
(239, 'Yugoslavia', 'يوغوسلافيا', 'YUG', '', '', ''),
(240, 'Zaire', 'زائير', '', '', '', ''),
(241, 'Zambia', 'زامبيا', 'ZMB', '+260', '-13.133897  ', '27.849332'),
(242, 'Zimbabwe', 'زيمبابوي', 'ZWE', '+263', '-19.015438  ', '29.154857');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `district_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `eng_name` varchar(255) NOT NULL,
  `arb_name` varchar(255) NOT NULL,
  `delivery_charges` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`district_id`, `city_id`, `eng_name`, `arb_name`, `delivery_charges`) VALUES
(1, 1, 'Jeddah District 1', 'Jeddah District 1', '15'),
(2, 1, 'Jeddah District 2', 'Jeddah District 2', '15'),
(3, 2, 'Madina District 1', 'Madina District 1', '15'),
(4, 2, 'Madina District 2', 'Madina District 2', '15'),
(5, 3, 'Makkah', 'مكة', '15'),
(6, 1, 'basteen', 'البساتين ', '15');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `email_template_id` bigint(20) UNSIGNED NOT NULL,
  `title_en` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_en` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description_en` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_ar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`email_template_id`, `title_en`, `subject_en`, `description_en`, `title_ar`, `subject_ar`, `description_ar`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Email Verification On Signup', 'Verify Your Email', '<p>Dear {{user_name}},</p>\r\n<p>Please verify your email address by clicking the link below.</p>\r\n<p>{{link}}</p>', '', '', '', 1, '2020-03-18 04:23:25', '2020-04-28 03:31:08'),
(2, 'Forgot Password For App', 'Forgot Password', '<p>Dear {{user_name}},</p>\r\n<p>Your new password for the app is {{code}}</p>\r\n<p>&nbsp;</p>', '', '', '', 1, '2020-04-28 05:20:45', '2020-04-28 05:20:45'),
(3, 'First Email Template', 'First Email Template Subject', '<p>First Email Template Description upate</p>', 'First Email Template', 'First Email Subject Ar', '<p>First Email Description&nbsp; updAr</p>', 1, '2020-09-04 00:41:25', '2020-09-04 00:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `faq_id` int(11) NOT NULL,
  `question_en` varchar(5000) NOT NULL,
  `question_ar` varchar(5000) NOT NULL,
  `answer_en` varchar(5000) NOT NULL,
  `answer_ar` varchar(5000) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `home_images`
--

CREATE TABLE `home_images` (
  `homeimage_id` int(10) UNSIGNED NOT NULL,
  `title_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` varchar(500) NOT NULL,
  `is_new_tab` tinyint(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_images`
--

INSERT INTO `home_images` (`homeimage_id`, `title_en`, `title_ar`, `image`, `url`, `is_new_tab`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Awards Ceremony ', 'حفلات تكريم الشركات ', 'uploads/home_images/67697656497201805060926331.jpg', 'https://www.msj.com.sa/page/awards', 1, '2018-03-30 09:49:03', '2020-08-18 12:30:34', 1),
(3, 'Become a Reseller', 'كن موزعاً لمنتجاتنا', 'uploads/home_images/76441663903201805061036084.jpg', 'https://www.msj.com.sa/page/become_a_seller', 1, '2018-03-30 09:17:11', '2020-08-18 12:31:23', 1),
(4, 'Special  Events', 'فعاليات خاصة', 'uploads/home_images/76118226781201805061053091.jpg', ' ', 0, '2018-03-30 09:17:46', '2018-10-11 06:37:23', 1),
(5, 'Modern Technology', 'تقنيات حديثة', 'uploads/home_images/57239200492201805061059013.jpg', '', 0, '2018-03-30 09:18:26', '2018-10-11 06:37:44', 1),
(6, 'Outstanding Projects', 'المشاريع الحاليه', 'uploads/home_images/53730176312201805060947362.jpg', ' ', 0, '2018-03-30 09:18:50', '2018-10-11 06:38:24', 1),
(7, 'Download Center', 'لتحميل المواصفات والنشرات', 'uploads/home_images/2487395239620180712062919dd.jpg', ' https://www.msj.com.sa/page/download_center', 0, '2018-03-30 09:45:42', '2018-10-11 06:39:11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `home_video`
--

CREATE TABLE `home_video` (
  `id` int(11) NOT NULL,
  `video_url` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_video`
--

INSERT INTO `home_video` (`id`, `video_url`) VALUES
(1, 'https://www.youtube.com/watch?v=Y-lBvI6u_hw');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `newsletter_id` int(11) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `subscribed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_track_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `status` enum('Received','Dispatched','Delivered','Cancelled By Customer','Cancelled By Admin') NOT NULL DEFAULT 'Received',
  `transaction_id` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `delivery_charges` varchar(255) DEFAULT NULL,
  `vat_total` varchar(255) NOT NULL,
  `total_items` int(11) NOT NULL,
  `otp` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `order_item_it` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL COMMENT 'for now it is only for download center page',
  `page_title_en` varchar(255) NOT NULL,
  `page_title_ar` varchar(255) NOT NULL,
  `page_description_en` varchar(5000) NOT NULL,
  `page_description_ar` varchar(5000) NOT NULL,
  `location` varchar(255) NOT NULL,
  `address1_en` varchar(1000) NOT NULL,
  `address2_en` varchar(1000) NOT NULL,
  `address3_en` varchar(1000) NOT NULL,
  `address1_ar` varchar(1000) NOT NULL,
  `address2_ar` varchar(1000) NOT NULL,
  `address3_ar` varchar(1000) NOT NULL,
  `address4_en` text NOT NULL,
  `address4_ar` text NOT NULL,
  `address5_en` text NOT NULL,
  `address5_ar` text NOT NULL,
  `address6_en` text NOT NULL,
  `address6_ar` text NOT NULL,
  `lat` varchar(225) NOT NULL,
  `lng` varchar(225) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `parent_id`, `page_title_en`, `page_title_ar`, `page_description_en`, `page_description_ar`, `location`, `address1_en`, `address2_en`, `address3_en`, `address1_ar`, `address2_ar`, `address3_ar`, `address4_en`, `address4_ar`, `address5_en`, `address5_ar`, `address6_en`, `address6_ar`, `lat`, `lng`, `image`, `created_at`, `updated_at`) VALUES
(1, 0, 'Terms and Condition', 'Terms and Condition', '<p><b>Terms and Condition</b><br></p>', '<span style=\"font-weight: 600;\">Terms and Condition</span>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2020-09-02 09:19:44'),
(2, 0, 'Commitment', 'إلتزاماتنا', '<p>We strongly believe in providing our clients with innovative, outstanding quality products that are competitively priced and always available to the local security market. One of our most important aims is to do whatever it takes to satisfy every client by providing fast responds on pricing inquires and other technical requests about the related supports and services. We will continue introducing the most recent up- to-date technology and integration services that are always available to fulfil our client’s requirements.</p>', '<p>نلتزم لعملائنا بتقديم أحدث المنتجات ذات الجودة العالية والاسعارالتنافسية في سوق منتجات الأنظمة الأمنية. حيث أن من اهم أهدافنا هو إرضاء عملائنا من خلال تقديم الخدمات السريعة في الاستعلام عن المنتجات&nbsp;والأسعار و الاستفسارات الفنية مع أقصى درجات الدعم والخدمة،  وتوفير احدث  التقنيات المتاحة لتلبية جميع متطلبات عملائنا.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/1335252165120180228082611shutterstock_728200222@1x.jpg', '2018-02-28 00:00:00', '2018-02-28 20:48:25'),
(3, 0, 'Mission', 'مهمة', '<p>Our mission is to continue as the leader in rapidly advanced security industry by providing most reliable products and services (quality products, strong management, and great services with an extensive historical experience in the business throughout the past years). It is our mission also to introduce a professional and long-term business relationship with the clients and to continue growing and having the capability of adapting to the changing environment with providing new solutions and concepts.</p>', '<p>أن نكون الرواد في توفير خدمات وحلول الأنظمة الأمنية ، وذلك بتقديم منتجات ذات جودة عالية،و خدمات متميزة تتحقق بفعل خبرتنا الواسعة  في هذا المجال لسنوات عديدة . و إنشاء علاقة مهنية بعيدة المدى مع عملائنا. والاستمرار في النمو وتحقيق القدرة على التكيف مع الظروف المتغيرة بتقديم مفاهيم وحلول جديدة.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/2130435174220180228080938mission.jpg', '2018-02-28 00:00:00', '2018-03-14 09:10:46'),
(4, 0, 'Partners', 'شركاء', 'The Brands that we are representing&nbsp;', '<p>العلامات التجارية التي نمثلها&nbsp;</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-10-11 06:21:07'),
(5, 0, 'Awards & Recognition', 'الجوائز والتقدير', '<p>MSJ P<span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\">resents</span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\"> </span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\">partner excellence </span><span style=\"font-weight: bold; color: rgb(106, 106, 106); font-family: arial, sans-serif; font-size: small;\">awards</span><span style=\"color: rgb(84, 84, 84); font-family: arial, sans-serif; font-size: small;\"> 2017</span></p>', '<p>نحن نسعى جاهدين لتوفير أفضل نوعية للعمل. نحن نعمة من الجوائز التالية.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/2593469047120180227102102328552180124035935back-to-school.jpg', '2018-02-28 00:00:00', '2018-07-12 22:26:23'),
(6, 0, 'Our Team', 'فريقنا', '<p>Through empowerment of the employees, we are able to build a durable base of talented individuals forming the foundation of our future. Their determination and dedication continues to contribute to the overall harmony of the MSJ family. We are glad and proud of speaking highly about our team. The team is highly educated and well trained due to the historical experience of our team with the capability to manage and handle all kinds of projects.</p>', '<p>إننا نشعر بالسعادة والفخر ونحن نمتدح فريقنا، إذ يمتاز هذا الفريق بدرجات علمية عالية، كما أنه مدرب تدريبا جيداً من قبل شركائنا المنتجين<font face=\"Tahoma\">، وكذلك بفعل التجربة الزمنية بقدرته على التعامل مع جميع المشار</font>يع، كما لدينا المعرفة والكفاءة التقنية لحماية جميع ممتلكاتكم.والخبرةللقيام بترجمة ناجحة للمفاهيم الأمنية.</p>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-08-07 12:50:37'),
(7, 0, 'Careers', 'وظائف', 'You can be one of our successful members by applying today, you will need just to send your CV, our HR will be more than happy to contact you if your qualification is matching our needs.', 'بإمكانك أن تكون أحد أعضاء فريق النجاح معنا كل ما عليك هو إرسال سيرتك الذاتية وسيكون فريق الموارد البشرية سعيد بالتواصل معك إذا كانت مؤهلاتك تتوافق مع متطلبات العمل معنا.', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/908304020820180227110707profile.png', '2018-02-28 00:00:00', '2018-10-11 06:30:05'),
(8, 0, 'Become A Reseller', 'كن موزع', '<h4>Reseller Program:</h4>\r\n            <p>Qualified system integrators and reseller can apply for our dedicated partner\'s program membership with following details:</p>\r\n\r\n            <h5>Key Benefits:</h5>\r\n            <ul>\r\n            <li>Support in System design & identifying proper solutions</li>\r\n            <li>Exclusive pricing support for large scale projects</li>\r\n            <li>Dedicated technical support & online remote assistance</li>\r\n            <li>Free periodical training programs  on latest products</li>\r\n            <li>On-demand training program for project based solutions</li>\r\n            <li>Large stock for immediate delivery on order</li>\r\n            <li>Spare parts stock for quick after sales support</li>\r\n            </ul>\r\n\r\n            <h5>Who can apply?</h5>\r\n            <ul>\r\n            <li>Security system integrators & resellers</li>\r\n            <li>Low current systems</li>\r\n            <li>Computer and network firms</li>\r\n            <li>Electro mechanical contractors & Main contractors in the projects</li>\r\n\r\n            </ul>\r\n\r\n\r\n            <h5>Requirements for registration:</h5>\r\n\r\n            <ul>\r\n            <li>Duly updated company profile</li>\r\n            <li>Full contact information with postal address</li>\r\n            <li>A copy of valid Commercial registration </li>\r\n            </ul>', '<h4>كن موزع:</h4><p>بإمكان جميع الشركات والمؤسسات التي تعمل في مجال التقنية التقدم بطلب للحصول على عضوية برنامج الشريك المتفاني لدينا مع للتمتع بالمميزات التالية:</p><h5><b>الفوائد الرئيسية:</b></h5><ul><li>الدعم في تصميم الأنظمة الأمنية وتحديد الحلول المناسبة.</li><li>الحصول على أسعار حصرية للمشاريع.</li><li>دعم فني خاص والمساعدة عن بعد.</li><li>برامج تدريب دورية مجانية على أحدث المنتجات.</li><li>برنامج تدريبي حسب الطلب للحلول القائمة على المشاريع.</li><li>مخزون كبير من المواد للتسليم الفوري.</li><li>مخزون قطع الغيار للدعم السريع لخدمات الصيانة وبعد البيع.</li></ul><h5><b>من يمكنه التقدم بطلب التوزيع؟</b></h5><ul><li>شركات ومؤسسات الأنظمة المتكاملة.</li><li>شركات ومؤسسات حلول التيار الخفيف.</li><li>شركات ومؤسسات تقنية المعلومات.</li><li>مقاولين الكهروميكانيك.</li><li>المقاولين الرئيسيين في المشاريع.</li></ul><h5><b>متطلبات التسجيل:</b></h5><ul><li>خطاب طلب التعامل.</li><li>معلومات الاتصال الكاملة مع العنوان البريدي.</li><li>نسخة من السجل التجاري ساري المفعول.</li></ul>', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/4568795490120180228125513dummy.pdf', '2018-02-28 00:00:00', '2018-10-11 05:52:38'),
(9, 0, 'Download Center', 'مركز التحميل', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/Profile.pdf', '2018-03-06 00:00:00', '2018-03-15 07:24:05'),
(10, 0, 'Our Suppliers', 'موردينا', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2020-04-18 21:42:13'),
(11, 0, 'Contact Us', 'Contact Us', '<p>MSJ is covering most of the major cities in Saudi Arabia by a well-organized branch with full facilities with qualified Sales and Technical teams.</p>', '<p>تغطي شركة MSJ جميع المدن الرئيسية في المملكة العربية السعودية ومملكة البحرين بسلسلة من الفروع المجهزة بجميع المرافق وفريق مؤهل ومدرب لتقديم الدعم الفني والتسويق.</p>', '6749 Khair Addin Az Zarkali, Al Ghadir, Riyadh 13311 3029, Saudi Arabia', '<p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><b>Head Office - Riyadh</b></p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Rabie District - Rabie Plaza</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">First Floor Office 1 & 2</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">P.O. Box 53492, Riyadh 11583</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Email: info@msj.com.sa</p>', '<p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><b>Riyadh Branch Office </b></p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Makkah Road - Olaya</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Ph.: 9200 00 675</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Fax: 011 462 3359</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\">Email: info@msj.com.sa</p><p style=\"margin:0in;margin-bottom:.0001pt;line-height:18.0pt\"><br></p>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span style=\"font-weight: 600; font-family: \" hind=\"\" madurai\",=\"\" sans-serif;\"=\"\">Jeddah Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">MadinaRoad,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 012 664 1799<br></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<p class=\"MsoNormal\" align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;\r\nmargin-bottom:0in;margin-left:0in;margin-bottom:.0001pt;text-align:right;\r\nline-height:normal;mso-outline-level:5\"><br></p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5><h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\">\r\n\r\n</h5>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5><h5 style=\"text-align: right; \">\r\n\r\n<p align=\"right\" style=\"margin: 0in 0in 0.0001pt;\"><font face=\"Arial\"></font></p></h5>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span style=\"font-weight: 600; font-family: \" hind=\"\" madurai\",=\"\" sans-serif;\"=\"\">Dammam Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">King Fahad Road,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 013 867 8085<br></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5>', '<p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\"><span hind=\"\" madurai\",=\"\" sans-serif;\"=\"\" style=\"font-weight: 600;\">Abha Branch Office </span></p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">King Fahad Road,</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Ph.: 9200 00 675</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Fax: 011 462 3359</p><p style=\"margin: 0in 0in 0.0001pt; line-height: 18pt;\">Email: info@msj.com.sa</p>', '<h5 align=\"right\" style=\"margin-top:7.5pt;margin-right:0in;margin-bottom:0in;\r\nmargin-left:0in;margin-bottom:.0001pt;text-align:right\"><br></h5>', '<p class=\"MsoNormal\"><font face=\"Hind Madurai, sans-serif\"><span style=\"font-size: 16px;\"><b> </b></span></font></p>', '<h5> </h5><p> </p>', '24.78962213606099', '46.648573858169584', '', '0000-00-00 00:00:00', '2019-09-04 12:19:58'),
(12, 9, 'Catelog', 'Catelog', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/catelog.pdf', '0000-00-00 00:00:00', '2018-03-15 07:25:00'),
(13, 9, 'Catelog', 'Catelog', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'uploads/page_images/catelog.pdf', '2018-03-15 00:00:00', '2018-03-15 07:25:00'),
(14, 0, 'Outstanding Projects ', 'Outstanding Projects ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2018-05-06 12:45:31'),
(15, 0, 'Events', 'Events', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2018-03-06 00:00:00', '2018-05-06 12:45:31'),
(16, 14, 'Speed gates & Turnstile gates', 'Speed gates & Turnstile gates', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:27:08'),
(17, 14, 'IP CCTV Systems ', 'IP CCTV Systems ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:29:29'),
(18, 15, 'Event 1', 'Event 1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-09 20:25:24'),
(19, 15, 'Event 2', 'Event 2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-05-27 03:19:49'),
(20, 14, 'Access Control Systems', 'Access Control Systems', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2018-06-10 08:56:42'),
(21, 0, 'Products', 'منتجات', 'Products', 'Products', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2020-04-20 07:06:15');

-- --------------------------------------------------------

--
-- Table structure for table `page_images`
--

CREATE TABLE `page_images` (
  `page_image_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_images`
--

INSERT INTO `page_images` (`page_image_id`, `page_id`, `image`) VALUES
(53, 18, 'uploads/page_images/6730090529720180527031219download.png'),
(54, 18, 'uploads/page_images/8286718198920180527031219image.png'),
(55, 19, 'uploads/page_images/8079790129420180527034919Pizza_01.png'),
(56, 19, 'uploads/page_images/6214825097820180527034919Screenshot_1.png'),
(59, 16, 'uploads/page_images/8891921899220180609080827IMG_20170807_103020.jpg'),
(60, 17, 'uploads/page_images/77199759844201806090829297.jpg'),
(61, 20, 'uploads/page_images/88363901385201806100842562018-06-05_(1).png'),
(62, 20, 'uploads/page_images/10798761413201806100842562018-06-05_(2).png'),
(63, 20, 'uploads/page_images/91647386920201806100842562018-06-05_(3).png'),
(64, 5, 'uploads/page_images/95588201958201807120649571.jpg'),
(65, 5, 'uploads/page_images/70399147670201807120744132.jpg'),
(66, 5, 'uploads/page_images/75464876260201807120753163.jpg'),
(67, 5, 'uploads/page_images/42831793602201807120752244.jpg'),
(68, 5, 'uploads/page_images/21142714159201807120739255.jpg'),
(69, 5, 'uploads/page_images/47957980062201807120739266.jpg'),
(70, 5, 'uploads/page_images/81788542592201807120745297.jpg'),
(71, 5, 'uploads/page_images/33005961955201807120706347.jpg'),
(72, 5, 'uploads/page_images/18059888759201807120703378.jpg'),
(73, 5, 'uploads/page_images/611903711372018071210232610.jpg'),
(74, 5, 'uploads/page_images/153227556412018071210232611.jpg'),
(75, 5, 'uploads/page_images/56300985452018071210232612.jpg'),
(76, 5, 'uploads/page_images/808667895882018071210232613.jpg'),
(77, 5, 'uploads/page_images/686300565652018071210232614.jpg'),
(78, 5, 'uploads/page_images/746746403802018071210232615.jpg'),
(79, 5, 'uploads/page_images/457853085082018071210232616.jpg'),
(80, 5, 'uploads/page_images/301605399212018071210232617.jpg'),
(81, 5, 'uploads/page_images/815022403702018071210232619.jpg'),
(82, 5, 'uploads/page_images/2754037457420180712102326122.jpg'),
(153, 4, 'uploads/page_images/41409135833201807300835151.jpg'),
(154, 4, 'uploads/page_images/49552874189201807300835152.jpg'),
(155, 4, 'uploads/page_images/27189230795201807300835153.jpg'),
(156, 4, 'uploads/page_images/61338403682201807300835154.jpg'),
(157, 4, 'uploads/page_images/61298952070201807300818165.jpg'),
(158, 4, 'uploads/page_images/60147428259201807300818166.jpg'),
(159, 4, 'uploads/page_images/53039309002201807300818167.jpg'),
(160, 4, 'uploads/page_images/58452315824201807300818168.jpg'),
(161, 4, 'uploads/page_images/518716426722018073008141700.jpg'),
(162, 4, 'uploads/page_images/54428881213201807300814179.jpg'),
(163, 4, 'uploads/page_images/202148430192018073008141710.jpg'),
(164, 4, 'uploads/page_images/641237033542018073008141711.jpg'),
(165, 4, 'uploads/page_images/895673215572018073008481712.jpg'),
(166, 4, 'uploads/page_images/705432484042018073008481713.jpg'),
(167, 4, 'uploads/page_images/905518663242018073008481714.jpg'),
(168, 4, 'uploads/page_images/990104720982018073008481715.jpg'),
(169, 4, 'uploads/page_images/562589879112018073008121816.jpg'),
(170, 4, 'uploads/page_images/673332707092018073008121817.jpg'),
(171, 4, 'uploads/page_images/983007348442018073008121818.jpg'),
(172, 4, 'uploads/page_images/161522980792018073008121819.jpg'),
(173, 10, 'uploads/page_images/4453173913520190130075407sdd.png'),
(174, 10, 'uploads/page_images/4446440899020200418091342Untitled-1_0000_download-1.jpg'),
(175, 10, 'uploads/page_images/6275549945320200418091342Untitled-1_0001_untitled-1_182.jpg'),
(176, 10, 'uploads/page_images/2806930236720200418091342Untitled-1_0002_Nesto-Hypermarket-Dubai.jpg'),
(177, 10, 'uploads/page_images/5979081538220200418091342Untitled-1_0003_images.jpg'),
(178, 10, 'uploads/page_images/2597091179120200418091342Untitled-1_0004_HyperPanda-logo-1150x1150.jpg'),
(179, 10, 'uploads/page_images/2147615736420200418091342Untitled-1_0005_download.jpg'),
(180, 10, 'uploads/page_images/8737318120420200418091342Untitled-1_0006_download-3.jpg'),
(181, 10, 'uploads/page_images/7673360025320200418091342Untitled-1_0007_download-2.jpg'),
(182, 10, 'uploads/page_images/2653861519620200418091342Untitled-1_0008_Background.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_title_en` varchar(255) NOT NULL,
  `product_title_ar` varchar(255) NOT NULL,
  `product_description_en` varchar(2000) NOT NULL,
  `product_description_ar` varchar(2000) NOT NULL,
  `product_colors` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `model_en` varchar(255) NOT NULL,
  `model_ar` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_featured` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title_en`, `product_title_ar`, `product_description_en`, `product_description_ar`, `product_colors`, `brand_id`, `category_id`, `sub_category_id`, `model_en`, `model_ar`, `price`, `is_active`, `is_featured`, `created_at`, `updated_at`) VALUES
(1, 'Fresh Apples', 'تفاح', 'Description of the product will be here', 'Description in arabic here', '', 10, 17, 69, '1 Kilogram', '1 كيلو', '22.00', 1, 1, '2020-08-18 12:45:38', '2020-08-18 12:45:38'),
(2, 'Ketchup', 'كاتشب', 'jryrty', 'غغغ', '', 10, 17, 69, '150', '150 مل', '15.00', 1, 1, '2020-08-18 13:57:53', '2020-08-18 13:57:53');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_id`, `product_image`) VALUES
(1, 1, 'uploads/product_images/4183738766620200818123845test_apple_image.jpg'),
(2, 2, 'uploads/product_images/37687900839202008180143502.jpg'),
(4, 2, 'uploads/product_images/3853975443202008180153573.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_specifications`
--

CREATE TABLE `product_specifications` (
  `product_specification_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `specification_title_en` varchar(255) NOT NULL,
  `specification_title_ar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_title`) VALUES
(1, 'Admin'),
(2, 'Support'),
(3, 'Client'),
(4, 'Warehouse'),
(5, 'Delivery'),
(6, 'Career Support');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `id` int(11) NOT NULL,
  `ios_app_version` decimal(10,1) NOT NULL,
  `ios_team_app_version` decimal(10,1) NOT NULL,
  `android_app_version` int(11) NOT NULL,
  `android_team_app_version` int(11) NOT NULL,
  `vat` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `ios_app_version`, `ios_team_app_version`, `android_app_version`, `android_team_app_version`, `vat`) VALUES
(1, '1.5', '1.0', 9, 1, 15);

-- --------------------------------------------------------

--
-- Table structure for table `support_tickets`
--

CREATE TABLE `support_tickets` (
  `id` int(11) NOT NULL,
  `ticket_id` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('open','closed') NOT NULL DEFAULT 'open',
  `ticket_type` varchar(120) NOT NULL,
  `full_name` varchar(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `city` varchar(120) NOT NULL,
  `phone` varchar(120) NOT NULL,
  `type_id` varchar(120) NOT NULL COMMENT 'complaint table primary key',
  `product` varchar(500) NOT NULL,
  `apartment_no` varchar(120) NOT NULL,
  `visit_time` varchar(120) NOT NULL,
  `location` varchar(400) NOT NULL,
  `message` text NOT NULL,
  `lat` varchar(120) NOT NULL,
  `lng` varchar(120) NOT NULL,
  `timestamp` varchar(120) NOT NULL COMMENT 'created at timestamp',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed_request` int(11) NOT NULL COMMENT '0 for nothing 1 for admin want to close 2 close 3 user request to re open',
  `ticket_taken_by` int(11) NOT NULL COMMENT 'This is the user id of the support user who has taken the ticket and has made 1st reply. now only this user will be able to see this ticket and admin',
  `last_closed_by` int(11) NOT NULL COMMENT 'This is the user ID who has closed this ticket lastly'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `support_users`
--

CREATE TABLE `support_users` (
  `id` int(11) NOT NULL,
  `complaint_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_orders`
--

CREATE TABLE `temp_orders` (
  `temp_order_id` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL COMMENT 'this is actually aa cookie value not user id logic change',
  `product_id` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temp_orders`
--

INSERT INTO `temp_orders` (`temp_order_id`, `user_id`, `product_id`, `product_quantity`) VALUES
(1, '31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimonial_id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `position_en` varchar(255) NOT NULL,
  `position_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL,
  `description_en` varchar(255) NOT NULL,
  `description_ar` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket_comments`
--

CREATE TABLE `ticket_comments` (
  `comment_id` int(11) NOT NULL,
  `ticket_id` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(120) NOT NULL COMMENT 'created at timestamp',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) DEFAULT NULL COMMENT 'it is warehouse user id for delivery users',
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '3',
  `phone` varchar(255) NOT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `city_id` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `verification_code` varchar(255) NOT NULL,
  `is_verified` tinyint(4) NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '1',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `address` varchar(500) DEFAULT NULL,
  `latitude` varchar(25) DEFAULT NULL,
  `longitude` varchar(25) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `iqama_number` varchar(255) DEFAULT NULL,
  `iqama_expiry` varchar(255) DEFAULT NULL,
  `driving_license_no` varchar(255) DEFAULT NULL,
  `driving_license_expiry` varchar(255) DEFAULT NULL,
  `vehicle_make` varchar(255) DEFAULT NULL,
  `vehicle_model` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `emergency_contact` varchar(255) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL,
  `driving_license_img` varchar(255) NOT NULL,
  `registration_license_img` varchar(255) NOT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `chat_request_id` int(11) NOT NULL,
  `receive_career_email_at` varchar(255) NOT NULL DEFAULT 'NULL' COMMENT 'This field is being used only for career support user type with role id 6 to specify on which email address to send career request emails'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `branch_id`, `full_name`, `username`, `email`, `role_id`, `phone`, `gender`, `city_id`, `device_type`, `device_token`, `country`, `password`, `verification_code`, `is_verified`, `is_approved`, `status`, `address`, `latitude`, `longitude`, `dob`, `iqama_number`, `iqama_expiry`, `driving_license_no`, `driving_license_expiry`, `vehicle_make`, `vehicle_model`, `registration_no`, `emergency_contact`, `profile_pic`, `driving_license_img`, `registration_license_img`, `iban`, `bank_name`, `created_at`, `updated_at`, `chat_request_id`, `receive_career_email_at`) VALUES
(1, NULL, 'Admin', 'Admin', 'backend@schopfen.com', 1, '', 'male', '', 'ios', '', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2018-02-22 00:00:00', '2020-04-14 05:15:59', 0, ''),
(92, NULL, 'Warehouse', 'Warehouse', 'warehouse@dkakeen.com', 1, '', 'male', '', 'ios', '', '', '5994c13b3d106964b88981d4f04742a1', '', 1, 0, 'active', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2018-02-22 00:00:00', '2020-04-14 05:15:59', 0, ''),
(94, NULL, 'ماجد', '', 'm.alrumaim@gmail.com', 3, '966503920211', 'male', '1', 'ios', 'dZsUaCTG6k0etTooR78itx:APA91bF_oiAbcHacMYOxEyC1_PJ8TixQa_YNCe1dB8-6Ek-4bugB2U2GpemTLBHPpjma36Eacy7kX5e9I6qUpTD5NPeCrajgIzwU2fc8hgKtDmcG0aJ50Oa3M1U23PXX-VY5z6KgosQb', '', '25f9e794323b453885f5181f1b624d0b', '', 1, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-08-21 14:39:27', '0000-00-00 00:00:00', -1, 'NULL'),
(95, NULL, 'djxjd', 'fjfjd', 'm@m.com', 3, '966564976417', 'male', '1', 'android', 'fe0n5LEvN9s:APA91bHGnkQriyRqGAdIoN0csFwn49Qi_2im1Wg-FOHK31gEbeo0gRSsFO_mn5-D94O3rBxxZ_O6KeqnUGQ5OSyXJXX56dQFdBAsI16ab_-xx-9jvSMygekA9fRKswNVi6yPSciEjkbS', '', '25d55ad283aa400af464c76d713c07ad', '', 1, 0, 'active', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, '2020-09-02 13:01:54', '0000-00-00 00:00:00', -1, 'NULL');

-- --------------------------------------------------------

--
-- Table structure for table `user_districts`
--

CREATE TABLE `user_districts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `user_type` enum('warehouse','delivery') NOT NULL DEFAULT 'warehouse'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visit_complaint_types`
--

CREATE TABLE `visit_complaint_types` (
  `visit_complaint_type_id` int(11) NOT NULL,
  `visit_complaint_title_en` varchar(255) NOT NULL,
  `visit_complaint_title_ar` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visit_complaint_types`
--

INSERT INTO `visit_complaint_types` (`visit_complaint_type_id`, `visit_complaint_title_en`, `visit_complaint_title_ar`, `created_at`, `updated_at`, `is_active`) VALUES
(7, 'Maintenance ', 'Maintenance ', '2018-11-01 09:46:12', '2018-11-01 09:46:12', 1),
(8, 'Technical', 'Technical', '2018-11-01 09:46:33', '2018-11-01 09:47:59', 1),
(9, 'Sales and Marketing', 'Sales and Marketing', '2018-11-01 09:47:21', '2018-11-01 09:47:21', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`ad_id`);

--
-- Indexes for table `assigned_orders_for_delivery`
--
ALTER TABLE `assigned_orders_for_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `chat_request`
--
ALTER TABLE `chat_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_types`
--
ALTER TABLE `complaint_types`
  ADD PRIMARY KEY (`complaint_type_id`);

--
-- Indexes for table `complaint_types_new`
--
ALTER TABLE `complaint_types_new`
  ADD PRIMARY KEY (`complaint_type_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `home_images`
--
ALTER TABLE `home_images`
  ADD PRIMARY KEY (`homeimage_id`);

--
-- Indexes for table `home_video`
--
ALTER TABLE `home_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`order_item_it`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `page_images`
--
ALTER TABLE `page_images`
  ADD PRIMARY KEY (`page_image_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD PRIMARY KEY (`product_specification_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `support_tickets`
--
ALTER TABLE `support_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_users`
--
ALTER TABLE `support_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_orders`
--
ALTER TABLE `temp_orders`
  ADD PRIMARY KEY (`temp_order_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indexes for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_districts`
--
ALTER TABLE `user_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visit_complaint_types`
--
ALTER TABLE `visit_complaint_types`
  ADD PRIMARY KEY (`visit_complaint_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `ad_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assigned_orders_for_delivery`
--
ALTER TABLE `assigned_orders_for_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `chat_request`
--
ALTER TABLE `chat_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `complaint_types`
--
ALTER TABLE `complaint_types`
  MODIFY `complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `complaint_types_new`
--
ALTER TABLE `complaint_types_new`
  MODIFY `complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `email_template_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `home_images`
--
ALTER TABLE `home_images`
  MODIFY `homeimage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_video`
--
ALTER TABLE `home_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `newsletter_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `order_item_it` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `page_images`
--
ALTER TABLE `page_images`
  MODIFY `page_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_specifications`
--
ALTER TABLE `product_specifications`
  MODIFY `product_specification_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `support_tickets`
--
ALTER TABLE `support_tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support_users`
--
ALTER TABLE `support_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_orders`
--
ALTER TABLE `temp_orders`
  MODIFY `temp_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ticket_comments`
--
ALTER TABLE `ticket_comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `user_districts`
--
ALTER TABLE `user_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visit_complaint_types`
--
ALTER TABLE `visit_complaint_types`
  MODIFY `visit_complaint_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
